<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/marks_layout.css">
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="print" /> 
<script>
	function printpage()
  {
  window.print()
  }
  function showbatch()
	{
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showbatch.php",true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("batch").innerHTML=xmlhttp.responseText;
			}
		}
	}


  function showsection(str)
	{
		
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showsection.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("section").innerHTML=xmlhttp.responseText;
			}
		}
	}
function slt(val){
if(val=='TOP' || val=='BOTTOM'){
document.getElementById("bth").style.visibility='visible';
document.getElementById("bth2").style.visibility='visible';
}
else{
document.getElementById("bth").style.visibility='hidden';
document.getElementById("bth2").style.visibility='hidden';
}}

function sdtl(){
document.getElementById("dtl").style.display='block';
document.getElementById("qaz").style.display='none';

}

function init(){
document.getElementById("bth").style.visibility='hidden';
document.getElementById("bth2").style.visibility='hidden';
// document.getElementById("dtl").style.display='none';
}
 </script>

</head>
<body onload="init();showbatch();">


<?php
include 'common.php';
include 'connect.php';
global $DB;
echo "<div id='space'></br></br></br></br></br></br></br></div>";
?>
<div style="font-size:18;font-weight:200%; margin-top:-5%; margin-bottom:4%;" align="center"> BRANCH-WISE INTERNAL MARKS</div>
	<div align="center"><form action="view_report_marks_all2.php" method="post">
	<table id='except'>
			<tr>
			<td>Batch:</td>
			<td>
				<select size="1" name="batch" id="batch" required="required" onchange="showsection(this.value)" required>
		
	</select>
			</td>
		</tr>
		<tr>
			<td>
			Branch</td>
			<td >
			<select size="1" name="brch" id="branch" required>
				<option>Select</option>
				<option value="CSE">CSE</option>
				<option value="IT">IT</option>
				<option value="ME">ME</option>
				<option value="ECE">ECE</option>
				<option value="EI">EI</option>
				<option value="EN">EN</option>
				<option value="CE">CE</option>
				<option value="MBA">MBA</option>
				<option value="MCA">MCA</option>
				
			</select></td>
		</tr>
			<tr>
				<td>FILTER:</td>
				<td colspan='2'>
					<select name='filter' onchange=slt(this.value);>
						<option value='NORMAL'>Normal Order</option>
						<option value='DEC'>Descending Order</option>
						<option value='ASC'>Ascending Order</option>
						<option value='TOP'>Top N</option>
						<option value='BOTTOM'>Bottom N</option>
					</select>
				</td>
				<!-- <td><input type="submit" value="Submit" class="button" name="submit"></td> -->
			</tr>
			<tr>
				<td id='bth'>NUMBER OF STUDENT:</td>
				
				<td id='bth2'><input type="text" name="nstudent" size="1" />
				</td>
			<td ><input type="submit" value="Submit" class="button" name="submit" ></td>
			</tr>
	</table></form></div>
	<?php
		if(isset($_POST['submit'])){
			$batch = $_POST['batch'];
			$brch = $_POST['brch'];
			$filter=$_POST['filter'];
			$z=$_POST['nstudent'];
			$k=1;

			$branch1=$brch;

				if($branch1=='CSE'){
			$fullname='COMPUTER SCIENCE AND ENGINEERING';	
			}
			else if($branch1=='IT'){
			$fullname='INFORMATION TECHNOLOGY';
			}
			else if($branch1=='ME'){
			$fullname='MECHANICAL ENGINEERING';
			}
			else if($branch1=='ECE'){
			$fullname='ELECTRONICS & COMMUNICATION ENGINEERING';
			}
			else if($branch1=='EI'){
			$fullname='ELECTRONICS & INSTRUMENTATION ENGINEERING';
			}
			else if($branch1=='EN'){
			$fullname='ELECTRICAL & ELECTRONICS ENGINEERING';
			}
			else if($branch1=='CE'){
			$fullname='CIVIL ENGINEERING';
			}
			else if($branch1=='MBA'){
			$fullname='MASTER OF BUSINESS APPLICATION';
			}
			else if($branch1=='MCA'){
			$fullname='MASTER OF COMPUTER APPLICATION';
			}
			echo "<div align='center' ><input type='button' value='PRINT THIS PAGE' onclick='printpage()' class='button' id='print'></div>";
			
			
			echo "<div id='heading' align='center'>
					
					<strong style=font-size:20px>Ajay Kumar Garg Engineering College, Ghaziabad</strong></br>
					<strong style=font-size:15px>Branch: $fullname</strong></br>
					<strong style=font-size:15px>Batch: $batch</strong></br>
					<strong style=font-size:18px>STUDENT MARKS LIST";
					if($filter=='NORMAL')
					$show='NORMAL ORDER';
					else if($filter=='DEC')
					$show='DESECENDING ORDER';
					else if($filter=='ASC')
					$show='ASECENDING ORDER';
					else if($filter=='TOP')
					$show='TOP '.$z;
					else if($filter=='BOTTOM')
					$show='BOTTOM '.$z;
					echo "($show)</strong>
				</div></br>";
			$sql="SELECT * FROM student_info WHERE batchyear='$batch' ORDER BY username ASC";
		
			$getsql = mysqli_query($conn, $sql);
			if(!$getsql )
					{
					  die('Could not enter dataaaaa: ' . mysqli_error($conn));
					}
			$i=0;
			$clr_dump ="DELETE FROM dump";
			
			$clear = mysqli_query( $conn,$clr_dump );
			
			while($row = mysqli_fetch_assoc($getsql)){
				$user->id=$row['id'];
				$user->firstname=$row['firstname'];
				$user->lastname=$row['lastname'];
				$user->username=$row['username'];
				$user->email=$row['email'];
				
				$user_section = $row['section'];
			//$student_section=$user_section;	
$user_bch = $row['branch'];
				if($user_bch==$brch)
				{	
					$i++;
					$student[$i]=$user->username;
					$nullv=0;

					$user_percentageinsemester1 = trim($row['1sem']);
					if($user_percentageinsemester1!=NULL && is_numeric($user_percentageinsemester1) || strpos($user_percentageinsemester1,'%') and !empty($user_percentageinsemester1))
					$nullv++;

					$user_percentageinsemester2 = trim($row['2sem']);
					if($user_percentageinsemester2!=NULL && is_numeric($user_percentageinsemester2) || strpos($user_percentageinsemester2,'%') and !empty($user_percentageinsemester2))
					$nullv++;

					$user_percentageinsemester3 = trim($row['3sem']);
					if($user_percentageinsemester3!=NULL && is_numeric($user_percentageinsemester3) || strpos($user_percentageinsemester3,'%') and !empty($user_percentageinsemester3))
					$nullv++;

					$user_percentageinsemester4 = trim($row['4sem']);
					if($user_percentageinsemester4!=NULL && is_numeric($user_percentageinsemester4) || strpos($user_percentageinsemester4,'%') and !empty($user_percentageinsemester4))
					$nullv++;

					$user_percentageinsemester5 = trim($row['5sem']);
					if($user_percentageinsemester5!=NULL && is_numeric($user_percentageinsemester5) || strpos($user_percentageinsemester5,'%')  and !empty($user_percentageinsemester5))
					$nullv++;

					$user_percentageinsemester6 = trim($row['6sem']);
					if($user_percentageinsemester6!=NULL && is_numeric($user_percentageinsemester6) || strpos($user_percentageinsemester6,'%')  and !empty($user_percentageinsemester6))
					$nullv++;

					$user_percentageinsemester7 = trim($row['7sem']);
					if($user_percentageinsemester7!=NULL && is_numeric($user_percentageinsemester7) || strpos($user_percentageinsemester7,'%') and !empty($user_percentageinsemester7))
					$nullv++;

					$user_percentageinsemester8 = trim($row['8sem']);
					if($user_percentageinsemester8!=NULL && is_numeric($user_percentageinsemester8) || strpos($user_percentageinsemester8,'%') and !empty($user_percentageinsemester8))
					$nullv++;


					$student_total=$user_percentageinsemester1+$user_percentageinsemester2+$user_percentageinsemester3+$user_percentageinsemester4+$user_percentageinsemester5+$user_percentageinsemester6+$user_percentageinsemester7+$user_percentageinsemester8;
					$student_average[$i]=number_format(($student_total/($nullv)),2);
					
					$temp[$student[$i]]=$student_average[$i];
					$total_marks[$student[$i]]=$student_average[$i];
				}
			
			}
			arsort($temp);
			foreach($temp as $x=>$x_value){
				$maxper=$temp[$x];
				break;
			}
			
			
			if($filter==NORMAL)
			{}
			else if($filter==ASC ||$filter==BOTTOM )
			asort($total_marks);
			else if($filter==DEC || $filter==TOP )
			arsort($total_marks);
			$z++;
			$n=0;
			echo "<table id='list' style='margin:auto;'>";
			echo "<tr>
					<td colspan='4'></td>
					<td colspan='2'></td>
					<td colspan='9' style='text-align:center'>OLD SEM MARKS(%)</td>
					<td colspan='6' style='text-align:center'>CURRENT SEM MARKS ATTENDANCE (%)</td>
				</tr>";
			echo "<tr id='head'>
					<td>S.NO.</td>
					<td>Roll No.</td>
					<td>Name</td>
					<td>Section</td>
					<td>10th %</td>
					<td>12th %</td>
					<td>Sem 1 Marks</td>
					<td>Sem 2 Marks</td>
					<td>Sem 3 Marks</td>
					<td>Sem 4 Marks</td>
					<td>Sem 5 Marks</td>
					<td>Sem 6 Marks</td>";
					if($branch1!='MCA'){
					echo "<td>Sem 7 Marks</td>
						<td>Sem 8 Marks</td>";
					}
					echo "<td>BTech Avg.</td>
					<td>CT1</td>
					<td>ST1</td>
					<td>CT2</td>
					<td>ST2</td>
					<td>PUT</td>
					<td>ATT. %</td>
				</tr>";
				$i=$z;
				foreach($total_marks as $x=>$x_value){
					$i--;
					$bot[$i]=$x;
					if($i==1)
					{break;}
				}
				
				
				foreach($total_marks as $x=>$x_value){
				$n++;
				if(($filter==TOP || $filter==BOTTOM) && $n==$z)
				{break;}
				if($filter==BOTTOM){
				$student_id = $bot[$n];
				}
				else{
				$student_id = $x;
				}

				$qry11="SELECT * from student_info WHERE username='$student_id' ";
				$res11=mysqli_query($conn, $qry11);

				if(!$res11)
					echo mysqli_error($res11);

				$row11=mysqli_fetch_assoc($res11);
				$name=$row11['firstname']." ".$row11['lastname'];
				$user_student_number = $row11['studentnumber'];
				
				$user_branch = $row11['branch'];
				$user_stion = $row11['section'];
				
				$user_semester = $row11['currsem'];
				$user_10tdmarks = $row11['10'];
				$user_12tdmarks = $row11['12'];
				$nullv=0;
				$userper[1]= trim($row11['1sem']);
				if(($userper[1]!=NULL && is_numeric($userper[1])  || strpos($userper[1],'%')) and $userper[1]>0)
				$nullv++;
				$userper[2]= trim($row11['2sem']);
				if(($userper[2]!=NULL && is_numeric($userper[2]) || strpos($userper[2],'%')) and $userper[2]>0)
				$nullv++;
				$userper[3]= trim($row11['3sem']);
				if(($userper[3]!=NULL  && is_numeric($userper[3]) || strpos($userper[3],'%')) and $userper[3]>0)
				$nullv++;
				$userper[4] = trim($row11['4sem']);
				if(($userper[4]!=NULL && is_numeric($userper[4]) || strpos($userper[4],'%')) and $userper[4]>0)
				$nullv++;
				$userper[5] = trim($row11['5sem']);
				if(($userper[5]!=NULL && is_numeric($userper[5]) || strpos($userper[5],'%')) and $userper[5]>0)
				$nullv++;
				$userper[6] = trim($row11['6sem']);
				if(($userper[6]!=NULL && is_numeric($userper[6]) || strpos($userper[6],'%')) and $userper[6]>0)
				$nullv++;
				$userper[7] = trim($row11['7sem']);
				if(($userper[7]!=NULL && is_numeric($userper[7]) || strpos($userper[7],'%')) and $userper[7]>0)
				$nullv++;
				$userper[8] = trim($row11['8sem']);
				if(($userper[8]!=NULL && is_numeric($userper[8]) || strpos($userper[8],'%')) and $userper[8]>0)
				$nullv++;

				$user_percentageinsemester_total=$userper[1]+$userper[2]+$userper[3]+$userper[4]+$userper[5]+$userper[6]+$userper[7]+$userper[8];
				$user_percentageinsemester_average=number_format(($user_percentageinsemester_total/($nullv)),2);




// $getr="SELECT * FROM marks WHERE st_id='$student_id' AND sub_id='$subjectid'";
							
// 							$getre= mysqli_query($conn,$getr);
// 							while($row = mysqli_fetch_array($getre)){
// 								if($examtype=='CT1')
// 								$e='exam1';
// 								if($examtype=='CT2')
// 								$e='exam2';
// 								if($examtype=='ST1')
// 								$e='exam3';
// 								if($examtype=='ST2')
// 								$e='exam4';
// 								if($examtype=='PUT')
// 								$e='exam5';
// 								if($row[$e] =='DB')
// 								{	$avg_total='DB';}
// 								else if($row[$e] =='PC')
// 								{	$avg_total='PC';}
// 								else
// 									echo "";
// 								$marks=$row[$e];
// 								$total = $total+$marks;
// 								if($marks=='DB')
// 								{
// 									$remark[$uni_no]=1;
// 								}
// 								else if($marks=='PC')
// 								{
// 									$remark[$uni_no]=2;
// 								}
// 								else if($marks=='A')
// 								{
// 									$remark[$uni_no]=3;
// 								}
// 								if($marks!=NULL)
// 								{
// 									include_once('marks.php');

// 								if($examtype=='CT1' )                
// 									$max=marks_t('CT1');

// 								elseif($examtype=='CT2')
// 									$max=marks_t('CT2');

// 								elseif($examtype=='ST1' )
// 									$max=marks_t('ST1');

// 								elseif($examtype=='ST2')
// 									$max=marks_t('ST2');

// 								elseif($examtype=='PUT')
// 								    $max=marks_t('PUT',$subjectid);

// 									if($examtype=='CT1')
// 									 $max=10;
// 									elseif($examtype=='CT2')
// 									$max=10;
// 									elseif($examtype=='ST1')
// 									$max=100;
// 									elseif($examtype=='ST2')
// 									$max=30;
// 									elseif($examtype=='PUT')
// 									{
// 										$query="SELECT marks FROM subject WHERE sub_id='$subjectid'"; 
// 										$get_query= mysql_query( $query, $conn );
// 										while($row2 = mysql_fetch_array($get_query, MYSQL_ASSOC)){
// 											$max=$row2['marks'];
// 										}
// 									}
// 									$checksome+=$max;
// 								}
// 							}
// 						}
							
// 					}
// 						if($avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
// 						$avg_total=number_format($total/$checksome*100,2);
// 						$total = 0;
// 							$checksome = 0;
							
// 							if($examtype=='CT1'){
// 								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
// 									$avg_total=" ";
// 								$CT1[$i]=$avg_total;}

// 							if($examtype=='CT2')
// 									{
// 								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
// 									$avg_total=" ";
// 								$CT2[$i]=$avg_total;}

// 							if($examtype=='ST1')
// 									{
// 								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
// 									$avg_total=" ";
// 								$ST1[$i]=$avg_total;}

// 							if($examtype=='ST2')
// 									{
// 								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
// 									$avg_total=" ";
// 								$ST2[$i]=$avg_total;}

// 							if($examtype=='PUT')
// 								{
// 								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
// 									$avg_total=" ";
// 								$PUT[$i]=$avg_total;}
// 				}
// 			}

				$getmarks="SELECT * FROM marks WHERE st_id='$student_id'";
				
				$retval = mysqli_query($conn, $getmarks);
				
				while($row = mysqli_fetch_assoc($retval)){
									
      				 if($row['exam1']=='PC' || $row['exam1']=='DB' || $row['exam1']==NULL || $row['exam1']==' '){
					 $ct1[$student_id]=$row['exam1'];
					 }else{$ct1[$student_id]=round($row['exam1'],2);}
					 if($row['exam2']=='PC' || $row['exam2']=='DB' || $row['exam2']==NULL || $row['exam2']==' '){
					 $ct2[$student_id]=$row['exam2'];
					 }else{$ct2[$student_id]=round($row['exam2'],2);}
					 if($row['exam3']=='PC' || $row['exam3']=='DB' || $row['exam3']==NULL || $row['exam3']==' '){
					 $st1[$student_id]=$row['exam3'];
					 }else{$st1[$student_id]=round($row['exam3'],2);}
					 if($row['exam4']=='PC' || $row['exam4']=='DB' || $row['exam4']==NULL || $row['exam4']==' '){
					 $st2[$student_id]=$row['exam4'];
					 }else{$st2[$student_id]=round($row['exam4'],2);}
					 if($row['exam5']=='PC' || $row['exam5']=='DB' || $row['exam5']==NULL || $row['exam5']==' '){
					 $put[$student_id]=$row['exam5'];
					 }else{$put[$student_id]=round($row['exam5'],2);}
					
				}








				
				$getatt="SELECT * FROM attendance_old WHERE st_id='$student_id' AND semester='$user_semester'";
				
				$retval = mysqli_query(  $conn,$getatt );
				while($row = mysqli_fetch_assoc($retval)){
					$att[$student_id]=$row['attendance'];
				}
				
				$aspect=round($maxper-$user_percentageinsemester_average,2);
				
				$ids[] = $student_id;
				$names[] = $name;
				$sttnqry="SELECT section FROM student WHERE st_id='$student_id'";
				$resstnn=mysqli_query($conn, $sttnqry);
				$sttn=mysqli_fetch_array($resstnn);
				echo "<tr>
					<td>$n</td>
					<td>$student_id</td>
					<td>$name</td>
					<td>$sttn[0]</td>
					<td>$user_10tdmarks </td>
					<td>$user_12tdmarks </td>
					<td>$userper[1]</td>
					<td>$userper[2]</td>
					<td>$userper[3]</td>
					<td>$userper[4]</td>
					<td>$userper[5]</td>
					<td>$userper[6]</td>";
					if($branch1!='MCA'){
					echo "<td>$userper[7]</td>
						<td>$userper[8]</td>";
					}
					echo "<td>$user_percentageinsemester_average</td>";
                     
                     $query="SELECT setting FROM allow WHERE type='T' order by setting ASC";
                     $execute= mysqli_query( $conn,$query );
                     while($row = mysqli_fetch_assoc($execute)){
                     $examtype=$row['setting'];
                     
                     $cursemqry="SELECT * FROM student WHERE st_id='$student_id'";
                     $rescursem=mysqli_query($conn,$cursemqry);
                     		$current_sem = mysqli_fetch_assoc($rescursem);
                     		$current_sec = $current_sem['section'];
                     		$current_sem = $current_sem['semester'];
                     	$getsub="SELECT DISTINCT(subject.sub_id),subject.category FROM assignrole,subject WHERE subject.sub_id=assignrole.sub_id AND assignrole.semester='$current_sem' AND assignrole.section='$current_sec'  ORDER BY assignrole.sub_id";	
                     		$get= mysqli_query($conn, $getsub );
                     	$avg_total = '';
                     	while($row = mysqli_fetch_assoc($get)){
                     		
                     		$subjectid=$row['sub_id'];
                     		$cat=$row['category'];
                     				
                     		if($cat=='T' || $cat=='O'){
                     					 $getr="SELECT * FROM marks WHERE st_id='$student_id' AND sub_id='$subjectid'";
                     			
                     			$getre= mysqli_query($conn, $getr );
                     			while($row = mysqli_fetch_assoc($getre)){
                     				if($examtype=='CT1')
                     				$e='exam1';
                     				if($examtype=='CT2')
                     				$e='exam2';
                     				if($examtype=='ST1')
                     				$e='exam3';
                     				if($examtype=='ST2')
                     				$e='exam4';
                     				if($examtype=='PUT')
                     				$e='exam5';
                     				if($row[$e] =='DB')
                     				{	$avg_total='DB';}
                     				else if($row[$e] =='PC')
                     				{	$avg_total='PC';}
                     				
                     				else
                     					echo "";
                     				
                     				 $marks=$row[$e];
                     				//echo $marks;
                     				$total = $total+$marks;
                     				if($marks=='DB')
                     				{
                     					$remark[$uni_no]=1;
                     				}
                     				else if($marks=='PC')
                     				{
                     					$remark[$uni_no]=2;
                     				}
                     				else if($marks=='A')
                     				{
                     					$remark[$uni_no]=3;
                     				}
                     				if($marks!=NULL)
                     				{
                     					if($examtype=='CT1')
                     					 $max=10;
                     					elseif($examtype=='CT2')
                     					$max=10;
                     					elseif($examtype=='ST1')
                     					$max=25;
                     					elseif($examtype=='ST2')
                     					$max=50;
                     					elseif($examtype=='PUT')
                     					{
                     						$query="SELECT marks FROM subject WHERE sub_id='$subjectid'"; 
                     						$get_query= mysqli_query($conn,$query );
                     						while($row2 = mysqli_fetch_assoc($get_query, MYSQL_ASSOC)){
                     							$max=$row2['marks'];
                     						}
                     					}
                     					$checksome+=$max;
                     				}
                     			}
                     		}
                     			
                     	}
                     	//10echo $total;
                     		if($avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
                     		$avg_total=number_format($total/$checksome*100,2);
                     		$total = 0;
                     			$checksome = 0;
                     			
                     			if($examtype=='CT1'){
								   if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$ct1[$student_id]=$avg_total;}

								if($examtype=='CT2'){
									if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									//echo "ayush shubham";
									$avg_total=" ";
								
									
								$ct2[$student_id]=$avg_total;}

								if($examtype=='ST1')
									{
									if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$st1[$student_id]=$avg_total;}

								if($examtype=='ST2')
									{
									if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$st2[$student_id]=$avg_total;}

								if($examtype=='PUT')
									{
									if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$put[$student_id]=$avg_total;}
                     }
                  


					echo "<td>$ct1[$student_id]</td>";
				if($st1[$student_id]=='PC' || $st1[$student_id]=='DB'){
				echo "<td style='background-color:#888888'>$st1[$student_id]</td>";
				}else
				echo "<td>$st1[$student_id]</td>";
					echo "<td>$ct2[$student_id]</td>";

				if($st2[$student_id]=='PC' || $st2[$student_id]=='DB'){
				echo "<td style='background-color:#888888'>$st2[$student_id]</td>";
				}else
					echo "<td>$st2[$student_id]</td>";
		
				if($put[$student_id]=='PC' || $put[$student_id]=='DB'){
				echo "<td style='background-color:#888888'>$put[$student_id]</td>";
				}else
					echo "<td>$put[$student_id]</td>";
			
					$qry="SELECT SUM(attended), SUM(totalclasses) FROM attendance WHERE st_id='$student_id' AND sub_id IN (SELECT sub_id FROM subject WHERE category='T' OR category='O' OR category='P')";
				$rqry=mysqli_query($conn,$qry);
				echo mysqli_error();
				$x=0;$y=0;$s1=$s2=0;
				while($row = mysqli_fetch_array($rqry))
				{
					//echo "string";
					$x=$row[0];
					$y=$row[1];
					$s1+=$row[0];
					$s2+=$row[1];
				}
				$att[$student_id]=round(($s1/$s2)*100,2);
			echo "<td>$att[$student_id]</td>";
					
				echo "</tr>";
			
			}
			
			echo "</table>";
    		// if($filter=='TOP'||$filter=='BOTTOM') echo "<div align='center' id='print'><input type='button' value='VIEW DETAILS' onclick='sdtl()' class='button' id='qaz'></div>";
			
    ////////////////////////////////////////////////////////////////////////

		if($filter=='TOP'||$filter=='BOTTOM')
		{	

			//mysql_select_db('portal');
			echo '<div id="dtl" style="display:none">';

			$query="SELECT setting FROM allow WHERE type='T' order by setting ASC";
			//mysql_select_db('portal');
			$execute= mysqli_query(  $conn,$query );
			while($row = mysqli_fetch_assoc($execute)){
			$examtype=$row[0];

			echo '<h3 style="text-align:center;">SUBJECT WISE '.$examtype.' EXAM DETAILS</h3>';

			echo "<center><table width='80%'>";
			echo '<tr id="head">';
			echo '<td>S.No</td>';
			echo '<td>Roll no.</td>';

			$qsub=mysqli_query($conn,"SELECT DISTINCT(sub_id) FROM marks WHERE st_id='$ids[0]'");

			while($rsub=mysqli_fetch_assoc($qsub,MYSQL_ASSOC)){
				foreach($rsub as $sub)
			{
				echo '<td>'.$sub.'</td>';
			}
			}
			echo '<td>Total</td>';
			echo '<td>Average %</td>';
			echo '</tr>';
			$serial=0;

			foreach($ids as $id)
			{
				$serial++;
				$m=0;
				$c=0;
				echo "<tr>";
				echo "<td>$serial</td>";
				echo '<td>'.$id.'</td>';
				$qsub=mysqli_query("SELECT DISTINCT(sub_id) FROM marks WHERE st_id='$id'",$conn);
				while($rsub=mysqli_fetch_assoc($qsub)){
				foreach($rsub as $sub)
				{
				if($examtype=='CT1')
				$e='exam1';
				if($examtype=='CT2')
				$e='exam2';
				if($examtype=='ST1')
				$e='exam3';
				if($examtype=='ST2')
				$e='exam4';
				if($examtype=='PUT')
				$e='exam5';
				$qr = mysqli_fetch_assoc(mysqli_query($conn,"SELECT $e FROM marks WHERE st_id='$id' AND sub_id='$sub'"));
				$m+=$qr[0];
				$c++;
				echo '<td>'.$qr[0].'</td>';
				}

				}			
				echo "<td>$m</td>";
				if($examtype=='CT1')
				$zxc=$ct1[$id];
				if($examtype=='CT2')
				$zxc=$ct2[$id];
				if($examtype=='ST1')
				$zxc=$st1[$id];
				if($examtype=='ST2')
				$zxc=$st2[$id];
				if($examtype=='PUT')
				$zxc=$put[$id];
				echo '<td>'.$zxc.'</td>';
				//echo '<td>'.round(($m/$c),2).'</td>';							
				echo "</tr>";
			}

			echo "</table></center>";
			}
		////////////////////////////////////////////////////////////////////////

			echo '<h3 style="text-align:center;">Attendance</h3>';
			echo '<center><table width="80%">
		<tr id="head"><td><b>S.No.</b></td><td> <b>Student Roll No.</b></td><td width=20%><b>Student Name</b></td>';

		//mysql_select_db('portal');
		$qry="SELECT DISTINCT(`subject`.sub_id) FROM `subject`,assignrole WHERE `subject`.sub_id=assignrole.sub_id AND `subject`.branch='$branch1' AND assignrole.semester='$user_semester' AND (`subject`.category='T' OR `subject`.category='O' OR `subject`.category='P')";
		
		$rqry=mysqli_query($qry,$conn);

		while($row = mysqli_fetch_array($rqry))
		{
			$subs[]=$row[0];
			echo "<td><b>".$row[0]."</b>< /td>";
		}

		echo "<td>Total</td><td>%</td></tr>";
		$i=1;
		foreach($ids as $id){

			echo '<tr>';
			$s1=$s2=0;
			echo '<td>'.$i.'</td>';
			echo '<td>'.$id.'</td>';
			echo '<td>'.$names[$i-1].'</td>';
			//echo '<td>'.$id.'</td>';
			//echo '<td>'.$sub.'</td>';
						echo $id;
			foreach($subs as $sub){
			echo $qry="SELECT SUM(attended), SUM(totalclasses) FROM attendance WHERE st_id='$id' AND sub_id='$sub'";
			$rqry=mysqli_query($qry,$conn);
			echo mysqli_error($conn);
			$x=0;$y=0;
				while($row = mysqli_fetch_assoc($rqry))
				{
					//echo '<td>'.print_r($row).'</td>';
			
					$x=$row[0];
					$y=$row[1];
					$s1+=$row[0];
					$s2+=$row[1];
				}
				echo '<td>'.$x.'/'.$y.'</td>';
			}
			echo '<td>'.$s1.'/'.$s2.'</td>';
			echo '<td>'.round((($s1/$s2)*100),2).'</td>';
			echo '</tr>';
			$i++;
		}
		echo '</table></center>';

	}

		echo '</div>';
		if($filter=='TOP'||$filter=='BOTTOM')
		echo '<script>document.getElementById("dtl").style.display="none";</script>';
		///////////////////////////////////////////////////////////////////////

			//mysql_select_db('portal');

			$get_hodname="SELECT * FROM login WHERE userid='$username'";
			//mysql_select_db('portal');
			$getit= mysqli_query( $conn,$get_hodname  );
			while($row = mysqli_fetch_assoc($getit)){
			$hodname=$row['name'];
			$branch=$row['branch'];
			$category=$row['category'];
			}

			$get_hodname="SELECT * FROM login WHERE branch='$branch' AND category='HOD'";
			mysql_select_db('portal');
			$getit= mysqli_query(  $conn, $get_hodname );
			while($row = mysqli_fetch_assoc($getit)){
			$h_name=$row['name'];
			}

			$query="SELECT * FROM coordinator WHERE fac_id='$username'";
			mysql_select_db('portal');
			$exec= mysqli_query($conn, $query );
			$is_c=0;
			while($row = mysqli_fetch_assoc($exec)){
			$is_c=1;
			}
			echo '<br/><br/><br/><br/>';
			if($category=='FACULTY'){
				if($is_c==1){

				echo "<table class='last' id='heading'><tr><td><div id='heading' align='left'>";
					echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>(FACULTY COORDINATOR)</strong>";
				echo "</div></td>";
				echo "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><td><div id='heading' align='right'>";
					echo "<strong style=font-size:18px>$h_name</br>
					<strong style=font-size:18px>(HOD $branch)</strong>";
				echo "</div></td></tr></table>";
				
				}

				else{
					echo "<table class='last' id='heading'><tr><td><div id='heading' align='left'>";
					echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>(FACULTY)</strong>";
				echo "</div></td>";
				echo "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><td><div id='heading' align='right'>";
					echo "<strong style=font-size:18px>$h_name</br>
					<strong style=font-size:18px>(HOD $branch)</strong>";
				echo "</div></td></tr></table>";
			
				}
			}
			else{
				echo "<div id='heading' align='right'>";
					echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>($category $branch)</strong>";
				echo "</div>";
			}
		
		}
		
	?>
</body>	
</html>
