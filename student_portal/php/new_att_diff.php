<?php session_start();
$all_diff=[];
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/marks_layout.css">
    <link rel="stylesheet" href="../css/print.css" type="text/css" media="print" />
</head>
<script>
    function printpage()
    {
        window.print()
    }

    function showbatch()
    {
        var xmlhttp;

        if(window.XMLHttpRequest)
        {
            //code for IE7,firefox,chrome,opera,safari

            xmlhttp=new XMLHttpRequest();
        }
        else
        {

            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.open("GET","showbatch.php",true)
        xmlhttp.send();

        xmlhttp.onreadystatechange=function()
        {
            if(xmlhttp.readyState==4&&xmlhttp.status==200)
            {

                document.getElementById("batch").innerHTML=xmlhttp.responseText;
            }
        }
    }



    function showsection(str)
    {

        var xmlhttp;

        if(window.XMLHttpRequest)
        {
            //code for IE7,firefox,chrome,opera,safari

            xmlhttp=new XMLHttpRequest();
        }
        else
        {

            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.open("GET","showsection.php?q="+str,true)
        xmlhttp.send();

        xmlhttp.onreadystatechange=function()
        {
            if(xmlhttp.readyState==4&&xmlhttp.status==200)
            {

                document.getElementById("section").innerHTML=xmlhttp.responseText;
            }
        }
    }
    function slt(val){
        if(val=='TOP' || val=='BOTTOM'){
            document.getElementById("bth").style.visibility='visible';
            document.getElementById("bth2").style.visibility='visible';
        }
        else{
            document.getElementById("bth").style.visibility='hidden';
            document.getElementById("bth2").style.visibility='hidden';
        }}
    function init(){
        document.getElementById("bth").style.visibility='hidden';
        document.getElementById("bth2").style.visibility='hidden';
    }



    function filu() {

        //alert("dfds");
        var filter = document.getElementById("filter").value;
        if (filter == 1 || filter == 2) {
            document.getElementById("start").style.visibility = "visible";
            document.getElementById("to1").style.visibility = "hidden";
            document.getElementById("end").style.visibility = "hidden";
        }
        else {
            document.getElementById("start").style.visibility = "visible";
            document.getElementById("to1").style.visibility = "visible";
            document.getElementById("end").style.visibility = "visible";
        }

    }
</script>
</html>


<?php
include 'common.php';
include 'connect.php';

echo "<div id='space'></br></br></br></br></br></br></br></div>";
?>
<html>
<body onload="init();showbatch();">
<div style="font-size:18;font-weight:200%; margin-top:-5%; margin-bottom:4%;" align="center"> SECTION-WISE ATTENDANCE DIFFERENCE</div>
<div align="center"><form action="new_att_diff.php" method="post">
        <table id='except'>
            <tr>
                <td>batch:</td>
                <td>
                    <select size="1" name="batch" id="batch" required="required" onchange="showsection(this.value)" required>

                    </select>

                </td>
            </tr>
            <tr>
                <td>
                    Section</td>
                <td >
                    <select size="1" name="section" id="section" onchange="showsubjectid(this.value)" required></select></td>
            </tr>
            <tr>
                <td>FILTER:</td>
                <td colspan='2'>
                    <select name='sort' onchange=slt(this.value);>
                        <option value='NORMAL'>Normal Order</option>
                        <option value='DEC'>Descending Order</option>
                        <option value='ASC'>Ascending Order</option>
                        <option value='TOP'>Top N</option>
                        <option value='BOTTOM'>Bottom N</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td id='bth'>NUMBER OF STUDENT:</td>
                <td id='bth2'><input type="text" name="nstudent" size="1" /></td>

            </tr>

            <tr>
                <td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Optional Filter</td>
            </tr>
            <tr>
                <td style="font-weight:bold">Filter</td>
                <td>
                    <select size="1" name="filter" id="filter" onchange="filu()">
                        <option value="">-select-</option>
                        <option value=1>Less Than</option>
                        <option value=2>Greater Than</option>
                        <option value=3>Between</option>
                    </select>
                </td>
                <td><input type="text" name="start" id="start" size="4"><label id="to1">TO</label></td>
                <td><input type="text" name="end" id="end" size="4"></td>
            </tr>
            <tr>
                <td></td><td><center><input type="submit" value="Submit" class="button" name="submit"></center></td>
            </tr>

        </table></form></div>
<?php
if(isset($_POST['submit'])){
    $batch = $_POST['batch'];
    $student_section = $_POST['section'];
    $sort = $_POST['sort'];
    $start = $_POST['start'];
    $end = $_POST['end'];
    $filter=$_POST['filter'];
    $z=$_POST['nstudent'];
    $k=1;


    $get_branch="SELECT DISTINCT branch FROM student_info WHERE section='$student_section' AND batchyear='$batch'";

    $getit= mysqli_query($conn,$get_branch );
    while($row = mysqli_fetch_assoc($getit)){
        $branch1=$row['branch'];
    }

    if($branch1=='CSE'){
        $fullname='COMPUTER SCIENCE AND ENGINEERING';
    }
    else if($branch1=='IT'){
        $fullname='INFORMATION TECHNOLOGY';
    }
    else if($branch1=='ME'){
        $fullname='MECHANICAL ENGINEERING';
    }
    else if($branch1=='ECE'){
        $fullname='ELECTRONICS & COMMUNICATION ENGINEERING';
    }
    else if($branch1=='EI'){
        $fullname='ELECTRONICS & INSTRUMENTATION ENGINEERING';
    }
    else if($branch1=='EN'){
        $fullname='ELECTRICAL & ELECTRONICS ENGINEERING';
    }
    else if($branch1=='CE'){
        $fullname='CIVIL ENGINEERING';
    }
    else if($branch1=='AS-HU'){
        $fullname='APPLIED SCIENCE AND HUMANITIES';
    }
    else if($branch1=='MCA'){
        $fullname='MASTER OF COMPUTER APPLICATION';
    }
    echo "<div align='center' ><input type='button' value='PRINT THIS PAGE' onclick='printpage()' class='button' id='print'></div>";
    echo "<div id='heading' align='center'>
					<div align='right'><p style=font-size:14px>AKGEC/MR/FM/09</p></div>
					<strong style=font-size:20px>Ajay Kumar Garg Engineering College, Ghaziabad</strong></br>
					<strong style=font-size:15px>Branch: $fullname (Section-$student_section)</strong></br>
					<strong style=font-size:15px>Batch: $batch</strong></br>
					<strong style=font-size:18px>STUDENT ATTENDANCE LIST";
    if($sort=='NORMAL')
        $show1='NORMAL ORDER';
    else if($sort=='DEC')
        $show1='DESECENDING ORDER';
    else if($sort=='ASC')
        $show1='ASECENDING ORDER';
    else if($sort=='TOP')
        $show1='TOP '.$z;
    else if($sort=='BOTTOM')
        $show1='BOTTOM '.$z;
    echo " (".$show1.")";
    
$fl=$_POST['filter'];
            if($fl=='1')
                echo '<center><h5>Less Than '.$_POST['start'].' %</h5></center>';
            elseif($fl=='2')
                echo '<center><h5>Greater Than '.$_POST['start'].' %</h5></center>';
            elseif($fl=='3')
                echo '<center><h5>Between '.$_POST['start'].'% to '.$_POST['end'].'%</h5></center>';

       echo " </strong>

                </div></br>";

    $crnt_sem_qry="SELECT currsem FROM student_info where batchyear='$batch' AND section='$student_section' LIMIT 1";
    $crntsem_res=mysqli_query($conn,$crnt_sem_qry);
    while($crntsem_data=mysqli_fetch_assoc($crntsem_res)){
        $current_sem=$crntsem_data['currsem'];
    }
    if($current_sem>2){
        $last_sem=($current_sem-1).'sem';
        $last_sem2=($current_sem-2).'sem';
    }
    else{
        $last_sem='1sem';
        $last_sem2='1sem';
    }

    $nstudent=$_POST['nstudent'];
    $filter=$_POST['filter'];
    switch($filter){
        case 1:	$start=-100;
            $end=$_POST['start'];
            break;
        case 2:	$start=$_POST['start'];
            $end=100;
            break;
        case 3: $start=$_POST['start'];
            $end=$_POST['end'];
            break;
        default: $start=-100;
            $end=100;

    }

$nstudent=$_POST['nstudent'];

$prev_sem=$current_sem-1;
    if($_POST['sort']=='ASC'){
        // echo $start;
        // echo "<br>";
        // echo $end;
     $sql="SELECT student_info.username,attendance_old.attendance,student_info.firstname,
            student_info.lastname,attendance.st_id,
            SUM(attendance.attended)/ SUM(attendance.totalclasses) * 100 - attendance_old.attendance as v1
            FROM attendance,student_info,attendance_old WHERE student_info.batchyear='$batch' 
            AND student_info.section='$student_section'  AND student_info.username=attendance.st_id 
            AND attendance_old.semester='$prev_sem' AND attendance_old.st_id=attendance.st_id
            AND (((attendance.attended / attendance.totalclasses) * 100) - attendance_old.attendance) BETWEEN $start AND $end
            GROUP BY student_info.username,attendance.st_id,attendance_old.st_id
            ORDER BY v1 ASC";
    }elseif ($_POST['sort']=='DEC') {
        $sql="SELECT student_info.username,attendance_old.attendance,student_info.firstname,
            student_info.lastname,attendance.st_id,
            SUM(attendance.attended) / SUM(attendance.totalclasses) * 100 - attendance_old.attendance as v1
            FROM attendance,student_info,attendance_old WHERE student_info.batchyear='$batch' 
            AND student_info.section='$student_section'  AND student_info.username=attendance.st_id 
            AND attendance_old.semester='$prev_sem' AND attendance_old.st_id=attendance.st_id
            AND (((attendance.attended / attendance.totalclasses) * 100) - attendance_old.attendance) BETWEEN $start AND $end
            GROUP BY student_info.username,attendance.st_id,attendance_old.st_id
            ORDER BY v1 DESC";
    }elseif ($_POST['sort']=='TOP') {
        $sql="SELECT student_info.username,attendance_old.attendance,student_info.firstname,
            student_info.lastname,attendance.st_id,
            SUM(attendance.attended) / SUM(attendance.totalclasses) * 100 - attendance_old.attendance as v1
            FROM attendance,student_info,attendance_old WHERE student_info.batchyear='$batch' 
            AND student_info.section='$student_section'  AND student_info.username=attendance.st_id 
            AND attendance_old.semester='$prev_sem' AND attendance_old.st_id=attendance.st_id
            AND (((attendance.attended / attendance.totalclasses) * 100) - attendance_old.attendance) BETWEEN $start AND $end
            GROUP BY student_info.username,attendance.st_id,attendance_old.st_id
            ORDER BY v1 DESC LIMIT $nstudent";
    }elseif ($_POST['sort']=='BOTTOM') {
        $sql="SELECT student_info.username,attendance_old.attendance,student_info.firstname,
            student_info.lastname,attendance.st_id,
            SUM(attendance.attended) / SUM(attendance.totalclasses) * 100 - attendance_old.attendance as v1
            FROM attendance,student_info,attendance_old WHERE student_info.batchyear='$batch' 
            AND student_info.section='$student_section'  AND student_info.username=attendance.st_id 
            AND attendance_old.semester='$prev_sem' AND attendance_old.st_id=attendance.st_id
            AND (((attendance.attended / attendance.totalclasses) * 100) - attendance_old.attendance) BETWEEN $start AND $end
            GROUP BY student_info.username,attendance.st_id,attendance_old.st_id
            ORDER BY v1 ASC LIMIT $nstudent";
    }else{
        $sql="SELECT distinct username FROM student_info where batchyear='$batch' AND section='$student_section' AND ($last_sem-$last_sem2) Between $start AND $end ORDER BY username ASC";
    }



    $getsql = mysqli_query( $conn , $sql);
    mysqli_num_rows($getsql).'ok';
    if(!$getsql )
    {
        die('Could not enter data: ' . mysql_error());
    }
    $i=0;

    $all_stu=[];
    $pk=1;


    while($row = mysqli_fetch_assoc($getsql)){
        $all_stu[]=$row['username'];
        //$user->id=$row['id'];
        $user->firstname=$row['firstname'];
        $user->lastname=$row['lastname'];
        $user->username=$row['username'];
        //$user->email=$row['email'];
        $u_st_id=$row['username'];
        $user_percentageinsemester = array();
        $qry_for_all_stu="SELECT * FROM attendance_old WHERE st_id='$u_st_id' ORDER BY semester ASC";
        $quest2_res=mysqli_query($conn,$qry_for_all_stu);
        $pk=[];
        while ($rust=mysqli_fetch_assoc($quest2_res)){
//            $true_str='user_percentageinsemester'.$pk;
            $pk[]=$rust['attendance'];
        }
        $user_percentageinsemester[1]=trim($pk[0]);
        $user_percentageinsemester[2]=trim($pk[1]);
        $user_percentageinsemester[3]=trim($pk[2]);
        $user_percentageinsemester[4]=trim($pk[3]);
        $user_percentageinsemester[5]=trim($pk[4]);
        $user_percentageinsemester[6]=trim($pk[5]);
        $user_percentageinsemester[7]=trim($pk[6]);
        $user_percentageinsemester[8]=trim($pk[7]);
         $qry_for_all_stu="SELECT * FROM attendance WHERE st_id='$u_st_id'";
            $quest2_res=mysqli_query($conn,$qry_for_all_stu);
            $attended=0;
            $totalclass = 0;
            while ($rust=mysqli_fetch_assoc($quest2_res)){
//            $true_str='user_percentageinsemester'.$pk;
                $attended += $rust['attended'];
                $totalclass += $rust['totalclasses'];
            }
            $per = round(($attended/$totalclass)*100,2);
            
           $user_percentageinsemester[$current_sem] = $per;

        $i++;
        $student[$i]=$user->username;
        $nullv=0;
        $user_section = $row['section'];

        $diff = round($user_percentageinsemester[$current_sem]- $user_percentageinsemester[$current_sem-1]);
            if($diff==-0)
                $diff=0;
        

        $total_marks[$student[$i]]=$diff;
    }

    //var_dump($total_marks);
    if($sort==NORMAL)
    {}
    else if($sort==ASC ||$sort==BOTTOM )
    {
        asort($total_marks);
        
    }
    else if($sort==DEC || $sort==TOP )
    {
       
        arsort($total_marks);
    }

    $z++;
    $n=1;
    echo "<table id='list'>";
    echo "<tr id='head'>
					<td>S.NO.</td>
					<td>Roll No.</td>
					<td>Name</td>
					<td>Sem</td>
					<td>Sem 1 Attend.</td>
					<td>Sem 2 Attend.</td>
					<td>Sem 3 Attend.</td>
					<td>Sem 4 Attend.</td>
					<td>Sem 5 Attend.</td>
					<td>Sem 6 Attend.</td>";
    if($branch1!='MCA'){
        echo "<td>Sem 7 Attend.</td>
						<td>Sem 8 Attend.</td>";
    }
    echo "<td>Attend Difference %</td>
					
				</tr>";
    $i=$z;

    foreach($total_marks as $x=>$x_value){
        if($x_value==0) continue;
        $i--;
        $bot[$i]=$x;
        if($i==1)
        {break;}
    }


    foreach($total_marks as $x=>$x_value){
        
        if(($filter==TOP || $filter==BOTTOM) && $n==$z)
        {break;}
        if($filter==BOTTOM){
            $student_id = $bot[$n];
        }
        else{
            $student_id = $x;
        }

        $qry2 = "SELECT * FROM student_info WHERE username='$student_id' LIMIT 1 ";

        $run2 = mysqli_query( $conn,$qry2 );
        if(!$run2 )
        {
            die('Could not enter data: ' . mysqli_error($conn));
        }



        $i=0;



        while($row2 = mysqli_fetch_assoc($run2)){
            $student_id=$row2['username'];
            $name=$row2['firstname']." ".$row2['lastname'];
            $user_student_number = $row2['studentnumber'];
            $user_section = trim($row2['section']);
            $user_branch = $row2['branch'];
            $user_semester = $row2['currsem'];

            $qry_for_all_stu="SELECT * FROM attendance_old WHERE st_id='$student_id' ORDER BY semester ASC";
            $quest2_res=mysqli_query($conn,$qry_for_all_stu);
            $pk=[];
            while ($rust=mysqli_fetch_assoc($quest2_res)){
//            $true_str='user_percentageinsemester'.$pk;
                $pk[]=$rust['attendance'];
            }

            $userper[1]=trim($pk[0]);
            $userper[2]=trim($pk[1]);
            $userper[3]=trim($pk[2]);
            $userper[4]=trim($pk[3]);
            $userper[5]=trim($pk[4]);
            $userper[6]=trim($pk[5]);
            $userper[7]=trim($pk[6]);
            $userper[8]=trim($pk[7]);
            $qry_for_all_stu="SELECT * FROM attendance WHERE st_id='$student_id'";
            $quest2_res=mysqli_query($conn,$qry_for_all_stu);
            $attended=0;
            $totalclass = 0;
            while ($rust=mysqli_fetch_assoc($quest2_res)){

                $attended += $rust['attended'];
                $totalclass += $rust['totalclasses'];
            }
            $per = round(($attended/$totalclass)*100,2);
            
            $userper[$current_sem] = $per;
            
            //$user_percentageinsemester_total=$userper[1]+$userper[2]+$userper[3]+$userper[4]+$userper[5]+$userper[6]+$userper[7]+$userper[8];
            //$user_percentageinsemester_average=number_format(($user_percentageinsemester_total/($nullv)),2);


            $marks_diff = round($userper[$user_semester] - $userper[$user_semester-1],2);
            if($marks_diff==-0)
                $marks_diff=0;
            
            $all_diff[]=$marks_diff;


if($marks_diff >=$start && $marks_diff <=$end )  {         
            echo "<tr>
					<td>$n</td>
					<td>$student_id</td>
					<td>$name</td>
					<td>$user_semester</td>
					<td>$userper[1]</td>
					<td>$userper[2]</td>
					<td>$userper[3]</td>
					<td>$userper[4]</td>
					<td>$userper[5]</td>
					<td>$userper[6]</td>";
            if($branch1!='MCA'){
                echo "<td>$userper[7]</td>
						<td>$userper[8]</td>";
            }

            echo "<td>".round($marks_diff,2)."</td>
					
				</tr>";
                $n++;
            
        }
    }
    }

    echo "</table>";
    $new_arr=[];
    $kk=0;
    foreach ($all_stu as $s) {
        $new_arr[]=['diff'=>$all_diff[$kk++],'st_id'=>$s];
    }
    if($_POST['sort']=='ASC'){
        sort($new_arr);
    }elseif($_POST['sort']=='DEC'){
        asort($new_arr);
    }


    $name=$_SESSION['name'];
    $username=$_SESSION['username'];
    $branch=$_SESSION['branch'];
    $category=$_SESSION['category'];
    $get_hodname="SELECT * FROM login WHERE userid='$username'";
    mysql_select_db('portal');
    $getit= mysqli_query( $conn ,$get_hodname );
    while($row = mysqli_fetch_assoc($getit)){
        $hodname=$row['name'];
        $branch=$row['branch'];
        $category=$row['category'];
    }

    $get_hodname="SELECT * FROM login WHERE branch='$branch' AND category='HOD'";

    $getit= mysqli_query( $conn,$get_hodname);
    while($row = mysqli_fetch_assoc($getit)){
        $h_name=$row['name'];
    }

    $query="SELECT * FROM coordinator WHERE fac_id='$username'";

    $exec= mysqli_query(  $conn,$query );
    $is_c=0;
    while($row = mysqli_fetch_assoc($exec)){
        $is_c=1;
    }
    echo "</br></br></br></br>";


    if($category=='FACULTY'){

        if($is_c==1){

            echo "<table class='last' id='heading'><tr><td><div id='heading' align='left'>";
            echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>(FACULTY COORDINATOR)</strong>";
            echo "</div></td>";
            echo "<td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><td><div id='heading' align='right'>";
            echo "<strong style=font-size:18px>$h_name</br>
					<strong style=font-size:18px>(HOD $branch)</strong>";
            echo "</div></td></tr></table>";

        }

        else{
            echo "<table class='last' id='heading'><tr><td><div id='heading' align='left'>";
            echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>(FACULTY)</strong>";
            echo "</div></td>";
            echo "<td><td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div id='heading' align='right'>";
            echo "<strong style=font-size:18px>$h_name</br>
					<strong style=font-size:18px>(HOD $branch)</strong>";
            echo "</div></td></tr></table>";

        }
    }
    else{

        echo "<div  align='right'>";
        echo "<strong style=font-size:18px>".$name."</br>
					<strong style=font-size:18px>( ".$category .$branch.")</strong>";
        echo "</div>";



    }

}


?>
</body>
</html>
