<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="../css/tab-view.css" />
<link rel="stylesheet" type="text/css" href="../css/new_layout.css">
<link rel="stylesheet" href="../css/print.css" type="text/css" media="print" /> 

</head>
<script>
  function printpage()
  {
    window.print();
  }
</script>
<?php

  include 'common.php';
  //echo "<div id='space'></br></br></br></br></br></br></div>";

function roman($num) 
{
    $n = intval($num);
    $res = '';
 
    /*** roman_numerals array  ***/
    $roman_numerals = array(
                'M'  => 1000,
                'CM' => 900,
                'D'  => 500,
                'CD' => 400,
                'C'  => 100,
                'XC' => 90,
                'L'  => 50,
                'XL' => 40,
                'X'  => 10,
                'IX' => 9,
                'V'  => 5,
                'IV' => 4,
                'I'  => 1);
 
    foreach ($roman_numerals as $roman => $number) 
    {
        $matches = intval($n / $number);
        $res .= str_repeat($roman, $matches);
        $n = $n % $number;
    }
 
    return $res;
    }


if (isset($_POST['submit'])&& isset($_POST['username']))
{
	include 'connect.php';

echo "<div id='space' <br><br><br><br><br>";
echo "<div align='center' >
    <strong style=font-size:20px>Ajay Kumar Garg Engineering College, Ghaziabad</strong></br></br>
    <strong style=font-size:18px>STUDENT INFORMATION</strong>
  </div>";
//echo "<div id='head1' align='center' style='background-color:#AAAAAA'><h1>PERSONAL INFORMATION</h1></div>";
echo "<div align='center' ><input type='button' value='PRINT THIS PAGE' onclick='printpage()' class='button' id='print'></div>";
echo "</br>";


	$username=$_POST['username'];

	$sql = "SELECT * FROM student_info WHERE username='$username'";
		
	$result = mysqli_query($conn, $sql);
	
	
	if ($result->num_rows < 1)
	{
		echo'NOT REGISTERED' ;
		exit();
	}
	else
	{  
	   $student_id=$username;

		$sql1 = "SELECT * FROM student_info WHERE username='$username'";

	    if  ($result1=mysqli_query($conn,$sql1))
	    
	       // if($result1->num_rows>0)
	        
                  
	            
           //  	while($row = mysqli_fetch_assoc($result1))
             		?>

             	
           <!--  	<tr>
              <th>NAME</th>
              <th>FATHER'S NAME</th>
              <th>ROLL NO.</th>
          </tr>-->

          
              <?php
                 if($result1->num_rows>0)
                 // Ifmysqli_num_rows($result)>0)
                   {
                     $row=mysqli_fetch_array($result1);
                     $d = (($row['1sem']+$row['2sem']+$row['3sem']+$row['4sem']+$row['5sem']+$row['6sem']+$row['7sem']+$row['8sem'])/2);
                    
                       

                ?>
                <table width='80%' id='table'>
      <tr id='head'>
        <td colspan='5' style='text-align:center'>PERSONAL DETAILS</td>
      </tr>
      <tr>
        <td rowspan='6'><div align='center' width="10%" height="10%">
        <?php

          
        ?>

          <img src='./../../assets/images/students/<?php echo $row['studentnumber'].".JPG" ?>' style="width: 150px ;" >
          </div>
        </td>
        <td id='design'>Name</td>
                  <td><?php echo $row['firstname']  ; ?></td> 
                  <td id='design'>FATHER'S NAME:</td>
                  <td><?php echo $row['fathername']; ?></td> 
          </tr>
           <tr>
                   <td id='design'>ROLL NO.</td>
                  <td><?php echo $row['username']; ?></td> 
                  <td id='design'>STUDENT NO.</td>
                  <td><?php echo $row['studentnumber']; ?></td> 
          </tr>
           <tr>
                   <td id='design'>BRANCH</td>
                  <?php $user_branch=$row['branch']; ?>
                  <td><?php echo $row['branch']; ?></td> 
                  <td id='design'>BATCH YEAR</td>
                  <td><?php echo $row['batchyear']; ?></td> 
          </tr>
           <tr>
                   <td id='design'>SECTION</td>
                  <td><?php echo $row['section']; ?></td> 
                  <td id='design'>SEMESTER</td>
                  <td><?php echo $row['currsem']; ?></td> 
          </tr>
           <tr>
                   <td id='design'>DOB</td>
                  <td><?php echo $row['dateofbirth']; ?></td> 
                  <td id='design'>CATEGORY</td>
                  <td><?php echo $row['category']; ?></td> 
          </tr>
           <tr>
                   <td id='design'>HOSTLER</td>
                  <td><?php echo $row['hostlerdaysc']; ?></td> 
                  <td id='design'>ENTRANCE EXAM RANK</td>
                  <td><?php echo $row['entranceexamrank']; ?></td> 
         
         </table>
         <table width='80%' id='table2'>
      <tr id='head'>
        <td colspan='5' style='text-align:center'>CONTACT DETAILS</td>
      </tr>
      <tr>
        <td id='design'>Contact No.</td>
                  <td><?php echo $row['contact']; ?></td> 
                  <td id='design'>PARENT'S CONTACT NO.</td>
                  <td><?php echo $row['parentcontact']; ?></td> 
          </tr>
           <tr>
                   <td id='design'>PERMANENT ADDRESS</td>
                  <td><?php echo $row['district']; ?></td> 
                  <td id='design'>EMAIL ID</td>
                  <td><?php echo $row['email']; ?></td> 
          </tr>


         </table>
        <table width='80%' id='table3'>
      <tr id='head'>
        <td colspan='8' style='text-align:center'>ACADEMIC PERFORMANCE</td>
      </tr>
                	
          <tr>
                   <td id='design' style='text-align:center'>10nth %</td>
                  <td colspan='2' style='text-align:center'><?php echo $row['10']; ?></td> 
                  <td id='design' style='text-align:center'>12th %</td>
                  <td colspan='2' style='text-align:center'><?php echo $row['12']; ?></td> 
                  <td id='design' style='text-align:center'>B.TECH %</td>
                  <td colspan='2' style='text-align:center'><?php echo $d; ?></td> 
          </tr>
          <tr id='subhead'>
        <td style='text-align:center'>SEM</td>
        <td style='text-align:center'>CT-I %</td>
        <td style='text-align:center'>CT-II %</td>
        <td style='text-align:center'>ST-I %</td>
        <td style='text-align:center'>ST-II %</td>
        <td style='text-align:center'>PUT %</td>
        <td style='text-align:center'>Final Marks %</td>
        <td style='text-align:center'>Attendance %</td>
      </tr>
       


        <?php
       

        if(($row['currsem']==1 || $row['currsem']==2) && $user_branch!='MCA')
   $get_hodname="SELECT * FROM login WHERE branch='AS-HU' AND category='HOD'";
else
  $get_hodname="SELECT * FROM login WHERE branch='$user_branch' AND category='HOD'";

  
  $getit= mysqli_query($conn, $get_hodname );
 
  while($row1 = mysqli_fetch_assoc($getit)){
   $h_name=$row1['name'];
   $h_branch=$row1['branch'];
  }



               
            }

            


        if($user_branch=='MCA')
        $n=6;
      else
        $n=8;
        for($i=1;$i<=$n;$i++){
      
      $getmarks="SELECT * FROM marks_old WHERE st_id='$student_id' AND semester='$i'";
      mysql_select_db('portal');
        $retval = mysqli_query(  $conn,$getmarks );
          while($row = mysqli_fetch_assoc($retval))
          {
            if($row['CT1']==NULL || $row['CT1']=='DB' || $row['CT1']=='PC' || $row['CT1']==' '){$CT1[$i]=$row['CT1'];}
            else{
            $CT1[$i]=round($row['CT1'],2);
}
            
            if($row['CT2']==NULL || $row['CT2']=='DB' || $row['CT2']=='PC' || $row['CT2']==' '){$CT2[$i]=$row['CT2'];}
            else
            $CT2[$i]=round($row['CT2'],2);
            
            if($row['ST1']==NULL || $row['ST1']=='DB' || $row['ST1']=='PC' || $row['ST1']==' '){$ST1[$i]=$row['ST1'];}
            else
            $ST1[$i]=round($row['ST1'],2);
            
            if($row['ST2']==NULL || $row['ST2']=='DB' || $row['ST2']=='PC' || $row['ST2']==' '){$ST2[$i]=$row['ST2'];}
            else
            $ST2[$i]=round($row['ST2'],2);
            
            if($row['PUT']==NULL || $row['PUT']=='DB' || $row['PUT']=='PC' || $row['PUT']==' '){$PUT[$i]=$row['PUT'];}
            else
            $PUT[$i]=round($row['PUT'],2);
          }
      $getattendance="SELECT * FROM attendance_old WHERE st_id='$student_id' AND semester='$i'";
      mysql_select_db('portal');
            $retval = mysqli_query( $conn, $getattendance );
          while($row = mysqli_fetch_assoc($retval))
          {$attendance[$i]=round($row['attendance'],2);}
        if($i==$user_semester)
        {
        $qry="SELECT SUM(attended), SUM(totalclasses) FROM attendance WHERE st_id='$student_id'";
        $rqry=mysqli_query($qry,$conn);
        $x=0;$y=0;$s1=0;$s2=0;
        while($row = mysqli_fetch_assoc($rqry))
        {
          $x=$row[0];
          $y=$row[1];
          $s1+=$row[0];
          $s2+=$row[1];
        }
        $attendance[$i]=round(($s1/$s2)*100,2);
        }

        if($attendance[$i]==0.00)
            $attendance[$i]=" ";  
        echo "<tr>
          <td style='text-align:center'><b style='font-size:14px'>";echo roman($i); echo "</b></td>
          <td style='text-align:center'>$CT1[$i]</td>
          <td style='text-align:center'>$CT2[$i]</td>
          <td style='text-align:center'>$ST1[$i]</td>
          <td style='text-align:center'>$ST2[$i]</td>
          <td style='text-align:center'>$PUT[$i]</td>
          <td style='text-align:center'>$userper[$i]</td>
          <td style='text-align:center'>$attendance[$i]</td>
      </tr>";
      }
      
      
  echo "</table>";

  echo "<table width='80%' id='table5'>
      <tr id='head'>
        <td colspan='5' style='text-align:center'>Current Semester Attendance</td>
      </tr>
      <tr id='subhead'>
        <td width='20%'>Till ST1</td>
        <td width='20%'>During ST2</td>
        <td width='20%'>Till ST2</td>
        <td width='20%'>During PUT</td>
        <td width='20%'>Till PUT</td>
      </tr>";
    $sql="SELECT * FROM course_setting";
    mysql_select_db('portal');
    $execute = mysqli_query(  $conn,$sql );
    while($row = mysqli_fetch_assoc($execute)){
      $exam[]= date("Y-m-d", strtotime($row['upto_date']));
    }
   $st1_date=$exam['0'];
    $st2_date=$exam['1'];
    $put_date=$exam['2'];

     $sql="SELECT * FROM attendance where st_id='$student_id' AND todate<='$st1_date'";
    mysql_select_db('portal');
    $execute = mysqli_query($conn,$sql );
    $total=0;
    $attended=0;
    while($row = mysqli_fetch_assoc($execute)){
      $total+=$row['totalclasses'];
      $attended+=$row['attended'];
    }

   $attended;
  $total;
  $till_st1=round($attended/$total*100,2);

   $sql="SELECT * FROM attendance where st_id='$student_id' AND todate<='$st2_date' AND fromdate>='$st1_date'";
    mysql_select_db('portal');
    $execute = mysqli_query( $conn,$sql );
    $total=0;
    $attended=0;
    while($row = mysqli_fetch_assoc($execute)){
     $total+=$row['totalclasses'];
       $attended+=$row['attended'];
    }
    $during_st2=round($attended/$total*100,2);

    $sql="SELECT * FROM attendance where st_id='$student_id' AND todate<='$st2_date'";
    mysql_select_db('portal');
    $execute = mysqli_query( $conn,$sql);
    $total=0;
    $attended=0;
    while($row = mysqli_fetch_assoc($execute)){
      $total+=$row['totalclasses'];
      $attended+=$row['attended'];
    }
    $till_st2=round($attended/$total*100,2);

    $sql="SELECT * FROM attendance where st_id='$student_id' AND todate<='$put_date' AND fromdate>='$st2_date'";
    mysql_select_db('portal');
    $execute = mysqli_query( $conn,$sql );
    $total=0;
    $attended=0;
    while($row = mysqli_fetch_assoc($execute)){
      $total+=$row['totalclasses'];
      $attended+=$row['attended'];
    }
    $during_put=round($attended/$total*100,2);

    $sql="SELECT * FROM attendance where st_id='$student_id' AND todate<='$put_date'";
    mysql_select_db('portal');
    $execute = mysqli_query( $conn,$sql );
    $total=0;
    $attended=0;
    while($row = mysqli_fetch_assoc($execute)){
      $total+=$row['totalclasses'];
      $attended+=$row['attended'];
    }


    $till_put=round($attended/$total*100,2);

    if($till_st1==0.00){
      $till_st1='';
    }
    if($till_st2==0.00){
      $till_st2='';
    }
    if($till_put==0.00){
      $till_put='';
    }
    if($during_st2==0.00){
      $during_st2='';
    }
    if($during_put==0.00){
      $during_put='';
    }

    echo "<tr>
        <td width='20%'>$till_st1</td>
        <td width='20%'>$during_st2</td>
        <td width='20%'>$till_st2</td>
        <td width='20%'>$during_put</td>
        <td width='20%'>$till_put</td>
      </tr>
      ";

  echo "</table>";
  echo "<br><br><br><br><br>";
  echo "<table width='80%' id='table6'>";
  echo "<tr id='head'>
      <td colspan='4' style='text-align:center'>Carry-over Details</td>
    </tr>";
  echo "<tr id='subhead'>
      <td width='20%'>Semester</td>
      <td width='40%'>Subject(s)</td>
      <td width='40%'>current status</td>
    </tr>";
  for($i=1;$i<=$n;$i++)
  {
    echo "<tr>
        <td width='15%'>$i</td>
        <td width='35%' style='text-align:top'>";
          $sub="SELECT * FROM carry WHERE st_id='$student_id' AND semester='$i' ORDER BY sub_id ASC";
          mysql_select_db('portal');
          $getsub = mysqli_query( $conn,$sub );
          $j=0;
          while($row = mysqli_fetch_assoc($getsub)){
          $j++;
          $subject=$row['sub_id'];
          echo "$j. $subject</br>";
          }
          
          
        echo "</td>
        
        <td width='30%' style='text-align:top'>";
          $sub="SELECT * FROM carry WHERE st_id='$student_id' AND semester='$i' ORDER BY sub_id ASC";
          mysql_select_db('portal');
          $getsub = mysqli_query( $conn,$sub);
          while($row = mysqli_fetch_assoc($getsub)){
          $status=$row['status'];
          echo "$status</br>";
          }
        echo "</td>";
          
        
        echo "</tr>";
  }
  echo "</table>";
  


              
              
              
  echo "<table width='80%' id='table5'>
      <tr id='head'>
        <td colspan='4' style='text-align:center'>Disciplinary Actions</td>
      </tr>
      <tr id='subhead'>
        <td width='10%'>S.No.</td>
        <td width='15%'>Date</td>
        <td width='40%'>Indisciplinary Case</td>
        <td width='35%'>Acction Taken</td>
      </tr>";
    $action="SELECT * FROM action WHERE st_id='$student_id'";
    mysql_select_db('portal');
    $getaction = mysqli_query( $conn,$action );
    $serial1=0;
    while($row = mysqli_fetch_assoc($getaction)){
      $serial1++;
      $date=$row['date'];
      $case=$row['case'];
      $action_taken=$row['action'];
      echo "<tr>
        <td width='10%'>$serial1</td>
        <td width='15%'>$date</td>
        <td width='40%'>$case</td>
        <td width='35%'>$action_taken</td>
      </tr>"; 
    }
    if($serial1==0){
      echo "<tr><td colspan='4' style='color:red'>*NOT HAVING ANY DISPLINARY ACTION</td></tr>";
    }
  echo "</table>";
  
  
  
  
echo "<table width='80%' id='table4'>
      <tr id='head'>
        <td colspan='5' style='text-align:center'>Extra/Co-Curricular Detail</td>
      </tr>
      <tr id='subhead'>
        <td width='10%'>S.No.</td>
        <td width='15%'>Date</td>
        <td width='20%'>Event</td>
        <td width='20%'>Participation Type</td>
        <td width='35%'>Description</td>
      </tr>";
    $award="SELECT * FROM award WHERE st_id='$student_id'";
    mysql_select_db('portal');
    $getaward = mysqli_query( $conn,$award );
    $serial2=0;
    while($row = mysqli_fetch_assoc($getaward)){
      $serial2++;
      $date=$row['date'];
      $event=$row['event'];
      $participation_type=$row['participation_type'];
      $description=$row['description'];
      echo "<tr>
        <td width='10%'>$serial2</td>
        <td width='15%'>$date</td>
        <td width='20%'>$event</td>
        <td width='20%'>$participation_type</td>
        <td width='35%'>$description</td>
      </tr>"; 
    }
    if($serial2==0){
      echo "<tr><td colspan='5' style='color:red'>*NOT HAVING ANY EXTRA/CO-CURRICULAR ACTIVITY</td></tr>";
    }
  echo "</table>";
  
  

  echo "<table width='80%' id='table5'>
      <tr id='head'>
        <td colspan='4' style='text-align:center'>PARENTS MEETING</td>
      </tr>
      <tr id='subhead'>
        <td width='10%'>S.No.</td>
        <td width='15%'>Date</td>
        <td width='40%'>Reason</td>
        <td width='35%'>Description</td>
      </tr>";
    $action="SELECT * FROM parents_meeting WHERE st_id='$student_id'";
    mysql_select_db('portal');
    $getaction = mysqli_query( $conn,$action);
    $serial1=0;
    while($row = mysqli_fetch_assoc($getaction)){
      $serial1++;
      $date=$row['date'];
      $reason=$row['reason'];
      $description=$row['description'];
      echo "<tr>
        <td width='10%'>$serial1</td>
        <td width='15%'>$date</td>
        <td width='40%'>$reason</td>
        <td width='35%'>$description</td>
      </tr>"; 
    }
    if($serial1==0){
      echo "<tr><td colspan='4' style='color:red'>*NOT HAVING ANY PARENT'S MEETING</td></tr>";
    }
  echo "</table>";  

if($h_branch=='AS-HU')
  echo "<br><br><br><div align='left'><b>$h_name<br>DEAN 1ST YEAR</b></div>";
  else
  echo "<br><br><br><div align='left'><b>$h_name<br>HoD $h_branch </b></div>";
echo "</div>"; 
  
        }
    }

?>