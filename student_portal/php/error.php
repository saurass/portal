<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/new_layout.css">
</head>
</html>

<?php
include 'connect.php';
include 'menu.php';
require_once('../../config.php');
require_login();

global $USER;
$userid = $USER->id;
$username = $USER->username;


if((strlen($username))<=8)
{
	header('Location:index.php');
}
function roman($num) 
{
    $n = intval($num);
    $res = '';
 
    /*** roman_numerals array  ***/
    $roman_numerals = array(
                'M'  => 1000,
                'CM' => 900,
                'D'  => 500,
                'CD' => 400,
                'C'  => 100,
                'XC' => 90,
                'L'  => 50,
                'XL' => 40,
                'X'  => 10,
                'IX' => 9,
                'V'  => 5,
                'IV' => 4,
                'I'  => 1);
 
    foreach ($roman_numerals as $roman => $number) 
    {
        $matches = intval($n / $number);
        $res .= str_repeat($roman, $matches);
        $n = $n % $number;
    }
 
    return $res;
    }
	

$student_id = $username;

global $DB;

$user = $DB->get_record('user', array('username'=>$student_id));
$user_student_number = $DB->get_field('user_info_data','data',array('fieldid'=>2,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_10tdmarks = $DB->get_field('user_info_data','data',array('fieldid'=>25,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_12tdmarks = $DB->get_field('user_info_data','data',array('fieldid'=>26,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress1 = $DB->get_field('user_info_data','data',array('fieldid'=>13,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress2 = $DB->get_field('user_info_data','data',array('fieldid'=>14,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress3 = $DB->get_field('user_info_data','data',array('fieldid'=>15,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress4 = $DB->get_field('user_info_data','data',array('fieldid'=>16,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress5 = $DB->get_field('user_info_data','data',array('fieldid'=>17,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress6 = $DB->get_field('user_info_data','data',array('fieldid'=>18,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentcontactnumber = $DB->get_field('user_info_data','data',array('fieldid'=>3,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_fatdersname = $DB->get_field('user_info_data','data',array('fieldid'=>8,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_parentscontactnumber = $DB->get_field('user_info_data','data',array('fieldid'=>9,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansname = $DB->get_field('user_info_data','data',array('fieldid'=>10,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardianscontactnumber = $DB->get_field('user_info_data','data',array('fieldid'=>11,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress1 = $DB->get_field('user_info_data','data',array('fieldid'=>19,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress2 = $DB->get_field('user_info_data','data',array('fieldid'=>20,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress3 = $DB->get_field('user_info_data','data',array('fieldid'=>21,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress4 = $DB->get_field('user_info_data','data',array('fieldid'=>22,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress5 = $DB->get_field('user_info_data','data',array('fieldid'=>23,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress6 = $DB->get_field('user_info_data','data',array('fieldid'=>24,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_branch = $DB->get_field('user_info_data','data',array('fieldid'=>39,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_semester = $DB->get_field('user_info_data','data',array('fieldid'=>27,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_dateofbirtd = $DB->get_field('user_info_data','data',array('fieldid'=>4,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_dateofbirtd_to_date = date('jS F Y ', $user_dateofbirtd);

$userper[1]= $DB->get_field('user_info_data','data',array('fieldid'=>28,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[1]==NULL)
$nullv++;
$userper[2]= $DB->get_field('user_info_data','data',array('fieldid'=>29,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[2]==NULL)
$nullv++;
$userper[3]= $DB->get_field('user_info_data','data',array('fieldid'=>30,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[3]==NULL)
$nullv++;
$userper[4] = $DB->get_field('user_info_data','data',array('fieldid'=>31,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[4]==NULL)
$nullv++;
$userper[5] = $DB->get_field('user_info_data','data',array('fieldid'=>32,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[5]==NULL)
$nullv++;
$userper[6] = $DB->get_field('user_info_data','data',array('fieldid'=>33,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[6]==NULL)
$nullv++;
$userper[7] = $DB->get_field('user_info_data','data',array('fieldid'=>34,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[7]==NULL)
$nullv++;
$userper[8] = $DB->get_field('user_info_data','data',array('fieldid'=>35,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[8]==NULL)
$nullv++;

$user_percentageinsemester_total=$userper[1]+$userper[2]+$userper[3]+$userper[4]+$userper[5]+$userper[6]+$userper[7]+$userper[8];
$user_percentageinsemester_average=round(($user_percentageinsemester_total/(8-$nullv)),2);

$user_section = $DB->get_field('user_info_data','data',array('fieldid'=>38,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_batch = $DB->get_field('user_info_data','data',array('fieldid'=>1,'userid'=>$user->id), $strictness=IGNORE_MISSING);
echo "</br></br></br></br></br></br>";

echo "<div>";
echo "<table width='80%' id='table1'>
			<tr id='head'>
				<td colspan='5' style='text-align:center'>PERSONAL DETAILS</td>
			</tr>
			<tr>
				<td rowspan='5'><div align='center'>";
					global $USER, $COURSE;
					$user_picture=new user_picture($user);
					$src=$user_picture->get_url($PAGE);
					$src = str_replace("f2?rev=","f1?rev=",$src);
					echo '<img src="'.$src.'" width="150" height="150">';

					echo "</div>
				</td>
				<td id='design'>Name</td>
				<td>$user->firstname $user->lastname</td>
				<td id='design'>Father's Name</td>
				<td>$user_fatdersname</td>
			</tr>
			<tr>
				<td id='design'>Roll No.</td>
				<td>$student_id</td>
				<td id='design'>Student No.</td>
				<td>$user_student_number</td>
			</tr>
			<tr>
				<td id='design'>Branch</td>
				<td>$user_branch</td>
				<td id='design'>Batch Year</td>
				<td>$user_batch</td>
			</tr>
			<tr>
				<td id='design'>Section</td>
				<td>$user_section</td>
				<td id='design'>Semester</td>
				<td>$user_semester</td>
			</tr>
			<tr>
				<td id='design'>DOB</td>
				<td>$user_dateofbirtd_to_date</td>
				<td id='design'>Email id</td>
				<td>$user->email</td>
			</tr>
	</table>";
	
echo "<table width='80%' id='table2'>
			<tr id='head'>
				<td colspan='5' style='text-align:center'>CONTACT DETAILS</td>
			</tr>
			<tr>
				<td id='design'>Contact No.</td>
				<td>$user_permanentcontactnumber</td>
				<td id='design'>Parent's Contact No.</td>
				<td>$user_parentscontactnumber</td>
			</tr>
			<tr>
				<td id='design'>Permanent Address</td>
				<td>$user_permanentaddress1<br>$user_permanentaddress2<br>$user_permanentaddress3<br>$user_permanentaddress4<br>$user_permanentaddress5<br>$user_permanentaddress6</td>
				<td id='design'>Local Address</td>
				<td>$user_localguardiansaddress1<br>$user_localguardiansaddress2<br>$user_localguardiansaddress3<br>$user_localguardiansaddress4<br>$user_localguardiansaddress5<br>$user_localguardiansaddress6</td>
			</tr>
	</table>";
	
echo "<table width='80%' id='table3'>
			<tr id='head'>
				<td colspan='8' style='text-align:center'>ACADEMIC PERFORMANCE</td>
			</tr>
			<tr>
				<td id='design'>10th %</td>
				<td colspan='2'>$user_10tdmarks %</td>
				<td id='design'>12th %</td>
				<td colspan='2'>$user_12tdmarks %</td>
				<td id='design'>B.Tech %</td>
				<td>$user_percentageinsemester_average %</td>
			</tr>
			<tr id='subhead'>
				<td>SEM</td>
				<td>CT-I %</td>
				<td>CT-II %</td>
				<td>ST-I %</td>
				<td>ST-II %</td>
				<td>PUT %</td>
				<td>Final Marks %</td>
				<td>Attendance %</td>
			</tr>
			<tr>";
				for($i=1;$i<=8;$i++){
			
			$getmarks="SELECT * FROM marks_old WHERE st_id='$student_id' AND semester='$i'";
			mysql_select_db('portal');
						$retval = mysql_query( $getmarks, $conn );
					while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
					{
						if($row['CT1']==NULL || $row['CT1']=='DB' || $row['CT1']=='PC' || $row['CT1']==' '){$CT1[$i]=$row['CT1'];}
						else{
						$CT1[$i]=round($row['CT1'],2);
}
						
						if($row['CT2']==NULL || $row['CT2']=='DB' || $row['CT2']=='PC' || $row['CT2']==' '){$CT2[$i]=$row['CT2'];}
						else
						$CT2[$i]=round($row['CT2'],2);
						
						if($row['ST1']==NULL || $row['ST1']=='DB' || $row['ST1']=='PC' || $row['ST1']==' '){$ST1[$i]=$row['ST1'];}
						else
						$ST1[$i]=round($row['ST1'],2);
						
						if($row['ST2']==NULL || $row['ST2']=='DB' || $row['ST2']=='PC' || $row['ST2']==' '){$ST2[$i]=$row['ST2'];}
						else
						$ST2[$i]=round($row['ST2'],2);
						
						if($row['PUT']==NULL || $row['PUT']=='DB' || $row['PUT']=='PC' || $row['PUT']==' '){$PUT[$i]=$row['PUT'];}
						else
						$PUT[$i]=round($row['PUT'],2);
					}
			$getattendance="SELECT * FROM attendance_old WHERE st_id='$student_id' AND semester='$i'";
			mysql_select_db('portal');
						$retval = mysql_query( $getattendance, $conn );
					while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
					{$attendance[$i]=round($row['attendance'],2);}
				if($i==$user_semester)
				{
				$qry="SELECT SUM(attended), SUM(totalclasses) FROM attendance WHERE st_id='$student_id'";
				$rqry=mysql_query($qry,$conn);
				$x=0;$y=0;
				while($row = mysql_fetch_array($rqry))
				{
					$x=$row[0];
					$y=$row[1];
					$s1+=$row[0];
					$s2+=$row[1];
				}
				$attendance[$i]=round(($s1/$s2)*100,2);

				$query="SELECT setting FROM allow WHERE type='T' order by setting ASC";
				$execute= mysql_query( $query, $conn );
				while($row = mysql_fetch_array($execute)){
				$examtype=$row[0];
			
					$getsub="SELECT DISTINCT(subject.sub_id),subject.category FROM assignrole,subject WHERE subject.sub_id=assignrole.sub_id AND assignrole.semester='$i' AND assignrole.section='$user_section'  ORDER BY assignrole.sub_id";	
						$get= mysql_query( $getsub, $conn );
					$avg_total = '';
					while($row = mysql_fetch_array($get, MYSQL_ASSOC)){
						
						$subjectid=$row['sub_id'];
						$cat=$row['category'];
								
						if($cat=='T' || $cat=='O'){
									$getr="SELECT * FROM marks WHERE st_id='$student_id' AND sub_id='$subjectid'";
							mysql_select_db('portal');
							$getre= mysql_query( $getr, $conn );
							while($row = mysql_fetch_array($getre, MYSQL_ASSOC)){
								if($examtype=='CT1')
								$e='exam1';
								if($examtype=='CT2')
								$e='exam2';
								if($examtype=='ST1')
								$e='exam3';
								if($examtype=='ST2')
								$e='exam4';
								if($examtype=='PUT')
								$e='exam5';
								if($row[$e] =='DB')
								{	$avg_total='DB';}
								else if($row[$e] =='PC')
								{	$avg_total='PC';}
								else
									echo "";
								$marks=$row[$e];
								$total = $total+$marks;
								if($marks=='DB')
								{
									$remark[$uni_no]=1;
								}
								else if($marks=='PC')
								{
									$remark[$uni_no]=2;
								}
								else if($marks=='A')
								{
									$remark[$uni_no]=3;
								}
								if($marks!=NULL)
								{
									
                                 /*     include('marks.php');

								if($examtype=='CT1' )                
									$max=marks_t('CT1');

								elseif($examtype=='CT2')
									$max=marks_t('CT2');

								elseif($examtype=='ST1' )
									$max=marks_t('ST1');

								elseif($examtype=='ST2')
									$max=marks_t('ST2');

								elseif($examtype=='PUT')
								    $max=marks_t('PUT',$subjectid);*/
								
									if($examtype=='CT1')
									 $max=10;
									elseif($examtype=='CT2')
									$max=10;
									elseif($examtype=='ST1')
									$max=100;
									elseif($examtype=='ST2')
									$max=30;
									elseif($examtype=='PUT')
									{
										$query="SELECT marks FROM subject WHERE sub_id='$subjectid'"; 
										$get_query= mysql_query( $query, $conn );
										while($row2 = mysql_fetch_array($get_query, MYSQL_ASSOC)){
											$max=$row2['marks'];
										}
									}
									$checksome+=$max;
								}
							}
						}
							
					}
						if($avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
						$avg_total=number_format($total/$checksome*100,2);
						$total = 0;
							$checksome = 0;
							if($examtype=='CT1'){
								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
									$CT1[$i]=$avg_total;}

							if($examtype=='CT2')
									{
								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$CT2[$i]=$avg_total;}

							if($examtype=='ST1')
									{
								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$ST1[$i]=$avg_total;}

							if($examtype=='ST2')
									{
								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$ST2[$i]=$avg_total;}

							if($examtype=='PUT')
								{
								if($avg_total==0.00&&$avg_total!='PC' && $avg_total!='DB' && $avg_total!='A')
									$avg_total=" ";
								$PUT[$i]=$avg_total;} 
								
				}
			}	
				echo "<tr>
					<td><b style='font-size:14px'>";echo roman($i); echo "</b></td>
					<td>$CT1[$i]</td>
					<td>$CT2[$i]</td>
					<td>$ST1[$i]</td>
					<td>$ST2[$i]</td>
					<td>$PUT[$i]</td>
					<td>$userper[$i]</td>
					<td>$attendance[$i]</td>
			</tr>";
			}
			
			
	echo "</table>";
	
echo "<table width='80%' id='table5'>
			<tr id='head'>
				<td colspan='4' style='text-align:center'>Disciplinary Actions</td>
			</tr>
			<tr id='subhead'>
				<td width='10%'>S.No.</td>
				<td width='15%'>Date</td>
				<td width='40%'>Indisciplinary Case</td>
				<td width='35%'>Acction Taken</td>
			</tr>";
		$action="SELECT * FROM action WHERE st_id='$student_id'";
		mysql_select_db('portal');
		$getaction = mysql_query( $action, $conn );
		$serial1=0;
		while($row = mysql_fetch_array($getaction, MYSQL_ASSOC)){
			$serial1++;
			$date=$row['date'];
			$case=$row['case'];
			$action_taken=$row['action'];
			echo "<tr>
				<td width='10%'>$serial1</td>
				<td width='15%'>$date</td>
				<td width='40%'>$case</td>
				<td width='35%'>$action_taken</td>
			</tr>";	
		}
		if($serial1==0){
			echo "<tr><td colspan='4' style='color:red'>*NOT HAVING ANY DISPLINARY ACTION</td></tr>";
		}
	echo "</table>";
	
	
	
	
echo "<table width='80%' id='table4'>
			<tr id='head'>
				<td colspan='5' style='text-align:center'>Extra/Co-Curricular Detail</td>
			</tr>
			<tr id='subhead'>
				<td width='10%'>S.No.</td>
				<td width='15%'>Date</td>
				<td width='20%'>Event</td>
				<td width='20%'>Participation Type</td>
				<td width='35%'>Description</td>
			</tr>";
		$award="SELECT * FROM award WHERE st_id='$student_id'";
		mysql_select_db('portal');
		$getaward = mysql_query( $award, $conn );
		$seria2=0;
		while($row = mysql_fetch_array($getaward, MYSQL_ASSOC)){
			$serial2++;
			$date=$row['date'];
			$event=$row['event'];
			$participation_type=$row['participation_type'];
			$description=$row['description'];
			echo "<tr>
				<td width='10%'>$serial2</td>
				<td width='15%'>$date</td>
				<td width='20%'>$event</td>
				<td width='20%'>$participation_type</td>
				<td width='35%'>$description</td>
			</tr>";	
		}
		if($serial2==0){
			echo "<tr><td colspan='5' style='color:red'>*NOT HAVING ANY EXTRA/CO-CURRICULAR ACTIVITY</td></tr>";
		}
	echo "</table>";

echo "</div>";

?>
