<?php session_start();
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/marks_layout.css">
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="print" /> 
</head>
<script>
	function printpage()
  {
  window.print()
  }
  
  function showbatch()
	{
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showbatch.php",true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("batch").innerHTML=xmlhttp.responseText;
			}
		}
	}



  function showsection(str)
	{
		
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showsection.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("section").innerHTML=xmlhttp.responseText;
			}
		}
	}
	function slt(val){
if(val=='TOP' || val=='BOTTOM'){
document.getElementById("bth").style.visibility='visible';
document.getElementById("bth2").style.visibility='visible';
}
else{
document.getElementById("bth").style.visibility='hidden';
document.getElementById("bth2").style.visibility='hidden';
}}
function init(){
document.getElementById("bth").style.visibility='hidden';
document.getElementById("bth2").style.visibility='hidden';
}
 </script>
</html>


<?php
include 'common.php';
include 'connect.php';

echo "<div id='space'></br></br></br></br></br></br></br></div>";
?>
<html>
<body onload="init();showbatch();">
<div style="font-size:18;font-weight:200%; margin-top:-5%; margin-bottom:4%;" align="center"> SECTION-WISE FINAL MARKS</div>
	<div align="center"><form action="view_report_card_custom_information_marks.php" method="post">
	<table id='except'>
			<tr>
			<td>batch:</td>
			<td>
				<select size="1" name="batch" id="batch" required="required" onchange="showsection(this.value)" required>
		
	</select>
				
			</td>
		</tr>
		<tr>
			<td>
			Section</td>
			<td >
			<select size="1" name="section" id="section" onchange="showsubjectid(this.value)" required></select></td>
		</tr>
			<tr>
				<td>FILTER:</td>
				<td colspan='2'>
					<select name='filter' onchange=slt(this.value);>
						<option value='NORMAL'>Normal Order</option>
						<option value='DEC'>Descending Order</option>
						<option value='ASC'>Ascending Order</option>
						<option value='TOP'>Top N</option>
						<option value='BOTTOM'>Bottom N</option>
					</select>
				</td>
			</tr>
			<tr>
				<td id='bth'>NUMBER OF STUDENT:</td>
				<td id='bth2'><input type="text" name="nstudent" size="1" /></td>
				<td ><input type="submit" value="Submit" class="button" name="submit" ></td>
			</tr>
	</table></form></div>
	<?php
		if(isset($_POST['submit'])){
			$batch = $_POST['batch'];
			$student_section = $_POST['section'];
			$filter=$_POST['filter'];
			$z=$_POST['nstudent'];
			$k=1;

/////////////////////////////////////////////////////////////////////////
	// 		$checksub="SELECT * FROM mdl_user_info_data,mdl_user WHERE mdl_user.id=mdl_user_info_data.userid AND mdl_user_info_data.fieldid=1 AND mdl_user_info_data.data='$batch' ORDER BY mdl_user.username ASC";
	// mysql_select_db('moodle');
	// $retval = mysql_query( $checksub, $conn );
	// $count=0;
	// while($row = mysql_fetch_array($retval, MYSQL_ASSOC)){
	// 	$count++;
	// 	$uid=$row['userid'];
	// 	//echo "<option value=$uid>$uid</option>";
	// 	$query="SELECT * FROM mdl_user_info_data WHERE userid='$uid' AND fieldid='27'";
	// 	mysql_select_db('moodle');
	// 	$run = mysql_query( $query, $conn );
	// 	while($row2 = mysql_fetch_array($run, MYSQL_ASSOC)){
	// 		if(!empty($row2['data']))
	// 			$semester=$row2['data'];
	// 		//echo "<option value=$row2[data]>$row2[data]</option>";
	// 	}
	// 	//echo "<option value=$row[section_alloted]>$row[section_alloted]</option>";
	// }
			
			/////////////////////////////////////////////////////////////

			$get_branch="SELECT DISTINCT branch FROM student_info WHERE section='$student_section' AND batchyear='$batch'";
			
			$getit= mysqli_query($conn,$get_branch );
			while($row = mysqli_fetch_assoc($getit)){
			$branch1=$row['branch'];
			}

			if($branch1=='CSE'){
			$fullname='COMPUTER SCIENCE AND ENGINEERING';	
			}
			else if($branch1=='IT'){
			$fullname='INFORMATION TECHNOLOGY';
			}
			else if($branch1=='ME'){
			$fullname='MECHANICAL ENGINEERING';
			}
			else if($branch1=='ECE'){
			$fullname='ELECTRONICS & COMMUNICATION ENGINEERING';
			}
			else if($branch1=='EI'){
			$fullname='ELECTRONICS & INSTRUMENTATION ENGINEERING';
			}
			else if($branch1=='EN'){
			$fullname='ELECTRICAL & ELECTRONICS ENGINEERING';
			}
			else if($branch1=='CE'){
			$fullname='CIVIL ENGINEERING';
			}
			else if($branch1=='AS-HU'){
			$fullname='APPLIED SCIENCE AND HUMANITIES';
			}
			else if($branch1=='MCA'){
			$fullname='MASTER OF COMPUTER APPLICATION';
			}
			echo "<div align='center' ><input type='button' value='PRINT THIS PAGE' onclick='printpage()' class='button' id='print'></div>";
			echo "<div id='heading' align='center'>
					<div align='right'><p style=font-size:14px>AKGEC/MR/FM/09</p></div>
					<strong style=font-size:20px>Ajay Kumar Garg Engineering College, Ghaziabad</strong></br>
					<strong style=font-size:15px>Branch: $fullname (Section-$student_section)</strong></br>
					<strong style=font-size:15px>Batch: $batch</strong></br>
					<strong style=font-size:18px>STUDENT MARKS LIST";
					if($filter=='NORMAL')
					$show='NORMAL ORDER';
					else if($filxter=='DEC')
					$show='DESECENDING ORDER';
					else if($filter=='ASC')
					$show='ASECENDING ORDER';
					else if($filter=='TOP')
					$show='TOP '.$z;
					else if($filter=='BOTTOM')
					$show='BOTTOM '.$z;
					echo "($show)</strong>
				</div></br>";
			
			
			$sql="SELECT * FROM student_info where batchyear='$batch' AND section='$student_section' ORDER BY username ASC";
			
			$getsql = mysqli_query( $conn , $sql);
			if(!$getsql )
					{
					  die('Could not enter data: ' . mysql_error());
					}
			$i=0;
			
			while($row = mysqli_fetch_assoc($getsql)){
				$user->id=$row['id'];
				$user->firstname=$row['firstname'];
				$user->lastname=$row['lastname'];
				$user->username=$row['username'];
				$user->email=$row['email'];
				
			
				
					$i++;
			$student[$i]=$user->username;
			$nullv=0;
			$user_section = $row['section'];
			$user_percentageinsemester = array();
			 $user_percentageinsemester[1] = trim($row['1sem']);
			if($user_percentageinsemester[1]!=NULL && is_numeric($user_percentageinsemester[1]) || strpos($user_percentageinsemester[1],'%'))
			$nullv++;
			$user_percentageinsemester[2] = trim($row['2sem']);
			if($user_percentageinsemester[2]!=NULL && is_numeric($user_percentageinsemester[2]) || strpos($user_percentageinsemester[2],'%'))
			$nullv++;
			$user_percentageinsemester[3] = trim($row['3sem']);
			if($user_percentageinsemester[3]!=NULL && is_numeric($user_percentageinsemester[3]) || strpos($user_percentageinsemester[3],'%'))
			$nullv++;
			$user_percentageinsemester[4] = trim($row['4sem']);
			if($user_percentageinsemester[4]!=NULL && is_numeric($user_percentageinsemester[4]) || strpos($user_percentageinsemester[4],'%'))
			$nullv++;
			$user_percentageinsemester[5] = trim($row['5sem']);
			if($user_percentageinsemester[5]!=NULL && is_numeric($user_percentageinsemester[5]) || strpos($user_percentageinsemester[5],'%'))
			$nullv++;
			$user_percentageinsemester[6] = trim($row['6sem']);
			if($user_percentageinsemester[6]!=NULL && is_numeric($user_percentageinsemester[6]) || strpos($user_percentageinsemester[6],'%'))
			$nullv++;
			$user_percentageinsemester[7] = trim($row['7sem']);
			if($user_percentageinsemester[7]!=NULL && is_numeric($user_percentageinsemester[7]) || strpos($user_percentageinsemester[7],'%'))
		$nullv++;
			$user_percentageinsemester[8] = trim($$row['8sem']);
			if($user_percentageinsemester[8]!=NULL && is_numeric($user_percentageinsemester[8]) || strpos($user_percentageinsemester[8],'%'))
			 $nullv++;
			$a=0;
			$student_total=0;
			for($s = 1;$s<=8;$s++)
			{
				if($user_percentageinsemester[$s]!='')
				$student_total=$student_total + $user_percentageinsemester[$s];
				if($user_percentageinsemester[$s]!='' && $user_percentageinsemester[$s]!=0.00)
					$a++;

			}
			// if($student[$i]==1602710901)
			// 		echo $a;
			
			// $student_total=$user_percentageinsemester1+$user_percentageinsemester2+$user_percentageinsemester3+$user_percentageinsemester4+$user_percentageinsemester5+$user_percentageinsemester6+$user_percentageinsemester7+$user_percentageinsemester8;
			
			
			$student_average[$i]=number_format(($student_total/($a)),2);
			$total_marks[$student[$i]]=$student_average[$i];
					/*
					$ins_dump="INSERT INTO dump".
								"(roll_no, marks)".
								"VALUES ('$student[$i]','$student_average[$i]')";
					$insert = mysql_query( $ins_dump, $conn );
					*/
				
				
			
			}
			
			if($filter==NORMAL)
			{}
			else if($filter==ASC ||$filter==BOTTOM )
			asort($total_marks);
			else if($filter==DEC || $filter==TOP )
			arsort($total_marks);
			$z++;
			$n=0;
			echo "<table id='list'>";
			echo "<tr id='head'>
					<td>S.NO.</td>
					<td>Roll No.</td>
					<td>Name</td>
					<td>Sem</td>
					<td>Sem 1 Marks</td>
					<td>Sem 2 Marks</td>
					<td>Sem 3 Marks</td>
					<td>Sem 4 Marks</td>
					<td>Sem 5 Marks</td>
					<td>Sem 6 Marks</td>";
				if($branch1!='MCA'){
					echo "<td>Sem 7 Marks</td>
						<td>Sem 8 Marks</td>";
				}	
					echo "<td>BTech Average</td>
				</tr>";
				$i=$z;
				foreach($total_marks as $x=>$x_value){
					if($x_value==0) continue;
					$i--;
					$bot[$i]=$x;
					if($i==1)
					{break;}
				}
				
				
				foreach($total_marks as $x=>$x_value){
				$n++;
				if(($filter==TOP || $filter==BOTTOM) && $n==$z)
				{break;}
				if($filter==BOTTOM){
				$student_id = $bot[$n];
				}
				else{
				$student_id = $x;
				}


				$qry2 = "SELECT * FROM student_info WHERE username='$student_id' LIMIT 1 ";
			
			$run2 = mysqli_query( $conn,$qry2 );
			if(!$run2 )
					{
					  die('Could not enter data: ' . mysqli_error($conn));
					}


			
			$i=0;
			
			while($row2 = mysqli_fetch_assoc($run2)){
				$student_id=$row2['username'];
			$name=$row2['firstname']." ".$row2['lastname'];
				$user_student_number = $row2['studentnumber'];
				$user_section = trim($row2['section']);
				$user_branch = $row2['branch'];
				$user_semester = $row2['currsem'];
				$nullv=0;
				$userper[1]= trim($row2['1sem']);
				if($userper[1]!=NULL && is_numeric($userper[1]) || strpos($userper[1],'%'))
				$nullv++;
				$userper[2]= trim($row2['2sem']);
				if($userper[2]!=NULL && is_numeric($userper[2]) || strpos($userper[2],'%'))
				$nullv++;
				$userper[3]= trim($row2['3sem']);
				if($userper[3]!=NULL  && is_numeric($userper[3]) || strpos($userper[3],'%'))
				$nullv++;
				$userper[4] = trim($row2['4sem']);
				if($userper[4]!=NULL && is_numeric($userper[4]) || strpos($userper[4],'%'))
				$nullv++;
				$userper[5] = trim($row2['5sem']);
				if($userper[5]!=NULL && is_numeric($userper[5]) || strpos($userper[5],'%'))
				$nullv++;
				$userper[6] = trim($row2['6sem']);
				if($userper[6]!=NULL && is_numeric($userper[6]) || strpos($userper[6],'%'))
				$nullv++;
				$userper[7] = trim($row2['7sem']);
				if($userper[7]!=NULL && is_numeric($userper[7]) || strpos($userper[7],'%'))
				$nullv++;
				$userper[8] = trim($row2['8sem']);
				if($userper[8]!=NULL && is_numeric($userper[8]) || strpos($userper[8],'%'))
				$nullv++;
				$a=0;
				$student_total=0;
				for($s = 1;$s<=8;$s++)
				{
					if($userper[$s]!='')
					$student_total=$student_total + $userper[$s];
					if($userper[$s]!='' && $userper[$s]!=0.00)
					$a++;

				}

				// $user_percentageinsemester_total=$userper[1]+$userper[2]+$userper[3]+$userper[4]+$userper[5]+$userper[6]+$userper[7]+$userper[8];
				$user_percentageinsemester_average=number_format(($student_total/($a)),2);

				echo "<tr>
					<td>$n</td>
					<td>$student_id</td>
					<td>$name</td>
					<td>$user_semester</td>
					<td>$userper[1]</td>
					<td>$userper[2]</td>
					<td>$userper[3]</td>
					<td>$userper[4]</td>
					<td>$userper[5]</td>
					<td>$userper[6]</td>";
					if($branch1!='MCA'){
					echo "<td>$userper[7]</td>
						<td>$userper[8]</td>";
					}
					echo "<td>$user_percentageinsemester_average</td>
				</tr>";
			
			}
		}

			echo "</table>";

			
				$name=$_SESSION['name'];
			 $username=$_SESSION['username'];
		 $branch=$_SESSION['branch'];
			 $category=$_SESSION['category'];
		$get_hodname="SELECT * FROM login WHERE userid='$username'";
			mysql_select_db('portal');
			$getit= mysqli_query( $conn ,$get_hodname );
			while($row = mysqli_fetch_assoc($getit)){
			$hodname=$row['name'];
			$branch=$row['branch'];
			$category=$row['category'];
			}

			$get_hodname="SELECT * FROM login WHERE branch='$branch' AND category='HOD'";
			
			$getit= mysqli_query( $conn,$get_hodname);
			while($row = mysqli_fetch_assoc($getit)){
			 $h_name=$row['name'];
			}

			$query="SELECT * FROM coordinator WHERE fac_id='$username'";
			
			$exec= mysqli_query(  $conn,$query );
			$is_c=0;
			while($row = mysqli_fetch_assoc($exec)){
			$is_c=1;
			}
			echo "</br></br></br></br>";

			
			if($category=='FACULTY'){

				if($is_c==1){

				echo "<table class='last' id='heading'><tr><td><div id='heading' align='left'>";
					echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>(FACULTY COORDINATOR)</strong>";
				echo "</div></td>";
				echo "<td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><td><div id='heading' align='right'>";
					echo "<strong style=font-size:18px>$h_name</br>
					<strong style=font-size:18px>(HOD $branch)</strong>";
				echo "</div></td></tr></table>";
				
				}

				else{
					echo "<table class='last' id='heading'><tr><td><div id='heading' align='left'>";
					echo "<strong style=font-size:18px>$hodname</br>
					<strong style=font-size:18px>(FACULTY)</strong>";
				echo "</div></td>";
				echo "<td><td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div id='heading' align='right'>";
					echo "<strong style=font-size:18px>$h_name</br>
					<strong style=font-size:18px>(HOD $branch)</strong>";
				echo "</div></td></tr></table>";
			
				}
			}
			else{

			echo "<div  align='right'>";
					echo "<strong style=font-size:18px>".$name."</br>
					<strong style=font-size:18px>( ".$category .$branch.")</strong>";
				echo "</div>";
				

				
			}
			
		}

		
	?>
	</body>
	</html>

