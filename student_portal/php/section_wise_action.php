<?php
include 'menu.php';
include 'connect.php';
echo "<div id='space'> <br><br><br><br><br><br><br></div>";

?>
<html>

<head>
	<link rel="stylesheet" type="text/css" href="../css/marks_layout.css">
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="print" /> 
</head>
<script>
	function printpage()
  {
  window.print()
  }
   function showbatch()
	{
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showbatch.php",true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("batch").innerHTML=xmlhttp.responseText;
			}
		}
	}



  function showsection(str)
	{
		
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showsection.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("section").innerHTML=xmlhttp.responseText;
			}
		}
	}
	
 </script>
</html>


<html>
<body onload="showbatch();">
<?php
	if(!isset($_POST['submit'])){
?>
<div style="font-size:18;font-weight:200%; margin-top:-5%; margin-bottom:4%;" align="center"> SECTION WISE ACTION/AWARDS</div>
	<div align="center"><form action="section_wise_action.php" method="post">
	<table id='except'>
			<tr>
			<td>batch:</td>
			<td>
					<select size="1" name="batch" id="batch" required="required" onchange="showsection(this.value)" required>
		
	</select>
				
			</td>
			
		</tr>
		<tr>
			<td>
			Section</td>
			<td >
				<select size="1" name="section" id="section" onchange="showsubjectid(this.value)" required></select>
			</td>
			<td ><input type="submit" value="Submit" class="button" name="submit" ></td>
		</tr>
	</table></form></div>
	<?php }?>



	<?php

		if(isset($_POST['submit'])){


			$batch = $_POST['batch'];
			$section = $_POST['section'];
		

			$get_branch="SELECT DISTINCT branch FROM student WHERE section='$student_section' AND semester='$semester'";
		
			$getit= $conn->query($get_branch);
			


			while($row = $getit->fetch_assoc()){
			$branch1=$row['branch'];
			}

			if($branch1=='CSE'){
			$fullname='COMPUTER SCIENCE AND ENGINEERING';	
			}
			else if($branch1=='IT'){
			$fullname='INFORMATION TECHNOLOGY';
			}
			else if($branch1=='ME'){
			$fullname='MECHANICAL ENGINEERING';
			}
			else if($branch1=='ECE'){
			$fullname='ELECTRONICS & COMMUNICATION ENGINEERING';
			}
			else if($branch1=='EI'){
			$fullname='ELECTRONICS & INSTRUMENTATION ENGINEERING';
			}
			else if($branch1=='EN'){
			$fullname='ELECTRICAL & ELECTRONICS ENGINEERING';
			}
			else if($branch1=='CE'){
			$fullname='CIVIL ENGINEERING';
			}
			else if($branch1=='MBA'){
			$fullname='MASTER OF BUISNESS ADMINISTRATION';
			}
			else if($branch1=='MCA'){
			$fullname='MASTER OF COMPUTER APPLICATION';
			}
			else if($branch1=='AS-HU'){
			$fullname='APPLIED SCIENCE & HUMANITIES';
			}
			echo "<div align='center'><input type='button' value='PRINT THIS PAGE' onclick='printpage()' class='button' id='print'></div>";
			echo "<div id='heading' align='center'>
					<div align='right'><p style=font-size:14px>AKGEC/MR/FM/09</p></div>
					<strong style=font-size:20px>Ajay Kumar Garg Engineering College, Ghaziabad</strong></br>
					<strong style=font-size:15px>Branch: $fullname (Section-$student_section)</strong></br>
					<strong style=font-size:15px>Batch: $batch</strong></br>
					<strong style=font-size:18px>STUDENT ACTION LIST</strong>
				</div></br>";
			echo "<table align='center' width='90%' style='margin-left:5%' class='nopadding'>
					<tr id='head'>
						<td colspan='3'></td>
						<td colspan='2'>DISCIPLINARY ACTIONS</td>
						<td colspan='4'>EXTRA/CO-CURRICULAR ACTIVITIES</td>	
						<td colspan='2'>Parents Meeting</td>
						<td colspan='3'>CARRY OVER DETAILS</td>
					<tr>";

				echo "<tr id='subhead'>
						<td>S.No.</td>
						<td>University No</td>
						<td>Name</td>
						<td>Date</td>
						<td>Case</td>
						<td>Date</td>
						<td>event</td>
						<td>Participation type</td>
						<td>Description</td>
						<td>date</td>
						<td>Reason</td>
						<td>Sem</td>
						<td>Subject</td>
						<td>Status</td>
					</tr>";		
				


			$qry="SELECT * FROm student_info, student where  batchyear='$batch' and student.section='$section' and student_info.username=student.st_id ";

			$run=$conn3->query($qry);

			if(!$run)
				echo mysqli_error($conn3);

			$i=1;

			while($row=$run->fetch_assoc())
			{	
				$username=$row['username'];
				$name=$row['name'];

				echo "<td>".$i++."</td>";
				echo "<td>".$username."</td>";
				echo "<td>".$name."</td>";




				$query="SELECT * FROM action WHERE st_id='username'";
					
					$execute =$conn3->query($query);
					echo "<td colspan='2' style='padding:0px'>";
					echo "<table class='nopadding1'>";
					while($row1 = $execute->fetch_assoc()){
						echo "<tr>";
						echo "<td>$row1[date]</td><td>$row1[case]</td>";
						echo "</tr>";
					}		
					echo "</table>";
					echo "</td>";

					$query="SELECT * FROM award WHERE st_id='$username'";
					
					$execute =$conn3->query($query);
					echo "<td colspan='4' style='padding:0px'>";
					echo "<table class='nopadding1'>";
					while($row1 = $execute->fetch_assoc()){
						echo "<tr>";
						echo "<td>$row1[date]</td><td>$row1[event]</td><td>$row1[participation_type]</td><td>$row1[description]</td>";
						echo "</tr>";
					}		
					echo "</table>";
					echo "</td>";

					$query="SELECT * FROM parents_meeting WHERE st_id='$user->username'";
					
					$execute =$conn3->query($query);
					echo "<td colspan='2' style='padding:0px'>";
					echo "<table class='nopadding1'>";
					while($row1 = $execute->fetch_assoc()){
						echo "<tr>";
						echo "<td>$row1[date]</td><td>$row1[reason]</td>";
						echo "</tr>";
					}		
					echo "</table>";
					echo "</td>";

					$query="SELECT * FROM carry WHERE st_id='$user->username' ORDER BY semester ASC";
					
					$execute =$conn3->query($query);
					echo "<td colspan='3' style='padding:0px'>";
					echo "<table class='nopadding1'>";
					while($row1 = $execute->fetch_assoc()){
						echo "<tr>";
						echo "<td>$row1[semester]</td><td>$row1[sub_id]</td><td>$row1[status]</td>";
						echo "</tr>";
					}		
					echo "</table>";
					echo "</td>";


					echo "</tr>";
				}

}

?>