<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/new_layout.css">
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="print" /> 
</head>
<script>
	function printpage()
  {
  window.print()
  }
 </script>
</html>


<?php

function roman($num) 
{
    $n = intval($num);
    $res = '';
 
    /*** roman_numerals array  ***/
    $roman_numerals = array(
                'M'  => 1000,
                'CM' => 900,
                'D'  => 500,
                'CD' => 400,
                'C'  => 100,
                'XC' => 90,
                'L'  => 50,
                'XL' => 40,
                'X'  => 10,
                'IX' => 9,
                'V'  => 5,
                'IV' => 4,
                'I'  => 1);
 
    foreach ($roman_numerals as $roman => $number) 
    {
        $matches = intval($n / $number);
        $res .= str_repeat($roman, $matches);
        $n = $n % $number;
    }
 
    return $res;
    }
	


include 'common.php';
include 'connect.php';

$student_id = $_POST['student_id'];

global $DB;

$user = $DB->get_record('user', array('username'=>$student_id));
$user_student_number = $DB->get_field('user_info_data','data',array('fieldid'=>2,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_10tdmarks = $DB->get_field('user_info_data','data',array('fieldid'=>25,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_12tdmarks = $DB->get_field('user_info_data','data',array('fieldid'=>26,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress1 = $DB->get_field('user_info_data','data',array('fieldid'=>13,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress2 = $DB->get_field('user_info_data','data',array('fieldid'=>14,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress3 = $DB->get_field('user_info_data','data',array('fieldid'=>15,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress4 = $DB->get_field('user_info_data','data',array('fieldid'=>16,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress5 = $DB->get_field('user_info_data','data',array('fieldid'=>17,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentaddress6 = $DB->get_field('user_info_data','data',array('fieldid'=>18,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_permanentcontactnumber = $DB->get_field('user_info_data','data',array('fieldid'=>3,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_fatdersname = $DB->get_field('user_info_data','data',array('fieldid'=>8,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_parentscontactnumber = $DB->get_field('user_info_data','data',array('fieldid'=>9,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansname = $DB->get_field('user_info_data','data',array('fieldid'=>10,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardianscontactnumber = $DB->get_field('user_info_data','data',array('fieldid'=>11,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress1 = $DB->get_field('user_info_data','data',array('fieldid'=>19,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress2 = $DB->get_field('user_info_data','data',array('fieldid'=>20,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress3 = $DB->get_field('user_info_data','data',array('fieldid'=>21,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress4 = $DB->get_field('user_info_data','data',array('fieldid'=>22,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress5 = $DB->get_field('user_info_data','data',array('fieldid'=>23,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_localguardiansaddress6 = $DB->get_field('user_info_data','data',array('fieldid'=>24,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_branch = $DB->get_field('user_info_data','data',array('fieldid'=>39,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_semester = $DB->get_field('user_info_data','data',array('fieldid'=>27,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_dateofbirtd = $DB->get_field('user_info_data','data',array('fieldid'=>4,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_dateofbirtd_to_date = date('jS F Y ', $user_dateofbirtd);

$userper[1]= $DB->get_field('user_info_data','data',array('fieldid'=>28,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[1]!=NULL && is_numeric($userper[1]) || strpos($userper[1],'%'))
$nullv++;
$userper[2]= $DB->get_field('user_info_data','data',array('fieldid'=>29,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[2]!=NULL && is_numeric($userper[2]) || strpos($userper[2],'%'))
$nullv++;
$userper[3]= $DB->get_field('user_info_data','data',array('fieldid'=>30,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[3]!=NULL  && is_numeric($userper[3]) || strpos($userper[3],'%'))
$nullv++;
$userper[4] = $DB->get_field('user_info_data','data',array('fieldid'=>31,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[4]!=NULL && is_numeric($userper[4]) || strpos($userper[4],'%'))
$nullv++;
$userper[5] = $DB->get_field('user_info_data','data',array('fieldid'=>32,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[5]!=NULL && is_numeric($userper[5]) || strpos($userper[5],'%'))
$nullv++;
$userper[6] = $DB->get_field('user_info_data','data',array('fieldid'=>33,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[6]!=NULL && is_numeric($userper[6]) || strpos($userper[6],'%'))
$nullv++;
$userper[7] = $DB->get_field('user_info_data','data',array('fieldid'=>34,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[7]!=NULL && is_numeric($userper[7]) || strpos($userper[7],'%'))
$nullv++;
$userper[8] = $DB->get_field('user_info_data','data',array('fieldid'=>35,'userid'=>$user->id), $strictness=IGNORE_MISSING);
if($userper[8]!=NULL && is_numeric($userper[8]) || strpos($userper[8],'%'))
$nullv++;

$user_percentageinsemester_total=$userper[1]+$userper[2]+$userper[3]+$userper[4]+$userper[5]+$userper[6]+$userper[7]+$userper[8];
$user_percentageinsemester_average=number_format(($user_percentageinsemester_total/($nullv)),2);

$user_section = $DB->get_field('user_info_data','data',array('fieldid'=>38,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$user_batch = $DB->get_field('user_info_data','data',array('fieldid'=>1,'userid'=>$user->id), $strictness=IGNORE_MISSING);
echo "<div id='space'></br></br></br></br></br></div>";
echo "<div id='heading' align='center'>
		<strong style=font-size:20px>Ajay Kumar Garg Engineering College, Ghaziabad</strong></br></br>
		<strong style=font-size:18px>STUDENT REPORT CARD</strong>
	</div>";
echo "<div align='center' ><input type='button' value='PRINT THIS PAGE' onclick='printpage()' class='button' id='print'></div>";
echo "</br>";
echo "<div>";
echo "<table width='80%' id='table1'>
			<tr id='head'>
				<td colspan='5' style='text-align:center'>PERSONAL DETAILS</td>
			</tr>
			<tr>
				<td rowspan='5'><div align='center'>";
					global $USER, $COURSE;
					print_user_picture($user, $COURSE->id, null, true);
					echo "</div>
				</td>
				<td id='design'>Name</td>
				<td>$user->firstname $user->lastname</td>
				<td id='design'>Father's Name</td>
				<td>$user_fatdersname</td>
			</tr>
			<tr>
				<td id='design'>Roll No.</td>
				<td>$student_id</td>
				<td id='design'>Student No.</td>
				<td>$user_student_number</td>
			</tr>
			<tr>
				<td id='design'>Branch</td>
				<td>$user_branch</td>
				<td id='design'>Batch Year</td>
				<td>$user_batch</td>
			</tr>
			<tr>
				<td id='design'>Section</td>
				<td>$user_section</td>
				<td id='design'>Semester</td>
				<td>$user_semester</td>
			</tr>
			<tr>
				<td id='design'>DOB</td>
				<td>$user_dateofbirtd_to_date</td>
				<td id='design'>Email id</td>
				<td>$user->email</td>
			</tr>
	</table>";
	
echo "<table width='80%' id='table2'>
			<tr id='head'>
				<td colspan='5' style='text-align:center'>CONTACT DETAILS</td>
			</tr>
			<tr>
				<td id='design'>Contact No.</td>
				<td>$user_permanentcontactnumber</td>
				<td id='design'>Parent's Contact No.</td>
				<td>$user_parentscontactnumber</td>
			</tr>
			<tr>
				<td id='design'>Permanent Address</td>
				<td>$user_permanentaddress1<br>$user_permanentaddress2<br>$user_permanentaddress3<br>$user_permanentaddress4<br>$user_permanentaddress5<br>$user_permanentaddress6</td>
				<td id='design'>Local Address</td>
				<td>$user_localguardiansaddress1<br>$user_localguardiansaddress2<br>$user_localguardiansaddress3<br>$user_localguardiansaddress4<br>$user_localguardiansaddress5<br>$user_localguardiansaddress6</td>
			</tr>
	</table>";
	
echo "<table width='80%' id='table3'>
			<tr id='head'>
				<td colspan='8' style='text-align:center'>ACADEMIC PERFORMANCE</td>
			</tr>
			<tr>
				<td id='design' style='text-align:center'>10th %</td>
				<td colspan='2' style='text-align:center'>$user_10tdmarks %</td>
				<td id='design' style='text-align:center'>12th/diploma  %</td>
				<td colspan='2' style='text-align:center'>$user_12tdmarks %</td>
				<td id='design' style='text-align:center'>B.Tech %</td>
				<td style='text-align:center'>$user_percentageinsemester_average %</td>
			</tr>
			<tr id='subhead'>
				<td style='text-align:center'>SEM</td>
				<td style='text-align:center'>CT-I %</td>
				<td style='text-align:center'>CT-II %</td>
				<td style='text-align:center'>ST-I %</td>
				<td style='text-align:center'>ST-II %</td>
				<td style='text-align:center'>PUT %</td>
				<td style='text-align:center'>Final Marks %</td>
				<td style='text-align:center'>Attendance %</td>
			</tr>
			<tr>";
				for($i=1;$i<=8;$i++){
			
			$getmarks="SELECT * FROM marks_old WHERE st_id=$student_id AND semester='$i'";
			mysql_select_db('portal');
						$retval = mysql_query( $getmarks, $conn );
					while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
					{
						$CT1[$i]=round($row['CT1'],2);
						$CT2[$i]=round($row['CT2'],2);
						$ST1[$i]=round($row['ST1'],2);
						$ST2[$i]=round($row['ST2'],2);
						$PUT[$i]=round($row['PUT'],2);
					}
			$getattendance="SELECT * FROM attendance_old WHERE st_id=$student_id AND semester='$i'";
			mysql_select_db('portal');
						$retval = mysql_query( $getattendance, $conn );
					while($row = mysql_fetch_array($retval, MYSQL_ASSOC))
					{$attendance[$i]=round($row['attendance'],2);}
					
				echo "<tr>
					<td style='text-align:center'><b style='font-size:14px'>";echo roman($i); echo "</b></td>
					<td style='text-align:center'>$CT1[$i]</td>
					<td style='text-align:center'>$CT2[$i]</td>
					<td style='text-align:center'>$ST1[$i]</td>
					<td style='text-align:center'>$ST2[$i]</td>
					<td style='text-align:center'>$PUT[$i]</td>
					<td style='text-align:center'>$userper[$i]</td>
					<td style='text-align:center'>$attendance[$i]</td>
			</tr>";
			}
			
			
	echo "</table>";

echo "</div>";

?>
