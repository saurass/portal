<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

     
    <link rel="stylesheet" href="../assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">

</head>
<?php
	if($_SESSION['category']!='DIRECTOR')
	{
		header("location:index.php");
	}
?>