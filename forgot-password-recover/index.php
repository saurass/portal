<?php
    if(!empty($_POST["forgot-password"])){
        $conn = mysqli_connect("localhost", "root", "HPv185e@#%", "portal");
        
        $condition = "";

        $ns=mysqli_real_escape_string($conn,$_POST['user-login-name']);

        if(!empty($_POST["user-login-name"])) 
            {
                $condition = " username = '" . $ns . "'";
            }
        if(!empty($_POST["user-email"])) {
            if(!empty($condition)) {
                $condition = " and ";
            }
            $condition = " email = '" . $ns . "'";
        }
        
        if(!empty($condition)) {
            $condition = " where " . $condition;
        }

        $sql = "Select * from users " . $condition;
        $result = mysqli_query($conn,$sql);
        $user = mysqli_fetch_assoc($result);
        
        if(!empty($user)) {
                                    require_once("mail_configuration.php");

                        $con=mysqli_connect('localhost','root','HPv185e@#%','portal');

                        $email=$user['email'];
                        $nos=rand(1000,1000000);
                        $str=$user['firstname'].$user['email'].$nos;
                        $key=md5($str);

                        $qrcheck="SELECT * FROM password_resets where email='$email'";
                        $rum=mysqli_query($conn,$qrcheck);
                        if(mysqli_num_rows($rum)>0){
                            $query="UPDATE password_resets SET token='$key' where email='$email'";
                        }
                        else{
                        $query="INSERT INTO password_resets VALUES('$email','$key')";}
                        $result=$con->query($query);


                         $emailBody = "<div>" . $user["firstname"] . ",<br><br><p>Click this link to recover your password<br><a href='10.10.156.201/forgot-password-recover/reset_password.php?key=" .$key. "'>php-forgot-password-recover/reset_password.php?key=" . $key . "</a><br><br></p>Regards,<br> Admin.</div>";

                        $To = $user['email'];
                        $Subject = 'password recovery email';
                        $Message = 'This example demonstrates how you can send plain text email with PHP';
                        $Headers = "From: ossrndcentre@gmail.com \r\n" .
                            "Reply-To: sender@yourdomain.com \r\n" .
                            "Content-type: text/html; charset=UTF-8 \r\n";

                        if(!mail($To, $Subject, $emailBody, $Headers)) {
                            $error_message = 'Problem in Sending Password Recovery Email';
                        } else {
                            $success_message = 'Please check your email to reset password!';
                        }
            
        } 
        else {
            $error_message = 'No User Found';
        }
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Forgot Password</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/custom.css">

</head>
<body class="green">
<script>
    <script>
function validate_forgot() {
    if((document.getElementById("user-login-name").value == "") && (document.getElementById("user-email").value == "")) {
        document.getElementById("validation-message").innerHTML = "Login name or Email is required!"
        return false;
    }
    return true;
}
</script>
</script>
<div class="preloading-screen">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
    <h4 class="ml-5 green-text text-darken-2"> GreenBoard! <i class="fa fa-pencil"></i></h4>
    <br><br>
    <h6 class="powered-by">Powered by Team OSS</h6>
</div>

<div class="login-preloader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-yellow-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>

<main class="container">

    <form name="frmForgot" id="frmForgot" method="post" onSubmit="return validate_forgot();" class="username-form">
        <h4 class="white-text">Enter Username</h4>

        <?php if(!empty($success_message)) { ?>
    
        <div class="row">
            <div class="input-field col s12 m6">
                

                <label for="username"><?php echo $success_message; ?></label>
            </div>
        </div>

    <?php } ?>



        <div class="row">
            <div class="input-field col s12 m6">
                <input type="text" name="user-login-name" id="user-login-name">
                <label for="username">Username</label>
            </div>
        </div>

    <div id="validation-message">
        <?php if(!empty($error_message)) { ?>

        <div class="row">
            <div class="input-field col s12 m6">
                <label><?php echo $error_message; ?></label>
            </div>
        </div>

    <?php } ?>
    </div>


        <p class="error" id="error-username"></p>
        <div class="row">
            <div class="input-field col s12 m6 right-align">
                <input type="submit" name="forgot-password" id="forgot-password" class="btn white black-text bold-text next" value="Recover">
            </div>
        </div>
    </form>

    
</main>


<aside class="right-aside">
    <div class="open-seasame green darken-2 white-text">Login Links <i class="fa fa-arrow-up"></i></div>
    <ul class="collection with-header center">
        <li class="collection-header"><h5>Links</h5></li>
        <a href="../login-faculty.php" class="collection-item active">Faculty Login</a>
        <a href="../login-student.php" class="collection-item">Student Login</a>
        <a href="index.php" class="collection-item"><i class="fa fa-arrow-left"></i> Back to Home</a>
    </ul>
</aside>

<aside class="left-aside">
    <div class="open-alohomora green darken-2 white-text">Register Links <i class="fa fa-arrow-up"></i></div>
    <ul class="collection with-header center">
        <li class="collection-header"><h5>Links</h5></li>
        <a href=".#" class="collection-item">Faculty Register</a>
        <a href="#" class="collection-item">Student Register</a>
        <a href="../index.php" class="collection-item"><i class="fa fa-arrow-left"></i> Back to Home</a>
    </ul>
</aside>

<a class="g" href="../about.html">G <i class='fa fa-pencil'></i></a>
<a class="h" href="../index.php"> <i class='fa fa-home'></i></a>

<div class="footnote text-capitalize green-text text-lighten-2">
    &copy; GreenBoard 2017 | <a href="about.html" class="green-text text-lighten-2">About</a>
</div>
<script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/plugins/materialize/js/materialize.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript">
        
    </script>
<script type="text/javascript" src="assets/js/login.js"></script>
</body>
</html>