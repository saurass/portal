<!DOCTYPE html>
<html lang="en">
<head>
    <title>Forgot Password</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <script>
    	function check()
    	{
    		var oldpass=document.getElementById('user-old-password').value;
	    	var newpass=document.getElementById('user-new-password').value;
	    	var compass=document.getElementById('comfirm-new-password').value;

	    	if(newpass!==compass){
	    		alert('Password do not match');
	    		return false;
	    	}
	    	elseif(newpass==='' || compass===''){
	    		alert('Please Enter Some Password');
	    		return false;
	    	}
	    	else{
	    		return true;
	    	}
    	}
    </script>

</head>

<body class="green">
<script>
    <script>
function validate_forgot() {
    if((document.getElementById("user-login-name").value == "") && (document.getElementById("user-email").value == "")) {
        document.getElementById("validation-message").innerHTML = "Login name or Email is required!"
        return false;
    }
    return true;
}
</script>
</script>
<div class="preloading-screen">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
    <h4 class="ml-5 green-text text-darken-2"> GreenBoard! <i class="fa fa-pencil"></i></h4>
    <br><br>
    <h6 class="powered-by">Powered by Team OSS</h6>
</div>

<div class="login-preloader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-yellow-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>


<main class="container">

    <form name="frmForgot" action="savepasswordnew.php" method="post" class="username-form">

        <h4 class="white-text">Reset Password !</h4>

        <div class="row">
            <div class="input-field col s12 m6">
                <input type="text" name="old-password" id="user-old-password">
                <label for="username">Old Password</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">
                <input type="text" name="new-password" id="user-new-password">
                <label for="username">New Password</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">
                <input type="text" name="comfirm-password" id="comfirm-new password">
                <label for="username">Comfirm New Password</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6 right-align">
                <input type="submit" name="forgot-password" id="forgot-password" class="btn white black-text bold-text next" onclick='return check();' value="RESET">
            </div>
        </div>

    </form>
</main>



<a class="g" href="../about.html">G <i class='fa fa-pencil'></i></a>
<a class="h" href="../index.php"> <i class='fa fa-home'></i></a>

<div class="footnote text-capitalize green-text text-lighten-2">
    &copy; GreenBoard 2017 | <a href="about.html" class="green-text text-lighten-2">About</a>
</div>
<script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/plugins/materialize/js/materialize.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript">
        // $(document).ready(function(){
        //     $(".password-form").hide();
        // });
    </script>
<script type="text/javascript" src="assets/js/login.js"></script>