<?php
include 'magic.php';
include('connect.php');
include('menuh.php');

echo "
<style>

@media print {
	
	body 
	{
		font-family:arial;
	}
	
	form 
	{
		display:none;
	}
	#cssmenu 
	{
		display:none;
	}
	#header1 
	{
		display:none;
	}
	#space 
	{
		display:none;
	}
	input
	{
		display: none;  
	}
    
	table{
		color:#000000;
		border:1px solid black;
		border-collapse:collapse;
		width:100%;
	}
	table tr td{
		border:1px solid black;
		padding:3px;
		font-size:16px;
	}
	table tr:first-child td{ 
		font-weight:900; 
	}

}	

</style>";

?>
<?php

$branch=$a;
if($branch=="CSE")
$bran="Computer Science Engineering";

elseif($branch=="ME")
$bran="Mechanical Engineering";

elseif($branch=="EN")
$bran="Electrical Engineering ";

else if($branch=="IT")
$bran="Information Technology ";

else if($branch=="EE")
$bran="Electrical and Electronics  Engineering ";

else if($branch=="AS-HU")
$bran="Applied Science and Humanities";

else if($branch=="ECE")
$bran="Electronics and Communication Engineering";

else if($branch=="MBA")
$bran="MBA";

if($branch=='MBA')
echo "<div id='report' width='80%' align='center'>
				<div align='center' style='font-weight:bold;font-size:20px'>
					<br>AJAY KUMAR GARG INSTITUTE OF MANAGEMENT,GHAZIABAD<br>
				</div>";
else
echo "<div id='report' width='80%' align='center'>
				<div align='center' style='font-weight:bold;font-size:20px'>
					<br>AJAY KUMAR GARG ENGINEERING COLLEGE,GHAZIABAD<br>
				</div>";

echo "<div align='center' style='font-weight:600;font-size:16px'>
					<br>DEPARTMENT OF $bran<br>DEPARTMENT WISE ATTENDANCE REPORT
				</div>";
	echo "<table align='center' width='80%' class='table_style'>
				<tr>
					<td><b>Branch</b></td>
					<td><b>semester</b></td>
					<td><b>less than 50</b></td>
					<td><b>50 to 60</b></td>
					<td><b>60 to 70</b></td>
					<td><b>70 to 75</b></td>
					<td><b>greater than 75</b></td>
					<td><b>TOTAL</b></td>
				</tr>";
				
			$q_sem="SELECT DISTINCT semester FROM student WHERE branch='$branch' ORDER BY semester ASC";
			mysql_select_db('portal');
			$getsem = mysql_query( $q_sem, $conn );
			$sem_count=0;
			while($row2 = mysql_fetch_array($getsem, MYSQL_ASSOC)){
				$sem_count++;
			}	
			$sem_count++;
			echo "<tr><td rowspan='$sem_count'><b>$branch</b></td>";
			
			$q_sem="SELECT DISTINCT semester FROM student WHERE branch='$branch' ORDER BY semester ASC";
			mysql_select_db('portal');
			$getsem = mysql_query( $q_sem, $conn );
			while($row2 = mysql_fetch_array($getsem, MYSQL_ASSOC)){
				$sem=$row2['semester'];
				echo "<td>$sem</td>";
				
				$count0=0;
				$count1=0;
				$count2=0;
				$count3=0;
				$count4=0;
				$get_student="SELECT * FROM student WHERE semester='$sem' AND branch='$branch'";
				mysql_select_db('portal');
				$getstudent = mysql_query( $get_student, $conn );
				while($row3 = mysql_fetch_array($getstudent, MYSQL_ASSOC)){
					$studentid=$row3['st_id'];
					$get_att="SELECT * FROM attendance WHERE st_id='$studentid'";
					mysql_select_db('portal');
					$getatt = mysql_query( $get_att, $conn );
					$total[$studentid]=0;
					$attend[$studentid]=0;
					while($row4 = mysql_fetch_array($getatt, MYSQL_ASSOC)){
						$total[$studentid]+=$row4['totalclasses'];
						$attend[$studentid]+=$row4['attended'];
					}
					$percentage[$studentid]=round($attend[$studentid]/$total[$studentid]*100,2);
				if($percentage[$studentid]<49.45){
					$count0++;
				}
				else if($percentage[$studentid]>=49.45 && $percentage[$studentid]<59.45){
					$count1++;
				}
				else if($percentage[$studentid]>=59.45 && $percentage[$studentid]<69.45){
					$count2++;
				}
				else if($percentage[$studentid]>=69.45 && $percentage[$studentid]<74.45){
					$count3++;
				}
				else if($percentage[$studentid]>=74.45){
					$count4++;
				}
				
				}
				$c[$sem]=$count0+$count1+$count2+$count3+$count4;
				$c0+=$count0;
				$c1+=$count1;
				$c2+=$count2;
				$c3+=$count3;
				$c4+=$count4;

				echo "<td><a href='student_list.php?sem=$sem&branch=$branch&start=0&end=49.45' target='_blank'>$count0</a></td>
					<td><a href='student_list.php?sem=$sem&branch=$branch&start=49.45&end=59.45' target='_blank'>$count1</a></td>
					<td><a href='student_list.php?sem=$sem&branch=$branch&start=59.45&end=69.45' target='_blank'>$count2</a></td>
					<td><a href='student_list.php?sem=$sem&branch=$branch&start=69.45&end=74.45' target='_blank'>$count3</a></td>
					<td><a href='student_list.php?sem=$sem&branch=$branch&start=74.45&end=100' target='_blank'>$count4</a></td>
					<td><b>$c[$sem]</b></td>
				</tr>";
				
			}
			echo "<tr>
					<td><b>Total</b></td>
					<td><b>$c0</b></td>
					<td><b>$c1</b></td>
					<td><b>$c2</b></td>
					<td><b>$c3</b></td>
					<td><b>$c4</b></td>
					<td><b>".($c0+$c1+$c2+$c3+$c4)."</b></td>
				</tr>";
				
		echo "</div>";
echo "<table style='border:none'>
			<tr><td style='text-align:left;border:none'>NOTE:</td></tr>
			<tr><td style='text-align:left;border:none'>1. The 'less than 50' for those who lies in range of 0 to 49.45</td></tr>
			<tr><td style='text-align:left;border:none'>2. The '50 to 60' for those who lies in range of 49.45-59.45</td></tr>
			<tr><td style='text-align:left;border:none'>3. The '60 to 70' for those who lies in range of 59.46-69.45</td></tr>
			<tr><td style='text-align:left;border:none'>4. The '70 to 75' for those who lies in range of 69.46-74.45</td></tr>
			<tr><td style='text-align:left;border:none'>5. The '75+' for those who lies in range of  74.46-100</td></tr>
		</table>";					


?>
