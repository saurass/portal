<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="../assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>

<?php
include 'require_login.php';
if($_SESSION['category']!='ADMIN')
{
	header("location:index.php");
}
?>
<div class="preloading-screen">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
    <h4 class="ml-5 green-text text-darken-2"> GreenBoard! <i class="fa fa-pencil"></i></h4>
    <br><br>
    <h6 class="powered-by">Powered by Team OSS</h6>
</div>

<header class="navbar-fixed">
    <nav class="green darken-2">
        <div class="nav-wrapper">
            <a href="index.html" class="brand-logo center">GreenBoard <i class="fa fa-pencil right"></i></a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <img align="center" class="icon mx-5" src="../assets/images/profile-image-1.png" align="center"> 
                </li>
                <li> <a href="logout.php"> <i class="fa fa-sign-out"></i> Logout &nbsp; </a> </li>
            </ul>
            <ul class="left hide-on-med-and-down">
                <li><a class="text-uppercase bold-text" href="#">Name of the Portal</a></li>
            </ul>
        </div>
    </nav>
</header>

<aside class="side-navigation green">
	<ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-plus"></i> Add <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        
        <!-- Replace             -->
        <li><a href='addHOD.php'>HOD</a></li>
		<li><a href='addfacadmin.php'>Faculty</a></li>
		<li><a href='add_student.php'>Student</a></li>
		<li><a href='addsubadmin.php'>Subject</a></li>
		<li><a href='addattendanceadmin.php'><span>ATTENDANCE</span></a></li>
		<li><a href='project.php'><span>ASSINGN ROLE</span></a></li>
		<li><a href='project_feedback.php'><span>Assign Role (feedback)</span></a></li>	
		<li><a href='fileuploadadmin.php'><span>UPLOAD STUDENTS</span></a></li>
		<li><a href='subjectupload.php'><span>UPLOAD SUBJECTS</span></a></li>
		<li><a href='facultyupload.php'><span>UPLOAD FACULTY</span></a></li>
		<li><a href='uploadassignrole.php'><span>UPLOAD ASSIGN ROLE</span></a></li>
		<!-- /Replace -->


        <li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>
<!-- <?php
/*echo"<div class='row'>
			<ul>
			   <li class='active'><a href='adminhome.php'><span>HOME</span></a></li>
			   <li><a href='#'><span>ADD</span></a>
					<ul>
						<li><a href='addHOD.php'><span>HOD</span></a></li>
						<li><a href='addfacadmin.php'><span>FACULTY</span></a></li>
						<li><a href='add_student.php'><span>STUDENT</span></a></li>
						<li><a href='addsubadmin.php'><span>SUBJECT</span></a></li>
						<li><a href='addattendanceadmin.php'><span>ATTENDANCE</span></a></li>
						<li><a href='project.php'><span>ASSINGN ROLE</span></a></li>

						<li><a href='project_feedback.php'><span>ASSINGN ROLE (feedback)</span></a></li>

						<li><a href='fileuploadadmin.php'><span>UPLOAD STUDENTS</span></a></li>
						<li><a href='subjectupload.php'><span>UPLOAD SUBJECTS</span></a></li>
						<li><a href='facultyupload.php'><span>UPLOAD FACULTY</span></a></li>
                        <li><a href='uploadassignrole.php'><span>UPLOAD ASSIGN ROLE</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>UPDATE</span></a>
					<ul>
						<li><a href='updatehodadmin.php'><span>HOD</span></a></li>
						<li><a href='updatefacultyadmin.php'><span>FACULTY</span></a></li>
						<li><a href='edit_student.php'><span>STUDENT</span></a></li>
						<li><a href='updatesubjectadmin.php'><span>SUBJECT</span></a></li>
						<li><a href='updateattendanceadmin.php'><span>ATTENDANCE</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>DELETE</span></a>
					<ul>
						<li><a href='deleteHODform.php'><span>HOD</span></a></li>
						<li><a href='deletefacultyadmin.php'><span>FACULTY</span></a></li>
						<li><a href='delete_student.php'><span>STUDENT</span></a></li>
						<li><a href='newdelad.php'><span>SUBJECT</span></a></li>
						<li><a href='new2.php'><span>SEMESTER</span></a></li>
						<!--<li><a href='updatedeleteassignadmin.php'><span>ASSINGN ROLE</span></a></li>-->
						
					</ul>
			   </li>
			   <li><a href='#'><span>VIEW</span></a>
				   <ul>
						<li><a href='viewfacultyadmin.php'><span>FACULTY</span></a></li>
						<li><a href='viewsubjectadmin.php'><span>ASSIGNROLE</span></a></li>
						<li><a href='viewattendanceadmin.php'><span>ATTENDANCE</span></a></li>
						 <li><a href='newsubad.php'><span>SUBJECT</span></a></li>
						
					</ul>
			   </li>
			   <li><a href='#'><span>DOWNLOAD</span></a>
				   <ul>
						<li><a href='viewstudentlist.php'><span>STUDENT LIST</span></a></li>
						<li><a href='dispatch_new.php'><span>DISPATCH LIST</span></a></li>
						<li><a href='disp.php'><span>DISPATCH NEW LIST</span></a></li>
						
					</ul>
			   </li>
			   <li><a href='#'><span>GENERATE LETTER</span></a>
					<ul>
						<li><a href='letter1.php'><span>MONTHLY COMMON</span></a></li>
						<!-- <li><a href='letter1a.php'><span>MONTHLY &lt65%</span></a></li>
						<li><a href='letter1b.php'><span>MONTHLY 65-75%</span></a></li> -->
						<li><a href='letter2a.php'><span>DEBARRED ST1|ST2</span></a></li>
						<li><a href='letter2b.php'><span>DEBARRED PUT|UT</span></a></li>
						<li><a href='letterpdp.php'><span>DEBARRED PDP</span></a></li>
						<li><a href='letter3.php'><span>PROVISIONALY CLEARED</span></a></li>
						<li><a href='letter4.php'><span>ABSENT FROM EXAM</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>COURSE COVERAGE</span></a>
					<ul>
						<li><a href='course_report.php'><span>GENERATE REPORT</span></a></li>
						<li><a href='course_setting.php'><span>COURSE SETTING</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>REPORT</span></a>
					<ul>
						<li><a href='report_admin.php'><span>COMPLETE REPORT</span></a></li>
						<li><a href='report_branch_dyn.php'><span>FILTERED DEPT WISE REPORT</span></a></li>
						<li><a href='report_branch.php'><span>BRANCH WISE REPORT</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>SETTINGS</span></a>
					<ul>
						<li><a href='updatesetting.php'><span>ALLOW UPDATE</span></a></li>
						<li><a href='changerollno.php'><span>CHANGE ROLL NO</span></a></li>
						<li><a href='allow_menu.php'><span>ALLOW MENU</span></a></li>
					</ul>
			   </li>
			</ul>
	</div>"; */
?> -->	
</aside>







