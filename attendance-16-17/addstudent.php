<?php
include 'magic.php';
include('menua.php');
session_start();
if(!isset($_SESSION['id']))
	{
		header('location:index.php');
	}
?>
<html>

<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />

<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>ADD STUDENT</title>
</head>


<script language="javascript" src="js/addstudent.js"></script>

<body>

<form method="POST" action="savestudent.php" onsubmit="return valid()">
	<div style="font-weight:bold;font-size:25px">ADD STUDENT</div>
	<table border="0">
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Enter All Details</td>
		</tr>
		<tr>
			<td width=200px style="font-weight:bold">
			Student ID</td>
			<td>
			<input type="text" name="uid" id="uid" size="20"></td>
		</tr>
		<tr>
			<td style="font-weight:bold">
			Name</td>
			<td>
			<input type="text" name="uname" id="uname" size="20"></td>
		</tr>
		<tr>
			<td style="font-weight:bold">
			Semester</td>
			<td>
			<input type="text" name="usem" id="usem" size="20"></td>
		</tr>
		<tr>
			<td style="font-weight:bold">
			Branch</td>
			<td><select name=ubranch id=ubranch>
			<option value="null">select</option>
			<option value="CSE">CSE</option>
			<option value="IT">IT</option>
			<option value="ME">ME</option>
			<option value="EN">EN</option>
			<option value="EE">EE</option>
			<option value="ECE">ECE</option>
			<option value="CE">CE</option>
			<option value="EI">EI</option>
			<option value="AS-HU">AS-HU</option>
			</select></td>
		</tr>
		<tr>
			<td style="font-weight:bold">
			Section</td>
			<td>
			<input type="text" name="usec" id="usec" size="20"></td>
		</tr>
	</table>

<p align="center"><input type="submit" value="Add" name="B1" style="width:110px">

	
</form>

</body>

</html>
