function valid()
{
	subjectid=document.getElementById("subjectid").value;
    subjectname=document.getElementById("subjectname").value;
    semester=document.getElementById("semester").value;
    branch=document.getElementById("branch").value;
    
    if(subjectid=="")
	{
		alert("Subjectid not given");
		return false;
	}
	else if(subjectname=="")
	{
		alert("Subject Name not given");
		return false;
	}
	else if(semester=="null"||semester==""||semester==0)
	{
		alert("Semester not given");
		return false;
	}

	else if(branch=="null"|| branch=="")
	{
		alert("Branch not given");
		return false;
	}
	else
	{
		return true;
	}

}
