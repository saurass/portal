function valid()
{
	currentpass=document.getElementById("oldpass").value;
    newpass=document.getElementById("newpass").value;
    repass=document.getElementById("repass").value;
    
    if(currentpass=="")
	{
		alert("currentpass not given");
		return false;
	}
	else if(newpass=="")
	{
		alert("newpass not given");
		return false;
	}
	else if(repass=="")
	{
		alert("repass not given");
		return false;
	}
	else if(repass!=newpass)
	{
		alert("New password and Retype Password doesn't match ");
		return false;
	}

	else
	{
		return true;
	}

}
