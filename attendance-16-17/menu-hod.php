<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="../assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>

<?php
	include 'require_login.php';
	include('db.inc');
	if($_SESSION['category']!='HOD')
	{
		header("location:index.php");
	}
?>


<!-- preloder round roun -->
<div class="preloading-screen">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
    <h4 class="ml-5 green-text text-darken-2"> GreenBoard! <i class="fa fa-pencil"></i></h4>
    <br><br>
    <h6 class="powered-by">Powered by Team OSS</h6>
</div>
<!-- /preloder round roun -->

<header class="navbar-fixed">
    <nav class="green darken-2">
        <div class="nav-wrapper">
            <a href="index.html" class="brand-logo center">GreenBoard <i class="fa fa-pencil right"></i></a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <img align="center" class="icon mx-5" src="../assets/images/profile-image-1.png" align="center"> 
                </li>
                <li> <a href="#"> <i class="fa fa-sign-out"></i> Logout &nbsp; </a> </li>
            </ul>
            <ul class="left hide-on-med-and-down">
                <li><a class="text-uppercase bold-text" href="#">Name of the Portal</a></li>
            </ul>
        </div>
    </nav>
</header>

<aside class="side-navigation green">
	<ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-plus"></i> Add <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>        
        <!-- Replace             -->
        
		    <li><a href='addfachod.php'><span>FACULTY</span></a></li>
			<li><a href='addstudh.php'><span>STUDENT</span></a></li>
			<li><a href='addsubhod.php'><span>SUBJECT</span></a></li>
			<li><a href='project_hd.php'><span>ADD/UPDATE ASSIGNROLE</span></a></li>
			<!--<li><a href='assignRolehod.php'><span>ASSINGN ROLE</span></a></li>
			<li><a href='fileUpload.php'><span>UPLOAD STUDENTS</span></a></li>-->
			<li><a href='project_feedback.php'><span>ASSINGN ROLE (feedback)</span></a></li>
		
		<!-- /Replace -->
		<li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>



    <ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-plus"></i> Update <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        <!-- Replace             -->
        
			<li><a href='updateFac.php'><span>FACULTY</span></a></li>
			<!--<li><a href='udpateSubject.php'><span>SUBJECT</span></a></li>-->
			<li><a href='updateAttendance.php'><span>ATTENDANCE</span></a></li>
			<li><a href='updatesubjecthod.php'><span>SUBJECT</span></a></li>
		
		<!-- /Replace -->
		<li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>

    <ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-plus"></i> Delete <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        <!-- Replace             -->
        
			<li><a href='delsub1.php'><span>SUBJECT</span></a></li>
			<li><a href='delstudh.php'><span>STUDENT</span></a></li>
			<!--<li><a href='deletefac.php'><span>FACULTY</span></a></li>
			<li><a href='updateDeleteAssignRole.php'><span>ASSINGN ROLE</span></a></li>-->
		
		<!-- /Replace -->
		<li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>

    <ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-plus"></i> View <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        <!-- Replace             -->
        
			<li><a href='viewFaculty.php'><span>FACULTY</span></a></li>
			<li><a href='newsubject.php'><span>SUBJECT</span></a></li>
			<li><a href='viewAttendanceHOD.php'><span>ATTENDANCE</span></a></li>
			<li><a href='viewSubject.php'><span>ASSINGN ROLE</span></a></li> 
			<li><a href='subject_faculty.php'><span>ASSINGN ROLE(Practical)</span></a></li> 
		
		<!-- /Replace -->
		<li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>
</aside>


<?php
/*echo"<div id='cssmenu'>
		       
                         <ul>
			   <li class='active'><a href='hodhome2.php'><span>HOME</span></a></li>
                          <!--  <li><a href='assign_hd.php'><span>ASSIGN SUBJECTS</span></a>-->";

              if($_SESSION['category']!='COORDINATOR')
              {               
			 echo  "<li><a href='#'><span>ADD</span></a>
					<ul>
					    <li><a href='addfachod.php'><span>FACULTY</span></a></li>
						<li><a href='addstudh.php'><span>STUDENT</span></a></li>
						<li><a href='addsubhod.php'><span>SUBJECT</span></a></li>
						<li><a href='project_hd.php'><span>ADD/UPDATE ASSIGNROLE</span></a></li>
						<!--<li><a href='assignRolehod.php'><span>ASSINGN ROLE</span></a></li>

						<li><a href='fileUpload.php'><span>UPLOAD STUDENTS</span></a></li>-->
						<li><a href='project_feedback.php'><span>ASSINGN ROLE (feedback)</span></a></li>
					</ul>
			                          
			   <li><a href='#'><span>UPDATE</span></a>
					<ul>
						<li><a href='updateFac.php'><span>FACULTY</span></a></li>
						<!--<li><a href='udpateSubject.php'><span>SUBJECT</span></a></li>-->
						<li><a href='updateAttendance.php'><span>ATTENDANCE</span></a></li>
						<li><a href='updatesubjecthod.php'><span>SUBJECT</span></a></li>

					</ul>
			   </li>
			      <li><a href='#'><span>DELETE</span></a>
					<ul>
					    
						<li><a href='delsub1.php'><span>SUBJECT</span></a></li>
						<li><a href='delstudh.php'><span>STUDENT</span></a></li>
						<!--<li><a href='deletefac.php'><span>FACULTY</span></a></li>
						<li><a href='updateDeleteAssignRole.php'><span>ASSINGN ROLE</span></a></li>-->
					</ul>
			   </li>";
			}
                             
			
			echo"  <li><a href='#'><span>VIEW</span></a>
				   <ul>
						<li><a href='viewFaculty.php'><span>FACULTY</span></a></li>
						<li><a href='newsubject.php'><span>SUBJECT</span></a></li>
						<li><a href='viewAttendanceHOD.php'><span>ATTENDANCE</span></a></li>
						<li><a href='viewSubject.php'><span>ASSINGN ROLE</span></a></li> 
						
					</ul>
			   </li>";

                          if($_SESSION['category']!='COORDINATOR')
                          {echo"   <li><a href='#'><span>DOWNLOAD</span></a>
				   <ul>
						<li><a href='viewstudentlist.php'><span>STUDENT LIST</span></a></li>
						<li><a href='dispatch_new.php'><span>DISPATCH LIST</span></a></li>
						
					</ul>
			   </li>";
			}

			   
			   echo" <li><a href='#'><span>GENERATE LETTER</span></a>
					<ul>
						<li><a href='letter1.php'><span>MONTHLY COMMON</span></a></li>
						<!-- <li><a href='letter1a.php'><span>MONTHLY &lt65%</span></a></li>
						<li><a href='letter1b.php'><span>MONTHLY 65-75%</span></a></li> -->
						<li><a href='letter2a.php'><span>DEBARRED ST1|ST2</span></a></li>
						<li><a href='letter2b.php'><span>DEBARRED PUT|UT</span></a></li>
						<li><a href='letterpdp.php'><span>DEBARRED PDP</span></a></li>
						<li><a href='letter3.php'><span>PROVISIONALY CLEARED</span></a></li>
                        <li><a href='letter4.php'><span>ABSENT FROM EXAM</span></a></li> 
					</ul>
                     	   </li>";

                      
                if($_SESSION['category']!='COORDINATOR')
                {           
                        
			   echo "<li><a href='#'><span>COURSE COVERAGE</span></a>
					<ul>
						<li><a href='course_report.php'><span>GENERATE REPORT</span></a></li>
					</ul>
			   </li>
			    
			   <li><a href='#'><span>DEPARTMENT WISE REPORT</span></a>
					<ul>
						<li><a href='report_hod.php'><span>GENRATE REPORT</span></a></li>
						<li><a href='report_branch_dyn_hod.php'><span>CUSTOMISED REPORT</span></a></li>
					</ul>
			   </li>

			   <li><a href='#'><span>ELECTIVE</span></a>
					<ul>
						<li><a href='allot_oe.php'><span>ASSIGN SUBJECTS</span></a></li>
						<li><a href='student_list_oe.php'><span>VIEW SUBJECTS</span></a></li>
					</ul>
			   </li>

                         
			</ul>
	</div>";}
	echo "<div id='space'></br></br></br></br></div>";*/
?>
