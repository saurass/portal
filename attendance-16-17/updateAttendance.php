<?php
include 'magic.php';
include('menuh.php');
?>
<html>

<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />

<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>UPDATE ATTENDANCE</title>
<script lang="javascript" src="js/updateattendance.js"></script>
</head>
<script>

function getsection(sem)
{

	var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","showsection.php?sem="+sem,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("section").innerHTML=xmlhttp.responseText;
			}
		}
}
function getsubid(sec)
{
	sem=document.getElementById("semester").value;
	
	var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","showsubjectid.php?sem="+sem+"&sec="+sec,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("subjectid").innerHTML=xmlhttp.responseText;
			}
		}
}
function getsubjectname(str)
{

	var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","showsubjectname.php?sid="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("subjectname").value=xmlhttp.responseText;
			}
		}	
}

function getdata()
{
var sem=document.getElementById("semester").value;
var sec=document.getElementById("section").value;
var subid=document.getElementById("subjectid").value;
	
if(sem==''){
	alert('SEMESTER field is missing');
	return false;
}
else if(sec==''){
	alert('SECTION field is missing');
	return false;
}
else if(subid==''){
	alert('SUBJECTID field is missing');
	return false;
}
	
		var xmlhttp;
		
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","updatedates.php?subid="+subid+" & sem="+sem+" & sec="+sec,true);
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
				document.getElementById("txt").innerHTML=xmlhttp.responseText;
			}
		}

}

</script>
<body >

<form  method="POST" action="">
	
	<div style="font-weight:bold;font-size:25px">UPDATE ATTENDANCE</div>
		

	<table border="0">
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Select all fields*</td>
		</tr>
		<tr>
			<td>Semester</td>
			<td><select size="1" name="semester" id="semester" onchange=getsection(this.value)>
			<option value=>select</option>
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
			<option value=5>5</option>
			<option value=6>6</option>
			<option value=7>7</option>
			<option value=8>8</option>
			</select></td>
			
		</tr>
		<tr>
			<td>Section</td>
			<td><select size="1" name="section" id="section" onchange=getsubid(this.value)></select></td>
		</tr>
		<tr>
			<td>Subject ID</td>
			<td><select size="1" name="subjectid" id="subjectid" onchange=getsubjectname(this.value)></select></td>
		</tr>
		<!-- <tr>
			<td>Subject Name</td>
			<td><input type="text" name="subjectname" id=subjectname size="40"></td>
		</tr>-->
		<tr>
			<td colspan="2" style="text-align:center"><input type="button" value="Submit" name="B1" id="submit" onclick=getdata()></td>
		</tr>
		 
	</table>
	
	
	
</form>
<div id=txt></div>
<input type=hidden id="studentcount">
<input type=hidden id="validornot">
</body>

</html>