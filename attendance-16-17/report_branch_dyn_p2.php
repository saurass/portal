<?php
include 'magic.php';
include('connect.php');

echo '<link rel="stylesheet" type="text/css" href="css/finallook.css" />
<link rel="stylesheet" type="text/css"  href="css/styles.css" />
<link rel="stylesheet" type="text/css"  href="css/common.css" />
<link rel="stylesheet" type="text/css"  href="css/button.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />';
echo "
<style>

@media print {
	input {
		display: none;
	}
}

	
	body 
	{
		font-family:arial;
	}
	
	form 
	{
		display:none;
	}
	#cssmenu 
	{
		display:none;
	}
	#header1 
	{
		display:none;
	}
	#space 
	{
		display:none;
	}
	input
	{
		display: none;  
	}
    .desc
    {
    	text-transform:none;
    }
	table{
		color:#000000;
		border:1px solid black;
		border-collapse:collapse;
		width:100%;
	}
	table tr td{
		border:1px solid black;
		padding:3px;
		font-size:14px;
	}
	table tr{
		background-color: white !important;
	}
	table tr:first-child td{ 
		font-weight:900; 
	}


</style>";

?>
<script type="text/javascript">
function printpage()
{
	window.print();
}
</script>
<p style='text-align:left;margin-left:10%'><input type='button' value='PRINT' onclick='printpage()' /></p>
<?php
$branch=$_POST['branch'];

if($branch=="CSE")
$bran="Computer Science Engineering";

elseif($branch=="ME")
$bran="Mechanical Engineering";

elseif($branch=="EN")
$bran="Electrical Engineering ";

else if($branch=="IT")
$bran="Information Technology ";

else if($branch=="EE")
$bran="Electrical and Electronics  Engineering ";

else if($branch=="AS-HU")
$bran="Applied Science and Humanities";

else if($branch=="ECE")
$bran="Electronics and Communication Engineering";

else if($branch=="MBA")
$bran="MBA";

if($branch=='MBA')
echo "<div id='report' width='80%' align='center'>
				<div align='center' style='font-weight:bold;font-size:20px'>
					<br>AJAY KUMAR GARG INSTITUTE OF MANAGEMENT,GHAZIABAD<br>
				</div>";
else
echo "<div id='report' width='80%' align='center'>
				<div align='center' style='font-weight:bold;font-size:20px'>
					<br>AJAY KUMAR GARG ENGINEERING COLLEGE,GHAZIABAD<br>
				</div>";


				
echo "<div align='center' style='font-weight:600;font-size:16px'>
					<br>ODD SEM: 2018-19<br>DEPARTMENT OF $bran
				</div>
				<div align='left' class='desc' style='width:80%;font-size:16px'>
					<br><br>To<br>The Controller of Examination<br>AKG Engineering College<br>Ghaziabad<br><br>The list of students detained from the $_POST[type] is forwarded for your necessary action as per the desired format as given under.
				</div>";
				
			$q_sem="SELECT DISTINCT semester FROM student WHERE branch='$branch' ORDER BY semester ASC";
			mysql_select_db('portal');
			$getsem = mysql_query( $q_sem, $conn );
			$sem_count=0;
			while($row2 = mysql_fetch_array($getsem, MYSQL_ASSOC)){
				$sem_count++;
			}	
			$sem_count++;
			
			$q_sem="SELECT DISTINCT semester FROM student WHERE branch='$branch' ORDER BY semester ASC";
			mysql_select_db('portal');
			$getsem = mysql_query( $q_sem, $conn );
			while($row2 = mysql_fetch_array($getsem, MYSQL_ASSOC)){
				$sem=$row2['semester'];

				$q_sec="SELECT DISTINCT section FROM student WHERE branch='$branch' AND semester='$sem' ORDER BY section ASC";
				$getsec = mysql_query($q_sec);
				while($row3 = mysql_fetch_array($getsec)){
					$sec=$row3['section'];
					if($sem=='1')
						$roman='I';
					elseif($sem=='2')
						$roman='II';
					elseif($sem=='3')
						$roman='III';
					elseif($sem=='4')
						$roman='IV';
					elseif($sem=='5')
						$roman='V';
					elseif($sem=='6')
						$roman='VI';
					elseif($sem=='7')
						$roman='VII';
					elseif($sem=='8')
						$roman='VIII';
					echo "<p style='font-weight:bold;text-align:left;width:80%;margin-bottom:0px;padding:0px;'>$roman Sem ($sec)</p>";
					echo "<table align='center' width='80%' style='margin-top:0px' class='table_style'>
								<tr>
									<td style='width:10%'><b>Sl. No.</b></td>
									<td style='width:15%'><b>Roll No</b></td>
									<td><b>Student Name</b></td>
									<td style='width:15%'><b>Paper Code(s) in which the students is detained</b></td>
									<td style='width:15%'><b>Reason of detainment</b></td>
								</tr>";
					$i=1;
					$fdate=$_POST['fdate'];
					$tdate=$_POST['tdate'];
					$get_student="SELECT * FROM student WHERE semester='$sem' AND branch='$branch' AND section='$sec' ORDER BY st_id";
					mysql_select_db('portal');
					$getstudent = mysql_query( $get_student, $conn );
					$ids = array();
					$names = array();
					while($row3 = mysql_fetch_array($getstudent, MYSQL_ASSOC)){
						$studentid=$row3['st_id'];
						$name=$row3['name'];
						$get_att="SELECT * FROM attendance WHERE st_id='$studentid' and fromdate between DATE_FORMAT('$fdate','%Y-%m-%d') and DATE_FORMAT('$tdate','%Y-%m-%d') and todate between DATE_FORMAT('$fdate','%Y-%m-%d') and DATE_FORMAT('$tdate','%Y-%m-%d')";
						mysql_select_db('portal');
						$getatt = mysql_query( $get_att, $conn );
						$total=0;
						$attend=0;
						while($row4 = mysql_fetch_array($getatt, MYSQL_ASSOC)){
							$total+=$row4['totalclasses'];
							$attend+=$row4['attended'];
						}
						$percentage=round($attend/$total*100,2);
						if($percentage>=$_POST['lower'] && $percentage<=$_POST['upper']){
							$ids[$i]=$studentid;
							$names[$i]=$name;
							$i++;
						}
					}
					$i=1;
					$count=count($ids);
					if($count==0)
						echo "<tr><td style='width:10%'>NIL</td>
							<td style='width:15%'>NIL</td>
							<td style='text-align:left'>NIL</a></td>
						<td  style='width:15%'>NIL</td><td style='width:15%'>NIL</td></tr>";
					else
					foreach ($ids as $id) {
						echo "<tr><td style='width:10%'>".($i++)."</td>
							<td style='width:15%'>".$id."</td>
							<td style='text-align:left'>".$names[$i-1]."</a></td>";
						if($i==2) echo "<td  style='width:15%' rowspan='$count'>All</td><td rowspan='$count' style='width:15%'>Short Attendance</td>";
						echo "</tr>";
					}
					echo '</table>';
				}
			}
	$hodname=mysql_fetch_array(mysql_query("SELECT name FROM login WHERE category='HOD' AND branch='$branch'"));
	echo "<br><br><br><br><br><div align='left' style='width:80%;font-weight:600;font-size:16px'>
			$hodname[0]<br>HOD $branch
			</div>";
	echo "<br><div class='desc' align='left' style='margin-left:40%;width:80%;font-weight:600;font-size:14px'>
			Received by:<br><br><br>Name & Signature of the Member of Examination Cell with Date
			</div>";
	echo "</div>";
?>
