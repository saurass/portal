<?PHP
/*
File Name: analyzeCSV.php
File URI: http://k-fez.com/
Description: This scripts verifies that a CSV file has been uploaded via HTTP POST and then analyzes the file's data and reports back to the user.
Author: Kevin Pheasey
Author URI: http://k-fez.com/
Version: 1.0
*/

/*  Copyright (C) 2010  Kevin Pheasey (email: kevin at k-fez.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
include 'magic.php';
if($_FILES["file"]["type"] != "application/vnd.ms-excel"){
	die("This is not a CSV file.");
}
elseif(is_uploaded_file($_FILES['file']['tmp_name'])){
	session_start()
	//Connect to the database
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = 'HPv185e@#%';
	$dbname = 'attendance';
	$link = mysql_connect($dbhost, $dbuser, $dbpass) or die('Error connecting to mysql server');
	mysql_select_db($dbname);
	
	//Process the CSV file
	$findings = "
<form method=\"post\" action=\"importCSV.php\">
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td>Result</td>
    <td>Attribute 0</td>
    <td>Attribute 1</td>
    <td>Attribute 2</td>
    <td>Attribute 3</td>
  </tr>";
	
	$handle = fopen($_FILES['file']['tmp_name'], "r");
	//$data = fgetcsv($handle, 1000, ";"); //Remove if CSV file does not have column headings
	while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
		$att0 = mysql_real_escape_string($data[0]);
		$att1 = mysql_real_escape_string($data[1]);
		$att2 = mysql_real_escape_string($data[2]);
		$att3 = mysql_real_escape_string($data[3]);
		
		//Check if row is in database already
		$sql = "SELECT *
				FROM `tbl_name`
				WHERE `attribute0` = '" . $att0 . "'";	//In this example attribute0 is the primary key	
		$result = mysql_query($sql);
		$count = mysql_num_rows($result);
		if($count > 0){
			$findings = $findings . "
  <tr>
    <td bgcolor=\"#FF0000\">DB Duplicate</td>
    <td>" . $att0 . "</td>
    <td>" . $att1 . "</td>
    <td>" . $att2 . "</td>
    <td>" . $att3 . "</td>
  </tr>";
		}
		
		//Check if row is already in INSERT queue
		elseif(strpos($_SESSION['insert'], "'" . $att0 . "'") != false){
			$findings = $findings . "
  <tr>
    <td bgcolor=\"#FF0000\">File Duplicate</td>
    <td>" . $att0 . "</td>
    <td>" . $att1 . "</td>
    <td>" . $att2 . "</td>
    <td>" . $att3 . "</td>
  </tr>";
  		}
		
		//Row is unique
		else{
			//Add INSERT statement to INSERT queue
			$_SESSION['insert'] = $_SESSION['insert'] . "INSERT INTO `student` (
					`studentid` ,
					`name` ,
					`semester` ,
					`branch`,`calyear`,`section`
					)
					VALUES ('" . $att0 . "', '" . $att1 . "', '" . $att2 . "', '" . $att3 . "');";
			
			//Add row for row to findings table and mark unique
			$findings = $findings . "
  <tr>
    <td bgcolor=\"#00FF00\">&nbsp;</td>
    <td>" . $att0 . "</td>
    <td>" . $att1 . "</td>
    <td>" . $att2 . "</td>
    <td>" . $att3 . "</td>
  </tr>";
		}
	}
	mysql_close($link);
	
	$findings = $findings . "  
  <tr>
    <td colspan=\"5\"><div align=\"center\"><input type=\"submit\" value=\"Confirm\" /></div></td>
  </tr>
</table>
</form>";
	echo $findings;
}
else{
	die("You shouldn't be here");
}
?>
