 </main>
    </section>
</div>



<aside class="right-aside">
    <div class="open-seasame green darken-2 white-text">Open Portals <i class="fa fa-arrow-up"></i></div>
    <ul class="collection with-header center green-text">
        <li class="collection-header"><h5>Portals</h5></li>
        <a href="marks.html" class="collection-item">Marks Portal</a>
        <a href="attendance.html" class="collection-item active">Attendance Portal</a>
        <a href="student.html" class="collection-item">Student Portal</a>
    </ul>
</aside>

<aside class="side-navigation green hide-on-med-and-down">
    <ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-plus"></i> Add <i class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        <li><a href="#">hod</a></li>
        
        <li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>
   
    
    
     <ul>
        <a class="waves-effect waves-ripple" href="print_report_card.php"> <i class="fa fa-bookmark-o"></i> Student Info<i class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
    </ul>
    <ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-bookmark-o"></i> Customized Info<i class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        
         <li><a href="name-wise-detail.php">name wise detail</a></li>
         <li><a href="custom-info-complete-form.php">complete information</a></li>
         <li><a href="section-wise-action.php">section wise action</a></li>
         


        
    </ul>


</aside>
<footer class="container">
    <hr>
    <div class="right-align py-5">
        &copy; 2017, OSS R&D Web Development Team.
    </div>
</footer>

<script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/plugins/materialize/js/materialize.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
</body>
</html>