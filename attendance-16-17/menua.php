<html>
<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />
<link rel="stylesheet" type="text/css"  href="css/styles.css" />
<link rel="stylesheet" type="text/css"  href="css/common.css" />
<link rel="stylesheet" type="text/css"  href="css/button.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
</head>

<?php
include 'require_login.php';
if($_SESSION['category']!='ADMIN')
{
	header("location:index.php");
}
echo"
<div id='header1'>
<div id='header_text1'>ATTENDANCE PORTAL</div>
<div id='header_right' align='right'>   
<ul class='button-group'>
<li><a class='button pill' href='../index.php'>Green Board</a></li>
<li><a class='button pill' href='../logout-process.php'>Logout</a></li>
</ul>
</div> 
</div>";
echo"<div id='cssmenu'>
			<ul>
			   <li class='active'><a href='adminhome.php'><span>HOME</span></a></li>
			   <li><a href='#'><span>ADD</span></a>
					<ul>
						<li><a href='addHOD.php'><span>HOD</span></a></li>
						<li><a href='addfacadmin.php'><span>FACULTY</span></a></li>
						<li><a href='addLabAssistantAdmin.php'><span>Add Lab Assistant</span></a></li>
						<li><a href='add_student.php'><span>STUDENT</span></a></li>
						<li><a href='addsubadmin.php'><span>SUBJECT</span></a></li>
						<li><a href='addattendanceadmin.php'><span>ATTENDANCE</span></a></li>
						<li><a href='project.php'><span>ASSINGN ROLE</span></a></li>

						<li><a href='project_feedback.php'><span>ASSINGN ROLE (feedback)</span></a></li>

						<li><a href='fileuploadadmin.php'><span>UPLOAD STUDENTS</span></a></li>
						<li><a href='subjectupload.php'><span>UPLOAD SUBJECTS</span></a></li>
						<li><a href='facultyupload.php'><span>UPLOAD FACULTY</span></a></li>
                        <li><a href='uploadassignrole.php'><span>UPLOAD ASSIGN ROLE</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>UPDATE</span></a>
					<ul>
						<li><a href='updatehodadmin.php'><span>HOD</span></a></li>
						<li><a href='updatefacultyadmin.php'><span>FACULTY</span></a></li>
						<li><a href='edit_student.php'><span>STUDENT</span></a></li>
						<li><a href='updatesubjectadmin.php'><span>SUBJECT</span></a></li>
						<li><a href='updateattendanceadmin.php'><span>ATTENDANCE</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>DELETE</span></a>
					<ul>
						<li><a href='deleteHODform.php'><span>HOD</span></a></li>
						<li><a href='deletefacultyadmin.php'><span>FACULTY</span></a></li>
						<li><a href='delete_student.php'><span>STUDENT</span></a></li>
						<li><a href='newdelad.php'><span>SUBJECT</span></a></li>
						<li><a href='new2.php'><span>SEMESTER</span></a></li>
						<!--<li><a href='updatedeleteassignadmin.php'><span>ASSINGN ROLE</span></a></li>-->
						
					</ul>
			   </li>
			   <li><a href='#'><span>VIEW</span></a>
				   <ul>
						<li><a href='viewfacultyadmin.php'><span>FACULTY</span></a></li>
						<li><a href='viewsubjectadmin.php'><span>ASSIGNROLE</span></a></li>
						<li><a href='viewattendanceadmin.php'><span>ATTENDANCE</span></a></li>
						 <li><a href='newsubad.php'><span>SUBJECT</span></a></li>
						
					</ul>
			   </li>
			   <li><a href='#'><span>DOWNLOAD</span></a>
				   <ul>
						<li><a href='viewstudentlist.php'><span>STUDENT LIST</span></a></li>
						<li><a href='dispatch_new.php'><span>DISPATCH LIST</span></a></li>
						<li><a href='disp.php'><span>DISPATCH NEW LIST</span></a></li>
						
					</ul>
			   </li>
			   <li><a href='#'><span>GENERATE LETTER</span></a>
					<ul>
						<li><a href='letter1.php'><span>MONTHLY COMMON</span></a></li>
						<li><a href='letter1a.php'><span>MONTHLY &lt65%</span></a></li>
						<!--<li><a href='letter1b.php'><span>MONTHLY 65-75%</span></a></li> -->
						<li><a href='letter2a.php'><span>DEBARRED ST1|ST2</span></a></li>
						<li><a href='letter2b.php'><span>DEBARRED PUT|UT</span></a></li>
						<li><a href='letterpdp.php'><span>DEBARRED PDP</span></a></li>
						<li><a href='letter3.php'><span>PROVISIONALY CLEARED</span></a></li>
						<li><a href='letter4.php'><span>ABSENT FROM EXAM</span></a></li>
						<li><a href='consecutiveabsentletter.php'><span>Consecutive Five Absent</span></a></li>
						
					    <li><a href='massbunk.php'><span>Mass Bunk</span></a></li>
					    
					    <li><a href='dispatchlist.php'><span>Dispatch List</span></a></li>
					    <li><a href='Dispatchlist123.php'><span>Dispatch List PDP</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>COURSE COVERAGE</span></a>
					<ul>
						<li><a href='course_report.php'><span>GENERATE REPORT</span></a></li>
						<li><a href='course_setting.php'><span>COURSE SETTING</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>REPORT</span></a>
					<ul>
						<li><a href='report_admin.php'><span>COMPLETE REPORT</span></a></li>
						<li><a href='report_branch_dyn.php'><span>FILTERED DEPT WISE REPORT</span></a></li>
						<li><a href='report_branch.php'><span>BRANCH WISE REPORT</span></a></li>
					</ul>
			   </li>
			   <li><a href='#'><span>SETTINGS</span></a>
					<ul>
						<li><a href='updatesetting.php'><span>ALLOW UPDATE</span></a></li>
						<li><a href='changerollno.php'><span>CHANGE ROLL NO</span></a></li>
						<li><a href='allow_menu.php'><span>ALLOW MENU</span></a></li>
						<li><a href='change_anonymous_pass.php'><span>Change Password</span></a></li>
					</ul>
			   </li>
			</ul>
	</div>";
	echo "<div id='space'></br></br></br></br></div>";
?>
