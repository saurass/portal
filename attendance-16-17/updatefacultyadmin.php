<?php
include 'magic.php';
include('menua.php');
?>
<html>

<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />

<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>UPDATE FACULTY</title>
<script language="javascript" src="facultyvalidation.js"></script>
</head>

<script>
	function showdetails()
	{
		var str=document.getElementById("uid").value;
		if(str==''){
			alert("please enter Faculty ID");
			return false;
		}

			
		if(str.length==0)
		{
			document.getElementById("txtHint").innerHTML="";
			return;
		}
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","search.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
				if(xmlhttp.responseText==''){
				alert("please enter valid Faculty ID");

				}
				else{
				document.getElementById("uname").value=xmlhttp.responseText;
				document.getElementById("uid").disabled=true;
				show();
				}
			}
		}
	}
	function showdetailstwo()
	{
		var str=document.getElementById("uid").value;	
		if(str.length==0)
		{
			document.getElementById("txtHint").innerHTML="";
			return;
		}
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","searchbranch.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
				//document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
				//document.getElementById("ubranch").innerHTML=xmlhttp.responseText;
				document.getElementById("ubranch").value=xmlhttp.responseText;
			}
		}
	}
	function message()
	{
	//alert("Updated");
	}
	function addhidden()
	{
	var id=document.getElementById("uid").value;
	document.getElementById("hid").value=id;
	}

	function init(){
		document.getElementById("a").style.visibility='hidden';
		document.getElementById("b").style.visibility='hidden';
		document.getElementById("c").style.visibility='hidden';
		document.getElementById("e").style.visibility='hidden';
	}
	function show(){
		document.getElementById("a").style.visibility='visible';
		document.getElementById("b").style.visibility='visible';
		document.getElementById("c").style.visibility='visible';
		document.getElementById("e").style.visibility='visible';
		document.getElementById("d").style.visibility='hidden';
		
	}
</script>
<body onload=init()>


<form method="POST" action="updatefaculty.php" >
	
		<div style="font-weight:bold;font-size:25px">UPDATE FACULTY</div>
        
		<table border="0">
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Enter All Details</td>
		</tr>
		<tr>
			<td width=200px style="font-weight:bold">
			Faculty ID</td>
			<td>
			<input type="text" name="uid" id="uid" size="20" ></td>
			<input type=hidden id=hid name=hid>
			<td><input type="button" value="GET" onclick=showdetails();showdetailstwo();addhidden() id="d"></td>
		</tr>
		<tr id='a'>
			<td style="font-weight:bold">
			Name</td>
			<td colspan='2'>
			<input type="text" name="uname" id="uname" size="20"></td>
		</tr>
		<tr id='b'>
			<td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Select Branch</td>
		</tr>
		<tr id='c'>
			<td style="font-weight:bold">Branch</td>
			<td colspan='2'><select name=ubranch id=ubranch><option value="null">select</option>
			<option value="CSE">CSE</option>
			<option value="IT">IT</option>
			<option value="ME">ME</option>
			<option value="EN">EN</option>
			<option value="EE">EE</option>
			<option value="ECE">ECE</option>
			<option value="CE">CE</option>
			<option value="EI">EI</option>
			<option value="AS-HU">AS-HU</option>
			</select></td>
		</tr>
		<tr><div id=txtHint></div></tr>
	</table>

			
		<input type="submit" value="Update" name="B1" style="width:110px" id='e'></td>
		
	</div>
	
</form>

</body>

</html>
