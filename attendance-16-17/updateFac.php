<?php
include 'magic.php';
include('menuh.php');
?>
<html>

<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>UPDATE FACULTY</title>
<script language="javascript" src="facultyvalidation.js"></script>
</head>

<script>
	function showdetails(str)
	{
			
		if(str.length==0)
		{
			document.getElementById("txtHint").innerHTML="";
			return;
		}
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","search.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{

				document.getElementById("uname").value=xmlhttp.responseText;
				document.getElementById("uid").disabled=true;
			}
		}
	}
	function showdetailstwo(str)
	{
			
		if(str.length==0)
		{
			document.getElementById("txtHint").innerHTML="";
			return;
		}
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","searchbranch.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
				//document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
				document.getElementById("ubranch").value=xmlhttp.responseText;
			}
		}
	}
	function message()
	{
	alert("Updated");
	}
	function addhidden()
	{
	var id=document.getElementById("uid").value;
	document.getElementById("hid").value=id;
	}
</script>
<body>


<form id"form3" method="POST" action="updatefaculty.php" onsubmit="return valid()">
	<h3><b>UPDATE FACULTY</b></h3>
	<table border="0">
		
		<tr>
			<td width=200px>
			Faculty ID</td>
			<td>
			<input type="text" name="uid" id="uid" size="20" onchange=showdetails(this.value);showdetailstwo(this.value);addhidden()></td>
			<input type=hidden id=hid name=hid>
		</tr>
		<tr>
			<td>
			Name</td>
			<td>
			<input type="text" name="uname" id="uname" size="20"></td>
		</tr>
		<tr>
			<td>Branch</td>
			<td><select name=ubranch id=ubranch>
			<option value="null">select</option>
			<option value="CE">Civil Engineering(CE)</option><option value="CSE">Computer Science and Engineering(CSE)</option><option value="ME">Mechanical Engineering(ME)</option><option value="AS-HU">Applied Science and Humanities(AS-HU)</option>
			<option value="IT">Information Technology(IT)</option><option value="EN">Electrical and Electronics  Engineering(EN)</option><option value="EI">Electrical and Instrumentation Engineering(EI)</option><option value="MBA">Electronics and Communication Engineering(ECE)</option> <option value="MCA">Master of Computer Application(MCA)</option><option value="MBA">Master of Business Application(MBA)</option></select></td>
		</tr>

		<tr><div id=txtHint></div></tr>
	</table>
	<input type="submit" value="Update" name="B1">
	
</form>

</body>

</html>
