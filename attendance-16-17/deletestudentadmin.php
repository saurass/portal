<?php
include 'magic.php';
include('menua.php');
?>

<html>

<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />

<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script src="js/addfaculty.js" lang="javascript"></script>

<title>DELETE STUDENT</title>

<script>
    function getname() {
        var str = document.getElementById("uid").value;

        if (str == '') {
            alert("Please enter Student ID");
            return false;
        }

        if (str.length == 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        var xmlhttp;

        if (window.XMLHttpRequest) {
            //code for IE7,firefox,chrome,opera,safari  

            xmlhttp = new XMLHttpRequest();
        }
        else {

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.open("GET", "getstudentname.php?q=" + str, true)
        xmlhttp.send();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                if (xmlhttp.responseText == '') {
                    alert("Please enter valid Student ID");
                }
                else {
                    show();
                    document.getElementById("uname").value = xmlhttp.responseText;
                    document.getElementById("uid").disabled = true;
                }
            }
        }
    }
    function getsem() {
        var str = document.getElementById("uid").value;

        if (str.length == 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        var xmlhttp;

        if (window.XMLHttpRequest) {
            //code for IE7,firefox,chrome,opera,safari  

            xmlhttp = new XMLHttpRequest();
        }
        else {

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.open("GET", "getstudentsem.php?q=" + str, true)
        xmlhttp.send();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

               document.getElementById("usem").value = xmlhttp.responseText;
               

            }
        }
    }
    function getsec() {
        var str = document.getElementById("uid").value;


        if (str.length == 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        var xmlhttp;

        if (window.XMLHttpRequest) {
            //code for IE7,firefox,chrome,opera,safari  

            xmlhttp = new XMLHttpRequest();
        }
        else {

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.open("GET", "getstudentsec.php?q=" + str, true)
        xmlhttp.send();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {


                document.getElementById("usec").value = xmlhttp.responseText;

            }
        }
    }

    function hide() {
        document.getElementById("a").style.visibility = 'hidden';
        document.getElementById("b").style.visibility = 'hidden';
        document.getElementById("c").style.visibility = 'hidden';
        document.getElementById("d").style.visibility = 'hidden';
        document.getElementById("e").style.visibility = 'hidden';
    }

    function show() {
        document.getElementById("a").style.visibility = 'visible';
        document.getElementById("b").style.visibility = 'visible';
        document.getElementById("c").style.visibility = 'visible';
        document.getElementById("d").style.visibility = 'visible';
        document.getElementById("e").style.visibility = 'visible';
        document.getElementById("g").style.visibility = 'hidden';
    }


    function getbranch() {
        var str = document.getElementById("uid").value;

        if (str.length == 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        var xmlhttp;

        if (window.XMLHttpRequest) {
            //code for IE7,firefox,chrome,opera,safari  

            xmlhttp = new XMLHttpRequest();
        }
        else {

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.open("GET", "getstudentbranch.php?q=" + str, true);
        xmlhttp.send();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
                //document.getElementById("ubranch").value = xmlhttp.responseText;   
                var chk1,chk2;
                chk1 = xmlhttp.responseText;
                chk1 = chk1.trim();
                for(i=0;i<=10;i++){
                chk2 = document.getElementsByTagName("option")[i].value;
                if(chk1==chk2){
                    document.getElementById("ubranch").selectedIndex = i; 
                    break;
                }
                }                                                             
            }
        }
    }
    function addhidden()
    {
    var id=document.getElementById("uid").value;
    document.getElementById("hid").value=id;
    }

</script>

</head>

<body onload=hide()>



<form method="POST" action="deletestudent.php" onsubmit="return valid()">

			
			<div style="font-weight:bold;font-size:25px">DELETE STUDENT</div>
	<table>
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Enter Student ID</td>
		</tr>
		<tr>
			<td width=200px>
			Student ID</td>
			<td>
			<input type="text" name="uid" id="uid" size="20" >
            <input type=hidden id=hid name=hid>
            
            <input type="button" value="GET" id="g" onclick="getname();getbranch();getsec();getsem(); addhidden();">
            </td>
		<!--<input type=text id=hid name=hid value=""> 	-->	
            
        </tr>
		<tr id="a">
			<td>
			Name</td>
			<td>
			<input type="text" name="uname" id="uname" size="20"></td>
		</tr>
		<tr id="b">
			<td>Branch</td>
			<td>
            <select name="ubranch" id="ubranch">
                <option value="null">select</option>
                <option value="CSE">Computer Science</option>
                <option value="ME">Mechanical Engineering</option>
                <option value="AS">Applied Science</option>
                <option value="IT">Information Technology</option>
                <option value="EN">Electrical Engineering</option>
                <option value="EE">Electrical and Electronics  Engineering</option>
                <option value="EC">Electronics and Communication Engineering</option>
                <option value="CE">Civil Engineering</option>
                <option value="EI">Electrical and Instrumentation</option>
                <option value="MCA">Masters of Computer Applications</option>

            </select>
			</td>
		</tr>
		<tr id="c">
			<td>Semester</td>
			<td>
			<input type="text" name="usem" id="usem" size="20"></td>
		</tr>
		<tr id="d">
			<td>Section</td>
			<td>
			<input type="text" name="usec" id="usec" size="20"></td>
		</tr>
		
		
	</table>

			
			<input type="submit" value="Delete" id="e" name="B1" style="width:110px"></td>
		
	</div>
	
</form>

</body>

</html>