<?php
include 'magic.php';
include('connect.php');
include('menuh.php');

if($_SESSION['category']!='HOD')
	header('Location: index.php');

echo "
<style>

@media print {
	
	body 
	{
		font-family:arial;
	}
	
	form 
	{
		display:none;
	}
	#cssmenu 
	{
		display:none;
	}
	#header1 
	{
		display:none;
	}
	#space 
	{
		display:none;
	}
	input
	{
		display: none;  
	}
    
	table{
		color:#000000;
		border:1px solid black;
		border-collapse:collapse;
		width:100%;
	}
	table tr td{
		border:1px solid black;
		padding:3px;
		font-size:16px;
	}
	table tr:first-child td{ 
		font-weight:900; 
	}

}	

</style>";

?>
<script type="text/javascript">
function printpage()
{
	window.print();
}
</script>
<form  method="POST" action="report_branch_dyn_hod.php">
		<div style="font-weight:bold;font-size:20px">DEPARTMENT WISE ATTENDANCE REPORT</div>	
		<table align=center>
		<tr>
			<td colspan="3" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Select Branch*</td>
		</tr>
		<tr>
			<td><b>EXAM</b></td>
			<td colspan="2"><select name='type' required>
				<option value='ST-1'>ST-1</option>
				<option value='ST-2'>ST-2</option>
				<option value='PUT'>PUT</option>
				</select>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="font-weight:bold">Date</td>
			<td style="text-align:right;font-weight:bold">FROM</td>
			<td><input type="date" name="fdate" id="fdate" size="4"></td>
		</tr>	
		<tr>
			<td style="text-align:right;font-weight:bold">TO</td>
			<td><input type="date" name="tdate" id="tdate" size="4"></td>
		</tr>
		<tr>
			<td><b>FILTER</b></td>
			<td colspan="2"><?php
				echo "<input type='hidden' name='branch' value='$_SESSION[branch]' >";
				?>
				FROM &nbsp;&nbsp;<input type=text value=0 name=lower size=4/>&nbsp;&nbsp;TO &nbsp;&nbsp;<input type=text value=100 name=upper size=4/></td>
		</tr>
		</table>
		<input type="submit" value="SUBMIT" name="submit">
			
</form>
<?php
if(isset($_POST['submit'])){
echo "<table align='center' width='80%' style='margin-top:0px' class='table_style'>
	<tr><td colspan=2 style='text-align:center'>DEBARRED</td><td colspan=2 style='text-align:center'>PROVISIONALLY CLEARED</td></tr>";
echo '<tr><td style="text-align:center"><form action="report_branch_dyn_p1.php" method="post" target="_blank">
		<input type="hidden" name="branch" value="'.$_POST['branch'].'">
		<input type="hidden" name="type" value="'.$_POST['type'].'">
		<input type="hidden" name="fdate" value="'.$_POST['fdate'].'">
		<input type="hidden" name="tdate" value="'.$_POST['tdate'].'">
		<input type="hidden" name="lower" value="'.$_POST['lower'].'">
		<input type="hidden" name="upper" value="'.$_POST['upper'].'">
		<input type="submit" name="print" class="push" value="FORMAT 1"></form></td>';
echo '<td style="text-align:center"><form action="report_branch_dyn_p2.php" method="post" target="_blank">
		<input type="hidden" name="branch" value="'.$_POST['branch'].'">
		<input type="hidden" name="type" value="'.$_POST['type'].'">
		<input type="hidden" name="fdate" value="'.$_POST['fdate'].'">
		<input type="hidden" name="tdate" value="'.$_POST['tdate'].'">
		<input type="hidden" name="lower" value="'.$_POST['lower'].'">
		<input type="hidden" name="upper" value="'.$_POST['upper'].'">
		<input type="submit" name="print" class="push" value="FORMAT 2"></form></td>';
echo '<td style="text-align:center"><form action="report_branch_dyn_p3.php" method="post" target="_blank">
		<input type="hidden" name="branch" value="'.$_POST['branch'].'">
		<input type="hidden" name="type" value="'.$_POST['type'].'">
		<input type="hidden" name="fdate" value="'.$_POST['fdate'].'">
		<input type="hidden" name="tdate" value="'.$_POST['tdate'].'">
		<input type="hidden" name="lower" value="'.$_POST['lower'].'">
		<input type="hidden" name="upper" value="'.$_POST['upper'].'">
		<input type="submit" name="print" class="push" value="FORMAT 1"></form></td>';
echo '<td style="text-align:center"><form action="report_branch_dyn_p4.php" method="post" target="_blank">
		<input type="hidden" name="branch" value="'.$_POST['branch'].'">
		<input type="hidden" name="type" value="'.$_POST['type'].'">
		<input type="hidden" name="fdate" value="'.$_POST['fdate'].'">
		<input type="hidden" name="tdate" value="'.$_POST['tdate'].'">
		<input type="hidden" name="lower" value="'.$_POST['lower'].'">
		<input type="hidden" name="upper" value="'.$_POST['upper'].'">
		<input type="submit" name="print" class="push" value="FORMAT 2"></form></td</tr></table>';
}
?>
