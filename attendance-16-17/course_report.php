<?php
include 'magic.php';
if($username=='111'){
	include('menua.php');
}
else{
	include('menuh.php');
}
include('connect.php');
echo "
<style>

@media print {
	
	body 
	{
		font-family:arial;
	}
	
	form 
	{
		display:none;
	}
	#cssmenu 
	{
		display:none;
	}
	#header1 
	{
		display:none;
	}
	#space 
	{
		display:none;
	}
	input
	{
		display: none;  
	}
    
	table{
		color:#000000;
		border:1px solid black;
		border-collapse:collapse;
		width:100%;
	}
	table tr td{
		border:1px solid black;
		padding:1px;
		font-size:10px;
	}
	table tr:first-child td{ 
		font-weight:900; 
	}

}	

</style>";
?>
<form method="POST" action="course_report.php">
	<div style="font-weight:bold;font-size:20px">COURSE REPORT</div>
	<table border="0">
		<tr>
			<td colspan="3" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Select all fields*</td>
		</tr>
		<tr>
			<td style="font-weight:bold">
			Semester</td>
			<td>
			<select name="semester" required>
				<?php
				$query="SELECT distinct(semester) FROM assignrole order by semester ASC";
				mysql_select_db('portal');
				$getit = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
				$sem=$row['semester'];
				echo "<option value='$sem'>$sem</option>";
				}
				?>
			</select>
			</td>
		</tr>
		<tr>
			<td style="font-weight:bold">
			Section</td>
			<td >
			<select name="section" required>
				<?php
				if($username=='111'){
					$query="SELECT distinct(section) FROM assignrole order by section ASC";
				}
				else{
					$query="SELECT distinct(section) FROM assignrole WHERE branch='$a' order by section ASC" ;
				}
				
				mysql_select_db('portal');
				$getit = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
				$sec=$row['section'];
				echo "<option value='$sec'>$sec</option>";
				}
				?>
			</select>
			</td>
		</tr>
		<tr>
			<td style="font-weight:bold">COURSE COVERAGE UPTO</td>
			<td>
				<select name="exam" required>
				<?php
				$query="SELECT * FROM course_setting";
				mysql_select_db('portal');
				$getit = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
				$exam=$row['exam'];
				echo "<option value='$exam'>$exam</option>";
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center"><input type="submit" name="submit" value="submit"></td>
		</tr>
	</table>
</form>
	<?php 
		if(isset($_POST['submit'])){
		$semester=$_POST['semester'];
		$section=$_POST['section'];
		$exam=$_POST['exam'];
		echo "<div id='report' width='80%' align='center'>
				<h3>AJAY KUMAR GARG ENGINEERING COLLEGE";
		$query="SELECT * FROM course_setting";
		mysql_select_db('portal');
		$getit = mysql_query( $query, $conn );
		while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
		$date=$row['upto_date'];
		}	
		
		$query="SELECT distinct(branch) FROM assignrole WHERE section='$section'";
		mysql_select_db('portal');
		$getit = mysql_query( $query, $conn );
		while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
		$dept=$row['branch'];
		}
		if($dept=='CSE'){
			$fullname='COMPUTER SCIENCE';
		}
		else if($dept=='IT'){
			$fullname='INFORMATION TECHNOLOGY';
		}
		else if($dept=='ECE'){
			$fullname='ELECTONICS AND COMMUNICATION';
		}
		else if($dept=='EN'){
			$fullname='ELECTICAL AND ELECTRONICS';
		}
		else if($dept=='EI'){
			$fullname='ELECTRONICS AND INSTRUMENTATION';
		}
		else if($dept=='ME'){
			$fullname='MECHANICAL ENGINEERING';
		}
		else if($dept=='CIVIL'){
			$fullname='CIVIL ENGINEERING';
		}
		echo "<br>DEPARTMENT OF $fullname</h3>";
		echo "<h4><u>STATUS OF COURSE COVERAGE UPTO $date<br>ODD SEMESTER 2016-17</u></h4>";
		echo "<p align='left'> Class & Section: B.Tech $semester sem (sec $section)</p>";
		echo "<table align='center' width='100%' class='table_style'>
				<tr>
					<td width='10%'>Subject Code</td>
					<td width='15%'>Subject Name</td>
					<td width='15%'>Faculty Name</td>
					<td>Theory/Lab (T/L)</td>
					<td>No of classes/lab sessions held</td>
					<td>No of Units/ Practicals as per Syllabus</td>
					<td>No of Units/ Practicals completed</td>
					<td>Remarks</td>
				</tr>";
		$query="SELECT * FROM course_coverage WHERE section='$section' AND semester='$semester' AND upto='$exam'";
		mysql_select_db('portal');
		$getit = mysql_query( $query, $conn );
		while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
			$sid=$row['sub_id'];
			$total=$row['total_unit'];
			$done=$row['complete_unit'];
			
			$data="SELECT * FROM assignrole WHERE section='$section' AND semester='$semester' AND sub_id='$sid'";
			mysql_select_db('portal');
			$get_data = mysql_query( $data, $conn );
			while($row = mysql_fetch_array($get_data, MYSQL_ASSOC)){
				$fac=$row['user_name'];
				$sub=$row['sub_name'];
			}
			$data="SELECT * FROM subject WHERE sub_id='$sid'";
			mysql_select_db('portal');
			$get_data = mysql_query( $data, $conn );
			while($row = mysql_fetch_array($get_data, MYSQL_ASSOC)){
				$category=$row['category'];
				if($category=='P'){
					$category='L';
				}
			}
			if($category=='T'){
				$data="SELECT distinct(fromdate) FROM attendance WHERE sub_id='$sid' AND section='$section'";
				mysql_select_db('portal');
				$get_data = mysql_query( $data, $conn );
				$sum=0;
				while($row = mysql_fetch_array($get_data, MYSQL_ASSOC)){
					$from=$row['fromdate'];
					$d="SELECT distinct(totalclasses) FROM attendance WHERE sub_id='$sid' AND section='$section' AND fromdate='$from'";
					mysql_select_db('portal');
					$get_d = mysql_query( $d, $conn );
					while($row = mysql_fetch_array($get_d, MYSQL_ASSOC)){
						$s=$row['totalclasses'];
					}
					$sum=$sum+$s;
				}
				$held=$sum;
			}
			else{
				$data="SELECT distinct(fromdate) FROM attendance WHERE sub_id='$sid' AND section='$section'";
				mysql_select_db('portal');
				$get_data = mysql_query( $data, $conn );
				$sum1=0;
				$sum2=0;
				while($row = mysql_fetch_array($get_data, MYSQL_ASSOC)){
					$from=$row['fromdate'];
					$d="SELECT distinct(totalclasses) FROM attendance WHERE sub_id='$sid' AND section='$section' AND fromdate='$from'";
					mysql_select_db('portal');
					$get_d = mysql_query( $d, $conn );
					$i=0;
					$s['1']=0;
					$s['2']=0;
					while($row = mysql_fetch_array($get_d, MYSQL_ASSOC)){
						$i++;
						$s[$i]=$row['totalclasses'];
					}
					if($s['2']!=0){
						$sum1=$sum1+$s['1'];
						$sum2=$sum2+$s['2'];
					}	
					else{
						$sum1=$sum1+$s['1'];
						$sum2=$sum2+$s['1'];
					}
				}
				$held=$sum1."/".$sum2;
			}
			
			echo "<tr>
					<td width='10%'>$sid</td>
					<td width='15%'>$sub</td>
					<td width='15%'>$fac</td>
					<td>$category</td>
					<td>$held</td>
					<td>$total</td>
					<td>$done</td>
					<td></td>
				</tr>";
		}
		$get_hodname="SELECT * FROM login WHERE userid='$username' AND category='HOD'";
		mysql_select_db('portal');
		$getit= mysql_query( $get_hodname, $conn );
		while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
		$hodname=$row['name'];
		$branch=$row['branch'];
		$cat=$row['category'];
		}
		echo "</table>
				<br><br><div align='left'>
					<h5>$hodname<br>$cat $branch</h5>
				</div>
			<div align='center'>
				<input type='button' value='PRINT' onclick='window.print()'>
				<form action='course_excel.php' method='post'>
					<input type='hidden' value='$semester' name='semester'>
					<input type='hidden' value='$section' name='section'>
					<input type='hidden' value='$exam' name='exam'>
					<input type='submit' value='SAVE'>
				</form>
			</div>
		</div><br><br>";
		}
		
		
			
	?>
		
