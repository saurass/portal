<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="../assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
</head>
<?php
	session_start();
	if($_SESSION['category']!='DIRECTOR')
	{
		header("location:index.php");
	}
?>

<div class="preloading-screen">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
    <h4 class="ml-5 green-text text-darken-2"> GreenBoard! <i class="fa fa-pencil"></i></h4>
    <br><br>
    <h6 class="powered-by">Powered by Team OSS</h6>
</div>

<header class="navbar-fixed">
    <nav class="green darken-2">
        <div class="nav-wrapper">
            <a href="index.html" class="brand-logo center">GreenBoard <i class="fa fa-pencil right"></i></a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <img align="center" class="icon mx-5" src="../assets/images/profile-image-1.png" align="center"> 
                </li>
                <li> <a href="logout.php"> <i class="fa fa-sign-out"></i> Logout &nbsp; </a> </li>
            </ul>
            <ul class="left hide-on-med-and-down">
                <li><a class="text-uppercase bold-text" href="#">Name of the Portal</a></li>
            </ul>
        </div>
    </nav>
</header>

<aside class="side-navigation green">
	<ul>
        <a class="waves-effect waves-ripple" href="#"> <i class="fa fa-eye"></i> View <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        
        <!-- Replace             -->
        <li><a href='viewfacultydirector.php'><span>FACULTY</span></a></li>
		<li><a href='viewsubjectdirector.php'><span>ASSIGNROLE</span></a></li>
		<li><a href='viewattendancedirector.php'><span>ATTENDANCE</span></a></li>
		<li><a href='newsubaddirector.php'><span>SUBJECT</span></a></li>
		<!-- /Replace -->


        <li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
    </ul>
	
	<ul>
		<a class="waves-effect waves-ripple" href="#"> <i class="fa fa-download"></i> Download <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        
        <!-- Replace             -->
        	<li><a href='viewstudentlist.php'><span>STUDENT LIST</span></a></li>
			<li><a href='dispatch_new.php'><span>DISPATCH LIST</span></a></li>
			<li><a href='disp.php'><span>DISPATCH NEW LIST</span></a></li>
		<!-- /Replace -->


        <li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
	</ul>

	<ul>
		<a class="waves-effect waves-ripple" href="#"> <i class="fa fa-file"></i> Report <i
                    class="fa fa-arrow-left right oh-here-is-a-back-button"></i></a>
        
        <!-- 
        	<li><a href='report_admin.php'><span>COMPLETE REPORT</span></a></li>
			<li><a href='report_branch_dyn.php'><span>FILTERED DEPT WISE REPORT</span></a></li>
			<li><a href='report_branch.php'><span>BRANCH WISE REPORT</span></a></li>
		  -->


        <li><a href='#' class='green darken-2 backwards'><i class='fa fa-arrow-left'></i>Go Back</a></li>
	</ul>

</aside>