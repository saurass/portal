<?php
include 'magic.php';
include('connect.php');
if($b=='ADMIN')
	include('menua.php');
elseif ($b=='PROCTOR') {
include 'menup.php';
}
elseif ($b=='DIRECTOR') 
{
	include 'menuk.php';
}

echo "
<style>

@media print {
	
	body 
	{
		font-family:arial;
	}
	
	form 
	{
		display:none;
	}
	#cssmenu 
	{
		display:none;
	}
	#header1 
	{
		display:none;
	}
	#space 
	{
		display:none;
	}
	input
	{
		display: none;  
	}
    
	table{
		color:#000000;
		border:1px solid black;
		border-collapse:collapse;
		width:100%;
	}
	table tr td{
		border:1px solid black;
		padding:3px;
		font-size:16px;
	}
	table tr:first-child td{ 
		font-weight:900; 
	}

}	

</style>";

$tc0=$tc1=$tc2=$tc3=$tc4=0;

echo "<div id='report' width='80%' align='center'>
				<div align='center' style='font-weight:bold;font-size:20px'>
					<br>AJAY KUMAR GARG ENGINEERING COLLEGE,GHAZIABAD<br>
				</div>
				<div align='center' style='font-weight:600;font-size:16px'>
					<br>COLLEGE WISE ATTENDANCE REPORT
				</div>";
	echo "<table align='center' width='80%' class='table_style'>
				<tr>
					<td><b>Branch</b></td>
					<td><b>semester</b></td>
					<td><b>less than 50</b></td>
					<td><b>50 to 60</b></td>
					<td><b>60 to 70</b></td>
					<td><b>70 to 75</b></td>
					<td><b>greater than 75</b></td>
					<td><b>TOTAL</b></td>
				</tr>";
				
		$query="SELECT DISTINCT branch FROM student ORDER BY branch ASC";
		mysql_select_db('portal');
		$getit = mysql_query( $query, $conn );
		while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
			$branch=$row['branch'];
			if($branch!=NULL){

				$q_sem="SELECT DISTINCT semester FROM student WHERE branch='$branch' ORDER BY semester ASC";
				mysql_select_db('portal');
				$getsem = mysql_query( $q_sem, $conn );
				$sem_count=0;
				while($row2 = mysql_fetch_array($getsem, MYSQL_ASSOC)){
					$sem_count++;
				}
				$sem_count++;
				echo "<tr><td rowspan='$sem_count'><b>$branch</b></td>";
				
				$q_sem="SELECT DISTINCT semester FROM student WHERE branch='$branch' ORDER BY semester ASC";
				mysql_select_db('portal');
				$getsem = mysql_query( $q_sem, $conn );
				while($row2 = mysql_fetch_array($getsem, MYSQL_ASSOC)){
					$sem=$row2['semester'];
					echo "<td>$sem</td>";
					
					$count0=0;
					$count1=0;
					$count2=0;
					$count3=0;
					$count4=0;
					$get_student="SELECT * FROM student WHERE semester='$sem' AND branch='$branch'";
					mysql_select_db('portal');
					$getstudent = mysql_query( $get_student, $conn );
					while($row3 = mysql_fetch_array($getstudent, MYSQL_ASSOC)){
						$studentid=$row3['st_id'];
						$get_att="SELECT * FROM attendance WHERE st_id='$studentid'";
						mysql_select_db('portal');
						$getatt = mysql_query( $get_att, $conn );
						$total[$studentid]=0;
						$attend[$studentid]=0;
						while($row4 = mysql_fetch_array($getatt, MYSQL_ASSOC)){
							$total[$studentid]+=$row4['totalclasses'];
							$attend[$studentid]+=$row4['attended'];
						}
						$percentage[$studentid]=round($attend[$studentid]/$total[$studentid]*100,2);
					if($percentage[$studentid]<49.45){
						$count0++;
					}
					else if($percentage[$studentid]>=49.45 && $percentage[$studentid]<59.45){
						$count1++;
					}
					else if($percentage[$studentid]>=59.45 && $percentage[$studentid]<69.45){
						$count2++;
					}
					else if($percentage[$studentid]>=69.45 && $percentage[$studentid]<74.45){
						$count3++;
					}
					else if($percentage[$studentid]>=74.45){
						$count4++;
					}
					
					}

					$c[$sem]=$count0+$count1+$count2+$count3+$count4;
					$c0[$branch]+=$count0;
					$c1[$branch]+=$count1;
					$c2[$branch]+=$count2;
					$c3[$branch]+=$count3;
					$c4[$branch]+=$count4;
					echo "<td><a href='student_list.php?sem=$sem&branch=$branch&start=0&end=49.45' target='_blank'>$count0</a></td>
						<td><a href='student_list.php?sem=$sem&branch=$branch&start=49.45&end=59.45' target='_blank'>$count1</a></td>
						<td><a href='student_list.php?sem=$sem&branch=$branch&start=59.45&end=69.45' target='_blank'>$count2</a></td>
						<td><a href='student_list.php?sem=$sem&branch=$branch&start=69.45&end=74.45' target='_blank'>$count3</a></td>
						<td><a href='student_list.php?sem=$sem&branch=$branch&start=74.45&end=100' target='_blank'>$count4</a></td>
						<td><b>$c[$sem]</b></td>
					</tr>";
					
				}
				echo "<tr>
					<td><b>Total</b></td>
					<td><b>$c0[$branch]</b></td>
					<td><b>$c1[$branch]</b></td>
					<td><b>$c2[$branch]</b></td>
					<td><b>$c3[$branch]</b></td>
					<td><b>$c4[$branch]</b></td>
					<td><b>".($c0[$branch]+$c1[$branch]+$c2[$branch]+$c3[$branch]+$c4[$branch])."</b></td>
				</tr>";
				$tc0+=$c0[$branch];
				$tc1+=$c1[$branch];
				$tc2+=$c2[$branch];
				$tc3+=$c3[$branch];
				$tc4+=$c4[$branch];
				
			}
		}
echo "<tr>
		<td colspan=2><b>Total</b></td>
		<td><b>$tc0</b></td>
		<td><b>$tc1</b></td>
		<td><b>$tc2</b></td>
		<td><b>$tc3</b></td>
		<td><b>$tc4</b></td>
		<td><b>".($tc0+$tc1+$tc2+$tc3+$tc4)."</b></td>
	</tr>";			
		
echo "</div>";
echo "<table style='border:none'>
			<tr><td style='text-align:left;border:none'>NOTE:</td></tr>
			<tr><td style='text-align:left;border:none'>1. The 'less than 50' for those who lies in range of 0 to 49.45</td></tr>
			<tr><td style='text-align:left;border:none'>2. The '50 to 60' for those who lies in range of 49.45-59.45</td></tr>
			<tr><td style='text-align:left;border:none'>3. The '60 to 70' for those who lies in range of 59.45-69.45</td></tr>
			<tr><td style='text-align:left;border:none'>4. The '70 to 75' for those who lies in range of 69.45-74.45</td></tr>
			<tr><td style='text-align:left;border:none'>5. The '75+' for those who lies in range of  74.45-100</td></tr>
		</table>";
	
		


?>
