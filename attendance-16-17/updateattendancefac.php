<?php
include 'magic.php';
include('menuf.php');
?>
<html>

<head>

<link rel="stylesheet" type="text/css" href="css/finallook.css" />
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>UPDATE ATTENDANCE</title>
<script lang="javascript" src="js/updateattendance.js"></script>
</head>
<script lang="javascript">


function showsubject()
{
		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","searchsubject.php",true)
		xmlhttp.send();

		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
				//alert("nnn");
				document.getElementById("subjectid").innerHTML=xmlhttp.responseText;
			}
		}
}

function showsection(str)
{
		

		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","showsubjectsec.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
	//	alert("hello");			
				document.getElementById("section").innerHTML=xmlhttp.responseText;
			}
		}
}
function showsem(str)
{
	

		var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","searchsubjectsem.php?q="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("semester").value=xmlhttp.responseText;
			}
		}
}


function getsubjectname(str)
{

	var xmlhttp;
			
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","showsubjectname.php?sid="+str,true)
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
			
				document.getElementById("subjectname").value=xmlhttp.responseText;
			}
		}	
}


function getdata()
{

var sem=document.getElementById("semester").value;
var sec=document.getElementById("section").value;
var subid=document.getElementById("subjectid").value;

if(sem==''){
	alert('SEMESTER field is missing');
	return false;
}
else if(sec==''){
	alert('SECTION field is missing');
	return false;
}
else if(subid==''){
	alert('SUBJECTID field is missing');
	return false;
}
	
		var xmlhttp;
		
		if(window.XMLHttpRequest)
		{
			//code for IE7,firefox,chrome,opera,safari	
			
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		xmlhttp.open("GET","updatedates.php?subid="+subid+" & sem="+sem+" & sec="+sec,true);
		xmlhttp.send();
			
		xmlhttp.onreadystatechange=function()
		{
			if(xmlhttp.readyState==4&&xmlhttp.status==200)
			{
				document.getElementById("txt").innerHTML=xmlhttp.responseText;
			}
		}
	


}
function msg(){
	alert("This Date Interval is not allowed to update\n -Please request respective HOD to enable this Interval");
}

</script>
<body onload=showsubject()>



<form  method="POST" action="">
	<h1><b>UPDATE ATTENDANCE</b></h3>
	<table border="0">
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold;background-color: #83c8f9;font-size:20px">Please Select all fields*</td>
		</tr>
	
		<tr>
			<td>Subject ID</td>
			<td><select size="1" name="subjectid" id=subjectid onchange=getsubjectname(this.value);showsection(this.value);showsem(this.value)></select></td>
		</tr>
		<tr>
			<td>Semester</td>
			<td><input type="text" name="semester" id=semester></td>
			
		</tr>
		<tr>
			<td>Subject Name</td>
			<td><input type="text" name="subjectname" id=subjectname size="40"></td>
		</tr>
		<tr>
			<td>Section</td>
			<td><select size="1" name="section" id="section"></select></td>
		</tr>
		<tr>
		<td colspan="2" style="text-align:center"><input type="button"  value="Submit" name="B1" id="submit" onclick=getdata() ></td>
		</tr>
		
	</table>
		
	
</form>
<div id=txt></div>
<input type=hidden id="studentcount">
<input type=hidden id="validornot">
</body>

</html>