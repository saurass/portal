<?php
include 'magic.php';
include 'connect.php';
$uid=$_POST['uid'];
$sname=$_POST['sname'];
$sdate=$_POST['sdate'];
$edate=$_POST['edate'];
$sem=$_POST['sem'];
$percentage=$_POST['per'];
$sec=$_POST['sec'];
$nsdate= date("d-m-Y", strtotime($sdate));
$nedate = date("d-m-Y", strtotime($edate));
$npercentage = round ($percentage, 2);

if($_POST['examtype']=='ST1')
{
    $exames='ST-1';
    $exame='Sessional Test-1';
    $examh='सेशनल टेस्ट-1';
}
elseif($_POST['examtype']=='ST2')
{
    $exames='ST-2';
    $exame='Sessional Test-2';
    $examh='सेशनल टेस्ट-2';
}
elseif($_POST['examtype']=='PUT')
{
    $exames='PUT';
    $exame='Pre University Test';
    $examh='पूर्व  विष्वविद्यालय परीक्षा';
}

require_once('../config.php');
require_login();


$db="SELECT * FROM student_info WHERE username='$uid'";
           


mysql_select_db('portal');
$query= mysql_query( $db, $conn );

if(!$query)
    echo mysql_error();
while($row = mysql_fetch_array($query, MYSQL_ASSOC)){
        



$username = $arr['username'];
$user_student_number = $arr['studentnumber'];
$gender=$arr['gender'];
$branch=$arr['branch'];
$user_permanentaddress1 = $arr['houseno'];
$user_permanentaddress2 = $arr['street'];
$user_permanentaddress3 = $arr['townvillage'];
$user_permanentaddress4 = $arr['district'];
$user_permanentaddress5 = $arr['state'];
$user_permanentaddress6 = $arr['pincode'];

$get_hodname="SELECT * FROM login WHERE branch='$branch' AND category='HOD'";
mysql_select_db('portal');
$getit= mysql_query( $get_hodname, $conn );
while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
$hodname=$row['name'];
$branch=$row['branch'];
$category=$row['category'];
}
echo "
<html>
<head>
<meta charset='UTF-8'>
<style>

@media print {
	
	form 
	{
		display:none;
	}
	img  
	{
		display:none;
	}
	a 
	{
		display:none;
	}
	input
	{
		display: none;  /* hides all input elements for the printer */ 
	}
    .record 
	{ 
		display: none;  /* hide all records by default */ 
	}
    input[checked] + .record, input:checked + div.record 
	{
		/* display all that are immediately preceeded by a input with checked attribute */
        display: block; 
	}
	body
	{
		margin-left:60px;
		margin-right:40px;
		font-size:12pt;
		line-height: 1.3;
	}
	

}	

</style>
<script>
function printpage()
  {
  window.print()
  }
</script>
</head>
<body>

<input type='button' value='Print this page' onclick='printpage()'>

</body>
</html>
";
echo "
<html>
<body>
<div style='margin-top:10px;height:25cm;'>";
if($sec=='MBA-1' || $sec=='MBA-2')
echo "<h1 style='text-align:center;font-size:22px;'>AJAY KUMAR GARG INSTITUTE OF MANAGEMENT</h1>";
else
echo "<h1 style='text-align:center;font-size:22px;'>AJAY KUMAR GARG ENGINEERING COLLEGE</h1>";

echo "<p style='text-align:center;font-size:12px;'>27th Km. Stone, Delhi-Hapur By Pass Road, Adhyatmik Nagar, Ghaziabad-201009</p>
<h3 style='text-align:center;font-size:12px;'><b>Ph.: 0120-2762841 to 2762851, Fax : 0120-2761844, 2761845, 2762170 Ext.: 2017, 2017</b></h3>
<h3 style='font-size:16px;'> ";
if($sec=='MBA-1' || $sec=='MBA-2')
echo "<table width='100%'><tr><td align=left>AKGIM/PoorAtt. </td><td align=right>/&nbsp;&nbsp;&nbsp;&nbsp;/2017<td></tr></table>
</h3>
<br>";
else
echo "<table width='100%'><tr><td align=left>AKGEC/HoD/PoorAtt. </td><td align=right>/&nbsp;&nbsp;&nbsp;&nbsp;/2017<td></tr></table>
</h3>
<br>";
echo "<div style='text-align:left;font-size:13px;'>";
echo "<b>";
echo "MR. ".$user_fathersname.""; echo"<br>";
echo "F/O ".$sname.""; echo"<br>";
//echo "F/O ".$user->firstname." ".$user->lastname.""; echo"<br>";
//echo "STUDENT NO : ".$user_student_number.""; echo"<br>";
echo "STUDENT ROLL NO: ".$uid.""; echo"<br>";
if(!empty($user_permanentaddress1)) {echo $user_permanentaddress1; echo"<br>";}
if(!empty($user_permanentaddress2)) {echo $user_permanentaddress2; echo"<br>";}
if(!empty($user_permanentaddress3)) {echo $user_permanentaddress3; echo"<br>";}
if(!empty($user_permanentaddress4)) {echo $user_permanentaddress4; echo"<br>";}
if(!empty($user_permanentaddress5)) {echo $user_permanentaddress5; echo" - ";}
if(!empty($user_permanentaddress6)) {echo $user_permanentaddress6; echo"<br>";} 
echo "</b>";
echo "</div>";
echo"
<h3 style='text-align:center;font-size:17px;'><u>POOR ATTENDANCE<br>PROVISIONALLY CLEARED TO APPEAR IN $exames</br></u><h3>

<h3 style='text-align:left;font-size:15px;'>Dear Parent,</h3> 

<p style='text-align:justify;font-size:16px;'>Your ";
if($gender=='MALE')
echo "son ";
else if($gender=='FEMALE')
echo "daughter ";
if($gender=='MALE')
echo "<b>Mr. </b>";
else if($gender=='FEMALE')
echo "<b>Ms. </b>";
echo "<b>$sname</b> (Roll No. $uid) has been absenting from classes resulting into low attendance up to $exame. This not only constitutes indiscipline but will also have serious consequences on";
if($gender=='MALE')
echo " his ";
else if($gender=='FEMALE')
echo " her "; 
echo"academic performance.

<br><br>You may be aware that as per the norms of the University, your ";
if($gender=='MALE')
echo "son ";
else if($gender=='FEMALE')
echo "daughter ";
echo "will not be permitted to appear in the ".(($_POST['examtype']!='PUT')?'sessional and ':'')."end semester exams in case the attendance during the period/semester falls short of the <b> required 75%</b>. In addition, the poor attendance will adversely affect the academic performance and finally the placements and career of your ward.
<br><br>Please be informed that your ward has failed to meet the prescribed attendance norms. However ";
if($gender=='MALE')
echo " he ";
else if($gender=='FEMALE')
echo " she ";

echo"
is being provisionally permitted to appear in the ";
if($_POST['examtype']=='ST1')
{
    $exames='ST-1';
    $exame='Sessional Test-1';
    $examh='सेशनल टेस्ट-1';
}
elseif($_POST['examtype']=='ST2')
{
    $exames='ST-2';
    $exame='Sessional Test-2';
    $examh='सेशनल टेस्ट-2';
};
echo$exame;
if($gender=='MALE')
echo ". His ";
else if($gender=='FEMALE')
echo ". Her ";

echo"
answer script will be evaluated and the marks considered if and only when he improves his attendance and meets the norms.

<br><br>In view of the above, you are advised to take serious note of the situation and counsel your ";
if($gender=='MALE')
echo "son ";
else if($gender=='FEMALE')
echo "daughter ";
echo "to attend classes regularly and concentrate on studies. In case your ";
if($gender=='MALE')
echo "son ";
else if($gender=='FEMALE')
echo "daughter ";
echo "ignores this warning and there is no improvement in ";
if($gender=='MALE')
echo "his ";
else if($gender=='FEMALE')
echo "her ";
echo "attendance, you will yourself be responsible for the adverse consequences of any strict action that the college may be compelled to take. 

<br><br>You are welcome to visit the college and meet the undersigned regarding the performance and problem of your ";
if($gender=='MALE')
echo "son. ";
else if($gender=='FEMALE')
echo "daughter. ";
echo "</p> 
<h3 style='text-align:left;font-size:15px;'>With regards,<br/>
Yours Sincerely, </h3>
";
if($sec=='MBA-1' || $sec=='MBA-2')
    echo "<h4>DR. T.R. Pandey<br>DIRECTOR, AKGIM </h4>";
else
      {
    if($sem==1 || $sem==2)
        echo "<h4>$hodname<br>Dean 1st Year </h4>";
    
    else
        echo "<h4>$hodname<br>$category $branch </h4>";
}

echo "<p style='text-align:justify;font-size:16px;'>Copy to: ";
if($gender=='MALE')
echo "Mr. ";
else if($gender=='FEMALE')
echo "Ms. ";
echo "$sname (Roll No. $uid)</p>
</div>";





echo "
<div style='height:25cm;'>";
if($sec=='MBA-1' || $sec=='MBA-2')
echo "<h1 style='text-align:center;font-size:22px;'>AJAY KUMAR GARG INSTITUTE OF MANAGEMENT</h1>";

else
echo "<h1 style='text-align:center;font-size:22px;'>AJAY KUMAR GARG ENGINEERING COLLEGE</h1>";

echo "<p style='text-align:center;font-size:12px;'>27th Km. Stone, Delhi-Hapur By Pass Road, Adhyatmik Nagar, Ghaziabad-201009</p>
<h3 style='text-align:center;font-size:12px;'><b>Ph.: 0120-2762841 to 2762851, Fax : 0120-2761844, 2761845, 2762170 Ext.: 2017, 2017</b></h3>
<h3 style='font-size:16px;'> ";
if($sec=='MBA-1' || $sec=='MBA-2')
echo "<table width='100%'><tr><td align=left>AKGIM/PoorAtt. </td><td align=right>/&nbsp;&nbsp;&nbsp;&nbsp;/2017<td></tr></table>
</h3>
<br>";
else
echo "<table width='100%'><tr><td align=left>AKGEC/HoD/PoorAtt. </td><td align=right>/&nbsp;&nbsp;&nbsp;&nbsp;/2017<td></tr></table>
</h3>
<br>";
echo "<div style='text-align:left;font-size:13px;'>";
echo "<b>";
echo "MR.".$user_fathersname.""; echo"<br>";
echo "F/O ".$sname.""; echo"<br>";
//echo "F/O ".$user->firstname." ".$user->lastname.""; echo"<br>";
//echo "STUDENT NO : ".$user_student_number.""; echo"<br>";
echo "STUDENT ROLL NO :".$uid.""; echo"<br>";
if(!empty($user_permanentaddress1)) {echo $user_permanentaddress1; echo"<br>";}
if(!empty($user_permanentaddress2)) {echo $user_permanentaddress2; echo"<br>";}
if(!empty($user_permanentaddress3)) {echo $user_permanentaddress3; echo"<br>";}
if(!empty($user_permanentaddress4)) {echo $user_permanentaddress4; echo"<br>";}
if(!empty($user_permanentaddress5)) {echo $user_permanentaddress5; echo" - ";}
if(!empty($user_permanentaddress6)) {echo $user_permanentaddress6; echo"<br>";} 
echo "</b>";
echo "</div>";

echo "

<h3 style='text-align:center;font-size:18px;'><u>अपर्याप्त उपस्थिति<br/>अस्थायी रूप से $examh में प्रवेश होने की अनुमति</u></h3>

<h3 style='text-align:left;font-size:16px;'>प्रिय अभिभावक, </h3>

<p style='text-align:justify;font-size:15px;'>आपको सूचित किया जाता है कि काॅलेज के वर्तमान शैक्षिक सत्र में <b>$examh</b> तक ";
if($gender=='MALE')
echo "आपके पुत्र ";
else if($gender=='FEMALE')
echo "आपकी पुत्री ";
echo "<b>$sname</b> (रोल नं0 $uid ) की कक्षा में उपस्थिति कम पायी गयी है। यह केवल अनुशासनहीनता का विषय ही नहीं अपितु यह";
if($gender=='MALE')
    echo " छात्र ";
else if($gender=='FEMALE')
    echo " छात्रा ";
echo"की शैक्षिक प्रगति में बाधक भी हो सकता है। 
<br><br>
आपको यह विदित होगा कि यदि उक्त";
if($gender=='MALE')
    echo " छात्र ";
else if($gender=='FEMALE')
    echo " छात्रा ";
echo "की उपस्थिति विश्वविद्यालय के द्वारा <b>निर्धारित 75%</b> से कम होगी तो उसे ".(($_POST['examtype']!='PUT')?'शैक्षिक और ':'')."सेमेस्टर परीक्षा में भाग लेने से वंचित कर दिया जायेगा। साथ ही, कक्षा से अनुपस्थिति न केवल उसके शैक्षिक प्रदर्शन को प्रभावित करेगी वरन रोज़गार अवसरों और अन्तत: उसके कैरियर पर भी प्रतिकूल प्रभाव डालेगी। 
<br><br>

आपको यह सूचित किया जाता है कि ";
if($gender=='MALE')
echo "आपके पुत्र ";
else if($gender=='FEMALE')
echo "आपकी पुत्री ";
echo" की उपस्थिति निर्धारित मापदंड से कम है। फिर भी, अस्थायी रूप से उसे";
echo $examh;
echo" में प्रवेश होने की इज़ाज़त दी जा रही है। उसकी उत्तर पुस्तिका की जांच की जाएगी परन्तु उसमे पाये हुए अंक तब ही मान्य होंगे जब ";
if($gender=='MALE')
echo "आपके पुत्र ";
else if($gender=='FEMALE')
echo "आपकी पुत्री ";
echo" की उपस्थिति निर्धारित मापदंड से अधिक हो जाती है।  
<br><br>
उपरोक्तानुसार, आपको यह परामर्श दिया जाता है कि इस प्रकरण को गम्भीरता से लेते हुए ";
if($gender=='MALE')
echo "अपने पुत्र ";
else if($gender=='FEMALE')
echo "अपनी पुत्री ";
echo "को निरन्तर कक्षा में उपस्थिति के लिए प्रेरित करें। यदि  ";
if($gender=='MALE')
echo " आपका पुत्र ";
else if($gender=='FEMALE')
echo " आपकी पुत्री ";
echo "इन चेतावनियों को नजरअन्दाज़ ";
if($gender=='MALE')
echo "करता ";
else if($gender=='FEMALE')
echo " करती ";
echo "है एवं कक्षा में उपस्थिति, शैक्षिक प्रदर्शन तथा व्यवहार में कोर्इ सुधार नहीं ";
if($gender=='MALE')
echo "करता ";
else if($gender=='FEMALE')
echo "करती ";
echo "है तो कालेज द्वारा उसके विरूद्ध होने वाली किसी भी कठोर कार्यवाही का सम्पूर्ण उत्तरदायित्व आपका होगा। 
<br><br>
आप ";
if($gender=='MALE')
echo "अपने पुत्र ";
else if($gender=='FEMALE')
echo "अपनी पुत्री ";
echo "की शैक्षिक प्रगति एवं उसकी किसी भी अन्य प्रकार की समस्या हेतु कॉलेज आकर मुझसे मिलकर विचार-विमर्श करना चाहें तो आपका स्वागत है। </p>
<h3 style='text-align:left;font-size:16px;'>सधन्यवाद, </h3>
<h3 style='text-align:left;font-size:16px;'>भवदीय, </h3>
";
if($sec=='MBA-1' || $sec=='MBA-2')
    echo "<h4>DR. T.R. Pandey<br>DIRECTOR, AKGIM </h4>";
else
      {
    if($sem==1 || $sem==2)
        echo "<h4>$hodname<br>Dean 1st Year </h4>";
    
    else
        echo "<h4>$hodname<br>$category $branch </h4>";
}
echo "<p style='text-align:justify;font-size:16px;'><br>Copy to: ";
if($gender=='MALE')
echo "Mr. ";
else if($gender=='FEMALE')
echo "Ms. ";
echo "$sname ($uid)</p>
</div>";
echo "</body>
</html>";
}
?>

