/**
 * Created by Ritik on 07-08-2017.
 */
     
$(document).ready(function(){
    $("#password-form").submit(function(){
        if($("#password").val()==""){
            $("p#error-password").html("<i class='fa fa-exclamation-triangle'></i> You, sir, have to enter the password");
            event.preventDefault();
            return false;
        }
        else return true;
        
    });


    $(".login-preloader").hide();
    $("#username").keypress(function(e){
        if(e.keyCode==13){
            e.preventDefault();
            $(".next").click();
        }
    });
    $(".next").click(function(){
        if($(this).closest("form").find("input").val() == ""){
            $("p#error-username").html("<i class='fa fa-exclamation-triangle'></i> You, sir, should understand the concept of username.");
        }
        else{
            var $username=$("#username").val();
            var $logtype = $("#logintype").val();
           
            

            $("#username1").attr("value",$username);
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                $(".login-preloader").show();
                $("main").addClass("opac-07");
                if (this.readyState == 4 && this.status == 200) {
                        setTimeout(function(){
                            $(".login-preloader").fadeOut(300);
                            $("main").addClass("opac-10");
                        },1000);
                    if(this.responseText == 1){
                        $("p#error-username").html("You, sir, are not a student.");    
                    }   
                    else if(this.responseText == 0){
                        $("p#error-username").html("You, sir, does not exist.");    
                    } 
                    else if (this.responseText == 2) {
                        $("p#error-username").html("You, sir,are not a faculty");}  
                    else{
                        var a=this.responseText;

                        var b=a.split('*');
                        var name=b[1];
                        var img=b[0];
                        
                        $("#name-of-person").html(name.toUpperCase()) ;
                        $("main").addClass("opac-10");
                        $(".username-form").hide();
                        $(".password-form").fadeIn();
                        $(".password-form").find("input[type=text]").focus();
                        $("#unique_image").attr("src", img);
                    } 
             }
          };
            xhttp.open("GET", "ajax-directive-pages/fetch-username.php?username="+$username + "&logintype="+$logtype , true);
            xhttp.send();         
        }
    });
    $("input").keypress(function(){
        $("p.error").html("");
    });
    $(".back").click(function(){
        $(".password-form").hide();
        $(".username-form").fadeIn();
        $(".username-form").find("input").focus();
    });
});
