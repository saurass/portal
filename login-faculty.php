<?php

include("database.inc.php");
session_start();
if(isset($_GET["id"]))
{
    $id = $_GET["id"];
    if($id==1){
?>
    <script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript"> 
        $(document).ready(function(){
            Materialize.toast("Your password, sir, do not match. Login Again.", 5000, "rounded green darken-2");
            $("#password-form").hide();
        });
    </script>

<?php    }
}
else if(!isset($_GET["id"])){
    ?>
    <script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#password-form").hide();
        });
    </script>
    <?php
}
// if(isset($_SESSION['username']))
//     {   if(isset($_SESSION['category']))
//             header("Location:/portal/welcome-student.php");
//         else
//         header("Location:/portal/welcome-faculty.php");
//     }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Faculty Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/custom.css">

</head>
<body class="green ">
<div class="preloading-screen">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
    <h4 class="ml-5 green-text text-darken-2"> GreenBoard! <i class="fa fa-pencil"></i></h4>
    <br><br>
    <h6 class="powered-by">Powered by Team OSS</h6>
</div>

<div class="login-preloader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-yellow-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>

<main class="container">

    <form class="username-form">
        <h4 class="white-text">Welcome, Faculties</h4>
        <div class="row">
            <div class="input-field col s12 m6">
                <input type="text" class="invalid" name="username" id="username" autocomplete="off" required>

                <label for="username">Username</label>
            </div>

            <input type="hidden" name="logintype" value="FACLOG" id="logintype">
        </div>
        <div class="row">

        
        
            <div class="input-field col s2 m6 right-align">
                <a href="#" class="btn white black-text bold-text next"> Next <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </form>

    <form class="password-form" action="login-process-faculty.php" method="POST">
        <div class="row">
            <div class="col s12 m6 center">
                <img src="#" id="unique_image" class="responsive-img icon-big">
            </div>
        </div>
        <div class="row">
            <h5 class="col s12 m6 white-text center " style="margin-top: -15px;" id="name-of-person">
                <!-- Ms. Jane Doe  -->
            </h5>
        </div>
        <div class="row">
            <div class="input-field col s12 m6">
                 <input type="hidden" name="username" value="" id="username1">

                <input type="password" name="password" id="password" required>
                <label for="password">Password</label>
            </div></div>
            <div class="row">
                <div class="input-field col s12 m6 left-align">
                <a href="forgot-password-recover/index.php" class="white-text"> Forgot Password? </a>
            </div>
            </div>
            
        </div>
        <p class="error" id="error-password"></p>
        <div class="row">
            <div class="input-field col s12 m6 right-align">
               <button type="student-login-button" name="submit" class="btn white black-text bold-text log-in" id="student-login-button"> Login <i class='fa fa-sign-in'></i> </button>
                <a href="#" class="btn-flat white-text bold-text left back"> <i class="fa fa-arrow-left"></i> Back </a>
            </div>
        </div>
    </form>
</main>


<aside class="right-aside">
    <div class="open-seasame green darken-2 white-text">Login Links <i class="fa fa-arrow-up"></i></div>
    <ul class="collection with-header center">
        <li class="collection-header"><h5>Links</h5></li>
        <a href="login-faculty.php" class="collection-item active">Faculty Login</a>
        <a href="login-student.php" class="collection-item">Student Login</a>
        <a href="index.php" class="collection-item"><i class="fa fa-arrow-left"></i> Back to Home</a>
    </ul>
</aside>

<aside class="left-aside">
    <div class="open-alohomora green darken-2 white-text">Register Links <i class="fa fa-arrow-up"></i></div>
    <ul class="collection with-header center">
        <li class="collection-header"><h5>Links</h5></li>
        <a href="student_portal/php/facultyregistration.php" class="collection-item">Faculty Register</a>
        <a href="student_portal/php/registerstudent_here.php" class="collection-item">Student Register</a>
        <a href="index.php" class="collection-item"><i class="fa fa-arrow-left"></i> Back to Home</a>
    </ul>
</aside>

<a class="g" href="about.html">G <i class='fa fa-pencil'></i></a>
<a class="h" href="index.php"> <i class='fa fa-home'></i></a>

<div class="footnote text-capitalize green-text text-lighten-2">
    &copy; GreenBoard 2017 | <a href="about.html" class="green-text text-lighten-2">About</a>
</div>
<script type="text/javascript" src="assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/plugins/materialize/js/materialize.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $(".password-form").hide();
        });
    </script>
<script type="text/javascript" src="assets/js/login.js"></script>
</body>
</html>