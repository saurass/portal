<?php
include 'common.php';
echo "<div id='top_margin'></br></br></br></br></br></br></div>";
include 'connect.php';
# PHPlot example: Bar chart, embedded image with image map
require_once '../graph/phplot.php';

# This global string accumulates the image map AREA tags.
$image_map = "";

# Data for bar chart:
$data = array(
    array('0-10%', $_POST['1']),
    array('10-20%', $_POST['2']),
    array('20-30%', $_POST['3']),
    array('30-40%', $_POST['4']),
    array('40-50%', $_POST['5']),
    array('50-60%', $_POST['6']),
    array('60-70%', $_POST['7']),
	array('70-80%', $_POST['8']),
    array('80-90%', $_POST['9']),
    array('90-100%', $_POST['10']),
);
$max=$_POST['max']+5;
$section = $_POST['section'];
$semester = $_POST['semester'];
$examtype = $_POST['examtype'];
$order = $_POST['order'];

echo "<div align='center'>
	<b style='font-size:18px'>AJAY KUMAR GARG ENGINEERING COLLEGE, GHAZIABAD</b></br>
	<b style='font-size:18px'>EVEN SEMESTER: 2013-14</b></br>
	<b style='font-size:16px'>BRANCH: COMPUTER SCIENCE & ENGINEERING: $semester SEMESTER (SECTION- $section)</b></br>
	<b style='font-size:17px'>$examtype MARKS</b></br></br></br>";
		
echo "</div>";
# Callback for 'data_points': Generate 1 <area> line in the image map:
function store_map($im, $passthru, $shape, $row, $col, $x1, $y1, $x2, $y2)
{
    global $image_map;

    # Title, also tool-tip text:
    $title = "Group $row, Bar $col";
    # Required alt-text:
    $alt = "Region for group $row, bar $col";
    # Link URL, for demonstration only:
    $href = "javascript:alert('($row, $col)')";
    # Convert coordinates to integers:
    $coords = sprintf("%d,%d,%d,%d", $x1, $y1, $x2, $y2);
    # Append the record for this data point shape to the image map string:
    $image_map .= "  <area shape=\"rect\" coords=\"$coords\""
               .  " title=\"$title\" alt=\"$alt\" href=\"$href\">\n";
}

# Create and configure the PHPlot object.
$plot = new PHPlot(1000, 480);
# Disable error images, since this script produces HTML:
$plot->SetFailureImage(False);
# Disable automatic output of the image by DrawGraph():
$plot->SetPrintImage(False);
# Set up the rest of the plot:
$plot->SetTitle("Student marks distribution (Average)");
$plot->SetImageBorderType('plain');
$plot->SetDataValues($data);
$plot->SetDataType('text-data');
$plot->SetPlotType('bars');
$plot->SetXTickPos('none');
# Set the data_points callback which will generate the image map.
$plot->SetCallback('data_points', 'store_map');
$plot->SetPlotAreaWorld(NULL, 0, NULL, $max);
# Produce the graph; this also creates the image map via callback:
$plot->DrawGraph();

# Now output the HTML page, with image map and embedded image:
?>
<html>
<head>
	<link rel="stylesheet" href="../css/coloringpageprint.css" type="text/css" media="print" /> 
 </head> 
<script>
	function printpage()
  {
  window.print()
  }
 </script>
<body>
<map name="map1">
<?php echo $image_map; ?>
</map>
<div align="center"><img src="<?php echo $plot->EncodeImage();?>" alt="Plot Image" usemap="#map1"></div>
</body>
</html>
<?php
echo "</br></br><div align='center' ><input type='button' value='PRINT THIS GRAPH' onclick='printpage()' class='button' id='print'></div></br></br>";
?>