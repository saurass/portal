<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords"    content="" />
<link rel="stylesheet" type="text/css" href="../css/imc.css" />
<link rel="stylesheet" type="text/css" href="../css/imc_print.css" media="print"/>

</head>
<?php
include 'common.php';
echo "<div id='space'></br></br></br></br></br></br></div>";
include 'connect.php';
$st_id=$_GET['st_id'];

$q1="SELECT * FROM student WHERE st_id='$st_id'";

$e1 = mysql_query( $q1, $conn );
while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
	$name=$row1['name'];
	$section=$row1['section'];
	$semester=$row1['semester'];
}

global $DB;                                                                 
$user = $DB->get_record('user', array('username'=>$st_id));                          //all this............... 
$user_student_number = $DB->get_field('user_info_data','data',array('fieldid'=>2,'userid'=>$user->id), $strictness=IGNORE_MISSING);
$userper[1]= trim($DB->get_field('user_info_data','data',array('fieldid'=>28,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[1]!=NULL && is_numeric($userper[1]) || strpos($userper[1],'%'))
$nullv++;
$userper[2]= trim($DB->get_field('user_info_data','data',array('fieldid'=>29,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[2]!=NULL && is_numeric($userper[2]) || strpos($userper[2],'%'))
$nullv++;
$userper[3]= trim($DB->get_field('user_info_data','data',array('fieldid'=>30,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[3]!=NULL  && is_numeric($userper[3]) || strpos($userper[3],'%'))
$nullv++;
$userper[4] = trim($DB->get_field('user_info_data','data',array('fieldid'=>31,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[4]!=NULL && is_numeric($userper[4]) || strpos($userper[4],'%'))
$nullv++;
$userper[5] = trim($DB->get_field('user_info_data','data',array('fieldid'=>32,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[5]!=NULL && is_numeric($userper[5]) || strpos($userper[5],'%'))
$nullv++;
$userper[6] = trim($DB->get_field('user_info_data','data',array('fieldid'=>33,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[6]!=NULL && is_numeric($userper[6]) || strpos($userper[6],'%'))
$nullv++;
$userper[7] = trim($DB->get_field('user_info_data','data',array('fieldid'=>34,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[7]!=NULL && is_numeric($userper[7]) || strpos($userper[7],'%'))
$nullv++;
$userper[8] = trim($DB->get_field('user_info_data','data',array('fieldid'=>35,'userid'=>$user->id), $strictness=IGNORE_MISSING));
if($userper[8]!=NULL && is_numeric($userper[8]) || strpos($userper[8],'%'))
$nullv++;

$user_percentageinsemester_total=$userper[1]+$userper[2]+$userper[3]+$userper[4]+$userper[5]+$userper[6]+$userper[7]+$userper[8];
$user_percentageinsemester_average=number_format(($user_percentageinsemester_total/($nullv)),2);
$category=$DB->get_field('user_info_data','data',array('fieldid'=>12,'userid'=>$user->id), $strictness=IGNORE_MISSING);

$get_branch="SELECT DISTINCT branch FROM student WHERE section='$section'";

$getit= mysql_query( $get_branch, $conn );
while($row = mysql_fetch_array($getit, MYSQL_ASSOC)){
$branch1=$row['branch'];
}

if($branch1=='CSE'){
$fullname='COMPUTER SCIENCE AND ENGINEERING';	
}
else if($branch1=='IT'){
$fullname='INFORMATION TECHNOLOGY';
}
else if($branch1=='ME'){
$fullname='MECHANICAL ENGINEERING';
}
else if($branch1=='ECE'){
$fullname='ELECTRONICS & COMMUNICATION ENGINEERING';
}
else if($branch1=='EI'){
$fullname='ELECTRONICS & INSTRUMENTATION ENGINEERING';
}
else if($branch1=='EN'){
$fullname='ELECTRICAL & ELECTRONICS ENGINEERING';
}
else if($branch1=='CE'){
$fullname='CIVIL ENGINEERING';
}
else if($branch1=='MBA'){
$fullname='MASTER OF BUISNESS ADMINISTRATION';
}
else if($branch1=='MCA'){
$fullname='MASTER OF COMPUTER APPLICATION';
}
else if($branch1=='AS-HU'){
$fullname='Applied Science and Humanities';
}

echo "
<style>
@media print
{.button {display:none;} td {padding:5px !important;}}
</style>
<script>
function prnt()
	{window.print();}
</script>
<div align='center' id='imc'>
				<h2>
					AJAY KUMAR GARG ENGINEERING COLLEGE, GHAZIABAD
				<h2>
				<h4>
					Department of $fullname<br>
					Theory Sessional Awards ODD SEMESTER 2018-19<br>
					Director Cases
				</h4>";
echo "<input type='button' value='PRINT' onclick='prnt()' class='button' /><br><br>";
echo "<table id='table3'>
			<tr>
				<td colspan='3' style='text-align:left'><b>Student No :</b></td>
				<td colspan='9' style='text-align:left'>$user_student_number</td>
			</tr>
			<tr>
				<td colspan='3' style='text-align:left'><b>Student Roll No :</b></td>
				<td colspan='9' style='text-align:left'>$st_id</td>
			</tr>
			<tr>
				<td colspan='3' style='text-align:left'><b>Student Name :</b></td>
				<td colspan='9' style='text-align:left'>$name</td>
			</tr>
			<tr>
				<td colspan='3' style='text-align:left'><b>Semester & Section :</b></td>
				<td colspan='9' style='text-align:left'>$semester-$section</td>
			</tr>
			<tr>
				<td colspan='3' style='text-align:left'><b>Category :</b></td>
				<td colspan='9' style='text-align:left'>$category</td>
			</tr>
			<tr>
				<td colspan='3' style='text-align:left'><b>Previous Semester Aggregate %</b></td>
				<td colspan='9' style='text-align:left'>$user_percentageinsemester_average</td>
			</tr>
			<tr>
				<td colspan='3' style='text-align:left'><b>Discipline cases (if any)</b></td>
				<td colspan='9' style='text-align:left'></td>
			</tr>";
			
		
			
$q1="SELECT DISTINCT sub_id FROM assignrole WHERE semester='$semester' AND section='$section' ORDER BY sub_id ASC";

$e1 = mysql_query( $q1, $conn );
while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
	$subjectid=$row1['sub_id'];                                    
	
	/*$q2="SELECT * FROM calculation WHERE semester='$semester' AND section='$section' AND sub_id='$subjectid' AND exam='PUT'";
	
	$e2 = mysql_query( $q2, $conn );
	while($row2 = mysql_fetch_array($e2, MYSQL_ASSOC)){                                 //calc.........
		$max=$row2['outof'];
	}*/
	$mmarks=mysql_fetch_array(mysql_query("SELECT marks FROM subject WHERE sub_id='$subjectid'"));
	$m_put=$mmarks[0];
	$marks[$subjectid]=$m_put;

	
}	
	//print_r($marks);
	ksort($marks);
	arsort($marks);

	$serial=0;
	if(in_array("100", $marks)){
		echo "<tr>
				<td rowspan='2' ><b>S.NO.</b></td>
				<td rowspan='2' ><b>Sub. Code</b></td>
				<td rowspan='2' ><b>Name of Faculty</b></td>
				<td colspan='7' style='text-align:center' ><b>RAW MARKS (FULL UNIT)</b></td>
				<td><b>MODERATED MARKS<b></td>
				<td rowspan='2' ><b>REMARKS</b></td>
			</tr>";
			if($semester>4){
				echo "
				<tr>
					<td><b>ST1 (7.5)</b></td>
					<td><b>ST2 (7.5)</b></td>
					<td><b>PUT (15)</b></td>
					<td><b>ST (30)</b></td>
					<td><b>TAQ (10)</b></td>
					<td><b>AT (10)</b></td>
					<td><b>ST+TA+AT (50)</b></td>
					<td><b>TOTAL (50)</b></td>
				</tr>";
			}
			else if($semester<=4){
				echo "
				<tr>
					<td><b>ST1 (4)</b></td>
					<td><b>ST2 (6)</b></td>
					<td><b>PUT (10)</b></td>
					<td><b>ST (20)</b></td>
					<td><b>TAQ (5)</b></td>
					<td><b>AT (5)</b></td>
					<td><b>ST+TA+AT (30)</b></td>
					<td><b>TOTAL (30)</b></td>
				</tr>";
			}

		foreach($marks as $x=>$x_value){
			if($marks[$x]==100){


				$serial++;
				//getting faculty_name
				$q1="SELECT * FROM assignrole WHERE semester='$semester' AND section='$section' AND sub_id='$x'";
				
				$e1 = mysql_query( $q1, $conn );
				while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
					$fac_id=$row1['fac_id'];                              
				}
				$q1="SELECT * FROM login WHERE userid='$fac_id'";
				
				$e1 = mysql_query( $q1, $conn );
				while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
					$faculty_name=$row1['name'];                              
				}
				//getting marks
				$q1="SELECT * FROM marks WHERE st_id='$st_id' AND sub_id='$x'";
				
				$e1 = mysql_query( $q1, $conn );
				while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
					$ct1=$row1['exam1'];
					$ct2=$row1['exam2'];
					$st1=$row1['exam3'];
					$st2=$row1['exam4'];
					$put=$row1['exam5'];
				}
				//getting maximum_marks
				/*$query="SELECT * FROM calculation WHERE semester='$semester' AND section='$section' AND sub_id='$subjectid' AND exam='CT1'";
				
				$execute_query = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($execute_query, MYSQL_ASSOC)){                                      //calc...........
					$m_ct1=$row['outof'];
				}
				$query="SELECT * FROM calculation WHERE semester='$semester' AND section='$section' AND sub_id='$subjectid' AND exam='CT2'";
				
				$execute_query = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($execute_query, MYSQL_ASSOC)){
					$m_ct2=$row['outof'];
				}*/
				$m_ct1=10;
				$m_ct2=10;
				//calculating TA
				$result="SELECT * FROM record WHERE st_id='$st_id' AND sub_id='$subjectid'";
				
				$execute_result = mysql_query( $result, $conn );
				$ass_no=0;
				$ass_marks=0;
				while($row1 = mysql_fetch_array($execute_result, MYSQL_ASSOC)){
					$ass_no++;
					$ass_marks=$ass_marks+$row1['marks'];
				}
				$ta=round(($ass_marks/$ass_no)/5*5,2);
				if($ass_no==0){
					$ta=round(($ct1+$ct2)/($m_ct1+$m_ct2)*5,2);
				}
				$taq=$ta+round(($ct1+$ct2)/($m_ct1+$m_ct2)*5,2);
				
				//getting attendance
				$result="SELECT * FROM attendance WHERE st_id='$st_id' AND sub_id='$x'";
				
				$execute_result = mysql_query( $result, $conn );
				$a=0;
				$b=0;
				while($row1 = mysql_fetch_array($execute_result, MYSQL_ASSOC)){
					$a=$a+$row1['totalclasses'];
					$b=$b+$row1['attended'];
				}
				$n_att=round($b/$a*10,2);
				
				$n_st1=round($st1*7.5/30,2);
				$n_st2=round($st2*7.5/30,2);
				$n_put=round($put*15/100,2);
				$st=$n_st1+$n_st2+$n_put;
				$total=$st+$taq+$n_att;
				
				echo "<tr>
						<td>$serial</td>
						<td>$x</td>
						<td>$faculty_name</td>
						<td>$n_st1</td>
						<td>$n_st2</td>
						<td>$n_put</td>
						<td>$st</td>
						<td>$taq</td>
						<td>$n_att</td>
						<td>$total</td>
						<td></td>
						<td></td>
					</tr>";
				
			}
		}
	}


	if(in_array("50", $marks)){

		echo "<tr><td colspan='12'>&nbsp;</td></tr>";
		echo "<tr>
				<td rowspan='2' ><b>S.NO.</b></td>
				<td rowspan='2' ><b>Sub. Code</b></td>
				<td rowspan='2' ><b>Name of Faculty</b></td>
				<td colspan='7' style='text-align:center' ><b>RAW MARKS (HALF UNIT)</b></td>
				<td><b>MODERATED MARKS</b></td>
				<td rowspan='2' ><b>REMARKS</b></td>
			</tr>
			<tr>
				<td><b>ST1 (3.75)</b></td>
				<td><b>ST2 (3.75)</b></td>
				<td><b>PUT (7.5)</b></td>
				<td><b>ST (15)</b></td>
				<td><b>TAQ (5)</b></td>
				<td><b>AT (5)</b></td>
				<td><b>ST+TA+AT (25)</b></td>
				<td><b>TOTAL (25)</b></td>
			</tr>";

		foreach($marks as $x=>$x_value){
			if($marks[$x]==50){


				$serial++;
				//getting faculty_name
				$q1="SELECT * FROM assignrole WHERE semester='$semester' AND section='$section' AND sub_id='$x'";
				
				$e1 = mysql_query( $q1, $conn );
				while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
					$fac_id=$row1['fac_id'];                              
				}
				$q1="SELECT * FROM login WHERE userid='$fac_id'";
				
				$e1 = mysql_query( $q1, $conn );
				while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
					$faculty_name=$row1['name'];                              
				}
				//getting marks
				$q1="SELECT * FROM marks WHERE st_id='$st_id' AND sub_id='$x'";
				
				$e1 = mysql_query( $q1, $conn );
				while($row1 = mysql_fetch_array($e1, MYSQL_ASSOC)){
					$ct1=$row1['exam1'];
					$ct2=$row1['exam2'];
					$st1=$row1['exam3'];
					$st2=$row1['exam4'];
					$put=$row1['exam5'];
				}
				//getting maximum_marks
				/*$query="SELECT * FROM calculation WHERE semester='$semester' AND section='$section' AND sub_id='$subjectid' AND exam='CT1'";
				
				$execute_query = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($execute_query, MYSQL_ASSOC)){
					$m_ct1=$row['outof'];
				}
				$query="SELECT * FROM calculation WHERE semester='$semester' AND section='$section' AND sub_id='$subjectid' AND exam='CT2'";
				
				$execute_query = mysql_query( $query, $conn );
				while($row = mysql_fetch_array($execute_query, MYSQL_ASSOC)){
					$m_ct2=$row['outof'];
				}*/
				$m_ct1=10;
				$m_ct2=10;
				//calculating TA
				$result="SELECT * FROM record WHERE st_id='$st_id' AND sub_id='$subjectid'";
				
				$execute_result = mysql_query( $result, $conn );
				$ass_no=0;
				$ass_marks=0;
				while($row1 = mysql_fetch_array($execute_result, MYSQL_ASSOC)){
					$ass_no++;
					$ass_marks=$ass_marks+$row1['marks'];
				}
				$ta=round(($ass_marks/$ass_no)/5*5,2);
				if($ass_no==0){
					$ta=round(($ct1+$ct2)/($m_ct1+$m_ct2)*5,2);
				}
				$taq=$ta+round(($ct1+$ct2)/($m_ct1+$m_ct2)*5,2);
				
				//getting attendance
				$result="SELECT * FROM attendance WHERE st_id='$st_id' AND sub_id='$x'";
				
				$execute_result = mysql_query( $result, $conn );
				$a=0;
				$b=0;
				while($row1 = mysql_fetch_array($execute_result, MYSQL_ASSOC)){
					$a=$a+$row1['totalclasses'];
					$b=$b+$row1['attended'];
				}
				$n_att=round($b/$a*10,2);
				
				$n_st1=round($st1*7.5/30,2);
				$n_st2=round($st2*7.5/30,2);
				$n_put=round($put*15/100,2);
				$st=$n_st1+$n_st2+$n_put;
				$total=$st+$taq+$n_att;
				
				echo "<tr>
						<td>$serial</td>
						<td>$x</td>
						<td>$faculty_name</td>
						<td>$n_st1</td>
						<td>$n_st2</td>
						<td>$n_put</td>
						<td>$st</td>
						<td>$taq</td>
						<td>$n_att</td>
						<td>$total</td>
						<td></td>
						<td></td>
					</tr>";
				
			}
		}
	}
	
			
			
	echo "</table></div>";
?>